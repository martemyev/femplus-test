#ifndef TEST_FEMPLUS_COARSE_RECTANGULAR_MESH_HPP
#define TEST_FEMPLUS_COARSE_RECTANGULAR_MESH_HPP

#include "config.hpp"
#include <gtest/gtest.h>

#include "femplus/function.hpp"
#include "femplus/coarse_mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/coarse_element.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/rectangle.hpp"
#include "femplus/coarse_rectangle.hpp"

//#define SLOW_HERE

using namespace femplus;

// =======================================================
TEST(CoarseRectangularMesh, build_1_1_1_1)
{
  RectMeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 1, 1));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh));

  cmesh->build_coarse_mesh();

  EXPECT_EQ(cmesh->coarse_mesh()->n_vertices(), 4);
  EXPECT_EQ(cmesh->coarse_mesh()->n_elements(), 1);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_build_1_1_1_1.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, build_1_5_1_1)
{
  RectMeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 1, 5));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh));

  cmesh->build_coarse_mesh();

  //EXPECT_EQ(cmesh->coarse_mesh()->n_vertices(), 4);
  //EXPECT_EQ(cmesh->coarse_mesh()->n_elements(), 1);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_build_1_5_1_1.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, build_10_5_1_1)
{
  RectMeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 10, 5));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh));

  cmesh->build_coarse_mesh();

  //EXPECT_EQ(cmesh->coarse_mesh()->n_vertices(), 4);
  //EXPECT_EQ(cmesh->coarse_mesh()->n_elements(), 1);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_build_10_5_1_1.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, build_1_1_5_1)
{
  RectMeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 1, 1));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 5, 1));

  cmesh->build_coarse_mesh();

  EXPECT_EQ(cmesh->coarse_mesh()->n_vertices(), 4);
  EXPECT_EQ(cmesh->coarse_mesh()->n_elements(), 1);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_build_1_1_5_1.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, build_1_5_5_1)
{
  RectMeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 1, 5));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 5, 1));

  cmesh->build_coarse_mesh();

  //EXPECT_EQ(cmesh->coarse_mesh()->n_vertices(), 4);
  //EXPECT_EQ(cmesh->coarse_mesh()->n_elements(), 1);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_build_1_5_5_1.msh");
}

#if defined(SLOW_HERE)
// =======================================================
TEST(CoarseRectangularMesh, build_10_5_5_10)
{
  RectMeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 10, 5));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 5, 10));

  cmesh->build_coarse_mesh();

  //EXPECT_EQ(cmesh->coarse_mesh()->n_vertices(), 4);
  //EXPECT_EQ(cmesh->coarse_mesh()->n_elements(), 1);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_build_10_5_5_10.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, build_tri_10_10_10_10)
{
  RectMeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 10, 10));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 10, 10,
                                                FineMeshType::Triangular));

  cmesh->build_coarse_mesh();

  //EXPECT_EQ(cmesh->coarse_mesh()->n_vertices(), 4);
  //EXPECT_EQ(cmesh->coarse_mesh()->n_elements(), 1);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_build_tri_10_10_10_10.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, full_fine_triangular_mesh_1)
{
  RectMeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 1, 1));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 1, 1,
                                                FineMeshType::Triangular));

  cmesh->build_coarse_mesh();

  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_full_fine_tri_dupl_1.msh");

  MeshPtr tri_mesh = cmesh->full_fine_triangular_mesh();
  tri_mesh->write_msh("coarse_rect_full_fine_tri_uniq_1.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, full_fine_triangular_mesh_4)
{
  RectMeshPtr mesh(new RectangularMesh(0, 10, 0, 10, 2, 2));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 2, 2,
                                                FineMeshType::Triangular));

  cmesh->build_coarse_mesh();

  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_full_fine_tri_dupl_4.msh");

  MeshPtr tri_mesh = cmesh->full_fine_triangular_mesh();
  tri_mesh->write_msh("coarse_rect_full_fine_tri_uniq_4.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, full_fine_triangular_mesh_25)
{
  RectMeshPtr mesh(new RectangularMesh(0, 10, 0, 10, 5, 5));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 5, 5,
                                                FineMeshType::Triangular));

  cmesh->build_coarse_mesh();

  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_full_fine_tri_dupl_25.msh");

  MeshPtr tri_mesh = cmesh->full_fine_triangular_mesh();
  tri_mesh->write_msh("coarse_rect_full_fine_tri_uniq_25.msh");
}
#endif // SLOW_HERE

// =======================================================
TEST(CoarseRectangularMesh, read_triangular_mesh)
{
  RectMeshPtr mesh(new RectangularMesh(0, 10, 0, 10, 5, 5));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 5, 5,
                                                FineMeshType::Triangular));

  cmesh->build_coarse_mesh();

  cmesh->read_fine_mesh(TESTFILES_DIR + "/tria_master.msh");

  cmesh->write_msh("coarse_rect_full_fine_tri_dupl_read.msh");

  MeshPtr tri_mesh = cmesh->full_fine_triangular_mesh();
  tri_mesh->write_msh("coarse_rect_full_fine_tri_uniq_read.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, build_struct_triangular_mesh_0)
{
  RectMeshPtr mesh(new RectangularMesh(0, 10, 0, 10, 2, 2));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 2, 2,
                                                FineMeshType::StructuredTriangular));

  cmesh->build_coarse_mesh();
  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_struct_tria_0.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, build_struct_triangular_mesh_1)
{
  RectMeshPtr mesh(new RectangularMesh(0, 10, 0, 10, 5, 5));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 5, 5,
                                                FineMeshType::StructuredTriangular));

  cmesh->build_coarse_mesh();
  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_struct_tria_1.msh");
}

// =======================================================
TEST(CoarseRectangularMesh, build_struct_triangular_mesh_2)
{
  RectMeshPtr mesh(new RectangularMesh(0, 10, 0, 10, 1, 5));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, 5, 1,
                                                FineMeshType::StructuredTriangular));

  cmesh->build_coarse_mesh();
  cmesh->build_fine_mesh();
  cmesh->write_msh("coarse_rect_struct_tria_2.msh");
}

//==============================================================================
TEST(CoarseRectangularMesh, build_diff_fine_mesh_0)
{
  const double x0 = 0;
  const double x1 = 15;
  const double y0 = 0;
  const double y1 = 15;
  const int    Nx = 3;
  const int    Ny = 3;
  const int    nx = 5;
  const int    ny = 5;
  RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  mesh->build();

  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny)); // like for the case of Rectangular fine grid

  const int types[Nx*Ny] = { 0, 1, 1, 0, 0, 1, 0, 1, 1 };

  for (int el = 0; el < Nx*Ny; ++el)
  {
    // current element
    Rectangle *elem = dynamic_cast<Rectangle*>(mesh->element(el));
    EXPECT_TRUE(elem != NULL);

    MeshPtr fmesh;

    switch (types[el])
    {
      case 0:
      {
        fmesh = MeshPtr(new RectangularMesh(mesh->vertex(elem->vertex_number(0)),
                                            mesh->vertex(elem->vertex_number(3)),
                                            nx, ny));
        break;
      }
      case 1:
      {
        fmesh = MeshPtr(new TriangularMesh(mesh->vertices(),
                                           MeshElementPtr(elem),
                                           nx));
        break;
      }
      default:
        throw std::runtime_error("Unexpected type of a fine mesh");
    }

    CoarseElement *c_elem = new CoarseRectangle(el,     // serial number
                                                fmesh,  // fine mesh inside
                                                *elem); // rectangle as a base
    cmesh->add_element(c_elem);
  }

  cmesh->build_fine_mesh();
  cmesh->write_msh("crect_diff_fine_0.msh");
}

//==============================================================================
TEST(CoarseRectangularMesh, build_diff_fine_mesh_1)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    Nx = 10;
  const int    Ny = 10;
  const int    nx = 10;
  const int    ny = 10;
  RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  mesh->build();

  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny)); // like for the case of Rectangular fine grid

  for (int el = 0; el < Nx*Ny; ++el)
  {
    // current element
    Rectangle *elem = dynamic_cast<Rectangle*>(mesh->element(el));
    EXPECT_TRUE(elem != NULL);

    MeshPtr fmesh;

    switch (rand()%2)
    {
      case 0:
      {
        fmesh = MeshPtr(new RectangularMesh(mesh->vertex(elem->vertex_number(0)),
                                            mesh->vertex(elem->vertex_number(3)),
                                            nx, ny));
        break;
      }
      case 1:
      {
        fmesh = MeshPtr(new TriangularMesh(mesh->vertices(),
                                           MeshElementPtr(elem),
                                           nx));
        break;
      }
      default:
        throw std::runtime_error("Unexpected type of a fine mesh");
    }

    CoarseElement *c_elem = new CoarseRectangle(el,     // serial number
                                                fmesh,  // fine mesh inside
                                                *elem); // rectangle as a base
    cmesh->add_element(c_elem);
  }

  cmesh->build_fine_mesh();
  cmesh->write_msh("crect_diff_fine_1.msh");
}


#endif // TEST_FEMPLUS_COARSE_RECTANGULAR_MESH_HPP
