#ifndef TEST_FEMPLUS_COARSE_FINITE_ELEMENT_MESH_HPP
#define TEST_FEMPLUS_COARSE_FINITE_ELEMENT_MESH_HPP

#include "config.hpp"
#include "femplus/coarse_finite_element.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/coarse_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/coarse_finite_element_mesh.hpp"
#include "femplus/finite_element_mesh.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/cg_dof_handler.hpp"
#include "femplus/pattern.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/coarse_element.hpp"
#include "femplus/dense_pattern.hpp"
#include "femplus/solver.hpp"
#include "femplus/vector.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/cg_interior_dof_handler.hpp"
#include "femplus/mixed_poly_rect_mesh.hpp"
#include "femplus/coarse_mixed_poly_rect_mesh.hpp"
#include "slepceps.h"
#include <gtest/gtest.h>
#include <memory>
#if defined(USE_BOOST)
  #include "boost/format.hpp"
   #include "boost/timer/timer.hpp"
#endif

//#define SHOW_MAT
//#define SHOW_EIG

using namespace femplus;

// =============================================================================
//
// functions declarations
//
// =============================================================================
void test_boundary_basis(int Nx, int Ny,
                         int nx, int ny,
                         int el_number,
                         int fine_mesh_order,
                         const std::string &type_coarse_mesh,
                         const std::string &type_fine_mesh,
                         CoarseFiniteElement &cf_element,
                         int &, std::shared_ptr<PetscMatrix>&);
void test_interior_basis(int Nx, int Ny,
                         int nx, int ny,
                         int el_number,
                         int fine_mesh_order,
                         const std::string &type_coarse_mesh,
                         const std::string &type_fine_mesh,
                         EPSType eps_type,
                         CoarseFiniteElement &cf_element,
                         int &, std::shared_ptr<PetscMatrix>&);
void get_tats_eigenvalues(int Nx, int Ny,
                          int nx, int ny,
                          int el,
                          std::vector<double> &tats_eigenvalues);



// =============================================================================
//
// TESTS ON SINGLE COARSE ELEMENT
//
// =============================================================================
TEST(RectCoarseFiniteElementMesh, mesh1_compute_basis_rect_fine_grid)
{
  // coarse mesh parameters
  const double X0 = 0;
  const double X1 = 1;
  const double Y0 = 0;
  const double Y1 = 1;
  const int Nx = 2;
  const int Ny = 2;
  // fine mesh size
  const int nx = 3;
  const int ny = 3;

  RectMeshPtr mesh(new RectangularMesh(X0, X1, Y0, Y1, Nx, Ny));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny));

  cmesh->build_coarse_mesh();
  cmesh->build_fine_mesh();

  const int coarse_order = 1;
  const int fine_order = 1;
  CoarseFEMeshPtr coarse_fe_mesh(new CoarseFiniteElementMesh(cmesh,
                                                             coarse_order,
                                                             fine_order));

  // pass through all coarse finite elements
  for (int el = 0; el < coarse_fe_mesh->n_elements(); ++el)
  {
    // current coarse finite element which is going to be changed
    CoarseFiniteElement *cf_element = coarse_fe_mesh->element(el);

    // --------------------- boundary basis ----------------------------
    std::shared_ptr<PetscMatrix> boundary_basis_R = NULL;
    int n_boundary_bf = 1;
    test_boundary_basis(Nx, Ny, nx, ny, el, fine_order,
                        "rect", "rect", *cf_element,
                        n_boundary_bf, boundary_basis_R);

#if defined(SHOW_MAT)
  std::cout << "\n\nboundary_basis_R\n";
  boundary_basis_R->view();
#endif

    // --------------------- interior basis ----------------------------
    std::shared_ptr<PetscMatrix> interior_basis_R = NULL;
    int n_interior_bf = 1;
    test_interior_basis(Nx, Ny, nx, ny, el, fine_order,
                        "rect", "rect",
                        EPSLAPACK,
                        *cf_element,
                        n_interior_bf,
                        interior_basis_R);

#if defined(SHOW_MAT)
  std::cout << "\n\ninterior_basis_R\n";
  interior_basis_R->view();
#endif

    DensePattern pattern(n_boundary_bf + n_interior_bf,
                         cf_element->coarse_element()->fine_mesh()->n_vertices());
    PetscMatrix *_R = new PetscMatrix(pattern);
    for (int i = 0; i < n_boundary_bf; ++i)
      _R->set_row(i, boundary_basis_R->get_row(i));

    for (int i = 0; i < n_interior_bf; ++i)
      _R->set_row(n_boundary_bf + i,
                  interior_basis_R->get_row(i));

    _R->final_assembly();

#if defined(SHOW_MAT)
  std::cout << "\n\nR MATRIX\n";
  _R->view();
#endif
  }

}
// =============================================================================
//TEST(RectCoarseFiniteElementMesh, mesh1_compute_basis_tria_fine_grid)
//{
//  // coarse mesh parameters
//  const double X0 = 0;
//  const double X1 = 1;
//  const double Y0 = 0;
//  const double Y1 = 1;
//  const int Nx = 1;
//  const int Ny = 1;
//  // fine mesh size
//  const int nx = 5;
//  const int ny = 5;

//  RectMeshPtr mesh(new RectangularMesh(X0, X1, Y0, Y1, Nx, Ny));
//  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh,
//                                                nx,
//                                                ny,
//                                                FineMeshType::Triangular));

//  cmesh->build_coarse_mesh();
//  cmesh->build_fine_mesh();

//  const int coarse_order = 1;
//  const int fine_order = 1;
//  CoarseFEMeshPtr coarse_fe_mesh(new CoarseFiniteElementMesh(cmesh,
//                                                             coarse_order,
//                                                             fine_order));

//  // pass through all coarse finite elements
//  for (int el = 0; el < coarse_fe_mesh->n_elements(); ++el)
//  {
//    // current coarse finite element which is going to be changed
//    CoarseFiniteElement *cf_element = coarse_fe_mesh->element(el);
//    test_boundary_basis(Nx, Ny, nx, ny, el, fine_order,
//                        "rect", "tria", *cf_element);
//    test_interior_basis(Nx, Ny, nx, ny, el, fine_order,
//                        "rect", "tria", *cf_element);
//  }
//}



//// =============================================================================
////
//// TESTS ON MESH CONSISTING OF 4 COARSE ELEMENTS
////
//// =============================================================================
//TEST(RectCoarseFiniteElementMesh, mesh4_compute_basis_rect_fine_grid)
//{
//  // coarse mesh parameters
//  const double X0 = 0;
//  const double X1 = 1;
//  const double Y0 = 0;
//  const double Y1 = 1;
//  const int Nx = 2;
//  const int Ny = 2;
//  // fine mesh size
//  const int nx = 5;
//  const int ny = 5;

//  RectMeshPtr mesh(new RectangularMesh(X0, X1, Y0, Y1, Nx, Ny));
//  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny));

//  cmesh->build_coarse_mesh();
//  cmesh->build_fine_mesh();

//  const int coarse_order = 1;
//  const int fine_order = 1;
//  CoarseFEMeshPtr coarse_fe_mesh(new CoarseFiniteElementMesh(cmesh,
//                                                             coarse_order,
//                                                             fine_order));

//  // pass through all coarse finite elements
//  for (int el = 0; el < coarse_fe_mesh->n_elements(); ++el)
//  {
//    // current coarse finite element which is going to be changed
//    CoarseFiniteElement *cf_element = coarse_fe_mesh->element(el);
//    test_boundary_basis(Nx, Ny, nx, ny, el, fine_order,
//                        "rect4", "rect", *cf_element);
//    test_interior_basis(Nx, Ny, nx, ny, el, fine_order,
//                        "rect4", "rect", *cf_element);
//  }
//}
//// =============================================================================
//TEST(RectCoarseFiniteElementMesh, mesh4_compute_basis_tria_fine_grid)
//{
//  // coarse mesh parameters
//  const double X0 = 0;
//  const double X1 = 1;
//  const double Y0 = 0;
//  const double Y1 = 1;
//  const int Nx = 2;
//  const int Ny = 2;
//  // fine mesh size
//  const int nx = 5;
//  const int ny = 5;

//  RectMeshPtr mesh(new RectangularMesh(X0, X1, Y0, Y1, Nx, Ny));
//  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh,
//                                                nx,
//                                                ny,
//                                                FineMeshType::Triangular));

//  cmesh->build_coarse_mesh();
//  cmesh->build_fine_mesh();

//  const int coarse_order = 1;
//  const int fine_order = 1;
//  CoarseFEMeshPtr coarse_fe_mesh(new CoarseFiniteElementMesh(cmesh,
//                                                             coarse_order,
//                                                             fine_order));

//  // pass through all coarse finite elements
//  for (int el = 0; el < coarse_fe_mesh->n_elements(); ++el)
//  {
//    // current coarse finite element which is going to be changed
//    CoarseFiniteElement *cf_element = coarse_fe_mesh->element(el);
//    test_boundary_basis(Nx, Ny, nx, ny, el, fine_order,
//                        "rect4", "tria", *cf_element);
//    test_interior_basis(Nx, Ny, nx, ny, el, fine_order,
//                        "rect4", "tria", *cf_element);
//  }
//}



//// =============================================================================
////
//// TESTS ON MIXED COARSE MESH
////
//// =============================================================================
//TEST(MixedCoarseFiniteElementMesh, mixed_mesh_compute_basis)
//{
//  // coarse mesh parameters
//  const double X0 = 0;
//  const double X1 = 1;
//  const double Y0 = 0;
//  const int Nx = 5;
//  const int Ny = 5;
//  // fine mesh size
//  const int nx = 5;
//  const int ny = 5;
//  // depth of the domain
//  const double depth = 1;

//  std::vector<Point> top_surface;
//  top_surface.push_back(Point(X0, Y0));
//  top_surface.push_back(Point(X1, Y0));

//  const int N = 100;
//  for (int i = 0; i < N - 1; ++i)
//  {
//    const double x = (i + 1) * 1. / N;
//    top_surface.push_back(Point(x, 0.02 * sin(10*math::PI*x)));
//  }

//  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface,
//                                                      depth,
//                                                      Nx,
//                                                      Ny));
//  CoarseMeshPtr cmesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny));
//  cmesh->build_coarse_mesh();
//  cmesh->build_fine_mesh();

//  const int coarse_order = 1;
//  const int fine_order = 1;
//  CoarseFEMeshPtr coarse_fe_mesh(new CoarseFiniteElementMesh(cmesh,
//                                                             coarse_order,
//                                                             fine_order));

//  // pass through all coarse finite elements
//  for (int el = 0; el < coarse_fe_mesh->n_elements(); ++el)
//  {
//    // current coarse finite element which is going to be changed
//    CoarseFiniteElement *cf_element = coarse_fe_mesh->element(el);
//    test_boundary_basis(Nx, Ny, nx, ny, el, fine_order,
//                        "mixed", "tria", *cf_element);
//    test_interior_basis(Nx, Ny, nx, ny, el, fine_order,
//                        "mixed", "tria", *cf_element);
//  }
//}



// =============================================================================
//
// BOUNDARY BASIS
//
// =============================================================================
void test_boundary_basis(int Nx, int Ny,
                         int nx, int ny,
                         int el_number,
                         int fine_mesh_order,
                         const std::string &type_coarse_mesh,
                         const std::string &type_fine_mesh,
                         CoarseFiniteElement &cf_element,
                         int &n_basis_functions,
                         std::shared_ptr<PetscMatrix> &boundary_basis_R)
{
  // first we create a new finite element mesh based on a fine mesh of
  // the coarse element
  FEMeshPtr fe_mesh(new LagrangeMesh(cf_element.coarse_element()->fine_mesh(),
                                     fine_mesh_order));
  // then we create a standard (CG) dof handler
  CGDoFHandler cg_dof_handler(fe_mesh);
  // and finally we initialize (distribute) all degrees of freedom in the
  // computational domain
  cg_dof_handler.distribute_dofs();
  expect(cg_dof_handler.n_dofs() > 0,
         "The fine scale degrees of freedom are not initialized (distributed)");
  expect(cg_dof_handler.n_boundary_dofs() > 0,
         "The fine scale boundary degrees of freedom are not known");

  // to make a sparse pattern of the future matrices we need to know the
  // connections between dofs
  std::vector<std::set<int> > connections;
  cg_dof_handler.dofs_connections(connections);
  // create a pattern of the future sparse mass and laplace matrices
  PatternPtr pattern(new CSRPattern(connections));
  // allocate the memory for the mass and laplace matrices
  PetscMatrixPtr _mass_matrix(new PetscMatrix(*pattern.get()));
  PetscMatrixPtr _laplace_matrix(new PetscMatrix(*pattern.get()));
  // build the matrices
  cg_dof_handler.build_mass_matrix(ConstantFunction(1),
                                   *_mass_matrix);
  _mass_matrix->final_assembly();
  cg_dof_handler.build_stiffness_matrix(ConstantFunction(1),
                                        *_laplace_matrix);
  _laplace_matrix->final_assembly();

  // now both mass and laplace matrices should be computed
  require(_mass_matrix != NULL && _laplace_matrix != NULL,
          "Mass and/or laplace matrix is not computed");

  // the number of boundary dofs is equal to the number of all functions from
  // V_H^1 (boundary basis functions).
  const int n_boundary_dofs = cg_dof_handler.n_boundary_dofs();
  // the number of all dofs is equal to the number of all fine scale basis
  // functions
  const int n_dofs = cg_dof_handler.n_dofs();
  // all weights of multiscale boundary basis functions
  PatternPtr dense_pattern(new DensePattern(n_boundary_dofs, n_dofs));
  PetscMatrixPtr all_bound_weights(new PetscMatrix(*dense_pattern.get()));

  // stiffness matrix with Dirichlet boundary condition:
  // allocate memory and copy values
  PetscMatrix stiff_mat_with_bc(*_laplace_matrix.get());

  // Dirichlet boundary condition
  cg_dof_handler.dirichlet(stiff_mat_with_bc);

  // SLAE solver
  Solver solver(stiff_mat_with_bc);

  // pass through all boundary dofs
  for (int bd = 0; bd < n_boundary_dofs; ++bd)
  {
    // calculate ms_weights coefficients.
    // at one boundary node the function is 1, at others - is 0

    // number of the current boundary dof, at which the basis function is 1
    const int b_dof_index = cg_dof_handler.boundary_dof_number(bd);

    // create zero vector
    Vector system_rhs(n_dofs);
    // set the value corresponding to the number of the boundary dof
    system_rhs(b_dof_index) = 1.;

    // solution of the SLAE for each boundary dof
    Vector ms_weights(n_dofs);

    // solve the SLAE
    solver.solve(system_rhs, ms_weights);

    // save the vector ms_weights in the matrix of all weights of multiscale
    // boundary basis functions
    all_bound_weights->set_row(bd,          // number of the row
                               ms_weights); // the values

  } // we were passing through all boundary dofs

  // final assembly for the matrix of coefficients (weights) is needed for
  // further usage
  all_bound_weights->final_assembly();

  // matrix for local spectral problem: WLWT = W * laplace_matrix * W^T,
  // where W is a matrix of all weights of multiscale basis functions
  PatternPtr coarse_scale_dense_pattern(new DensePattern(n_boundary_dofs,
                                                         n_boundary_dofs));
  PetscMatrix *WLWT = new PetscMatrix(*coarse_scale_dense_pattern.get());
  // the WLWT matrix has allocated memory and all we need is to set the values
  // of the matrix
  WLWT->RARt(_laplace_matrix, all_bound_weights);

  // solution of the local spectral problem
  EPS eps;
  EPSCreate(MPI_COMM_WORLD, &eps);
  EPSSetOperators(eps, WLWT->mat(), NULL);
  EPSSetProblemType(eps, EPS_HEP);
  EPSSetType(eps, EPSLAPACK);
  EPSSetWhichEigenpairs(eps, EPS_SMALLEST_MAGNITUDE);
  EPSSolve(eps);

  int nconv;
  EPSGetConverged(eps, &nconv);
  std::vector<double> eigenvalue_real(nconv, 0);
  std::vector<double> eigenvalue_imag(nconv, 0);
  Vec eigenvector_real, eigenvector_imag;
  VecCreateSeq(MPI_COMM_WORLD, n_boundary_dofs, &eigenvector_real);
  VecCreateSeq(MPI_COMM_WORLD, n_boundary_dofs, &eigenvector_imag);

  PatternPtr dense_pat(new DensePattern(n_boundary_dofs, n_boundary_dofs));
  PetscMatrix *eigenvectors = new PetscMatrix(*dense_pat.get());

#if defined(SHOW_EIG)
  std::cout << "\n\nboundary eigenvalues\n";
#endif

  for (int i = 0; i < nconv; ++i)
  {
    EPSGetEigenpair(eps,
                    i,
                    &eigenvalue_real[i],
                    &eigenvalue_imag[i],
                    eigenvector_real,
                    eigenvector_imag);

    // all imaginary parts should be 0
    expect(fabs(eigenvalue_imag[i]) < math::FLOAT_NUMBERS_EQUALITY_TOLERANCE,
           "The imaginaty part of the eigenvalue is not 0, it's " +
           d2s<double>(fabs(eigenvalue_imag[i]), 1));

#if defined(SHOW_EIG)
    std::cout << eigenvalue_real[i] << "\n";
#endif

    // save the eigenvector
    eigenvectors->set_row(i, Vector(eigenvector_real));
  }

  eigenvectors->final_assembly();

  EPSDestroy(&eps);

  // now we multiply a matrix of eigenvectors by the matrix of weights of
  // boundary basis functions to get the basis functions from V^1_H space
  boundary_basis_R = std::shared_ptr<PetscMatrix>(new PetscMatrix(*dense_pattern.get()));

  // boundary_basis_R = eigenvectors * all_bound_weights;
//  const double fill = 1. * boundary_basis_R->pattern()->n_nonzero_elements() /
//                      (eigenvectors->pattern()->n_nonzero_elements() +
//                       all_bound_weights->pattern()->n_nonzero_elements());

#if defined(SHOW_MAT)
  std::cout << "eigenvectors\n";
  eigenvectors->view();
  std::cout << "\n\nall_bound_weights\n";
  all_bound_weights->view();
#endif

  Mat temp_mat;
  MatMatMult(eigenvectors->mat(),
             all_bound_weights->mat(),
             MAT_INITIAL_MATRIX,
             //fill,
             PETSC_DEFAULT,
             &temp_mat);
  boundary_basis_R->set_mat(temp_mat);
  MatDestroy(&temp_mat);

#if defined(SHOW_MAT)
  std::cout << "\n\nboundary_basis_R\n";
  boundary_basis_R->view();
#endif

  delete eigenvectors;
  //delete all_bound_weights;

  // the number of basis functions that will be used in computations. if it's
  // senseless (like -1, or so), all boundary basis functions are used
  if (n_basis_functions <= 0)
    n_basis_functions = n_boundary_dofs;

}


// =============================================================================
//
// INTERIOR BASIS
//
// =============================================================================
void test_interior_basis(int Nx, int Ny,
                         int nx, int ny,
                         int el_number,
                         int fine_mesh_order,
                         const std::string &type_coarse_mesh,
                         const std::string &type_fine_mesh,
                         EPSType eps_type,
                         CoarseFiniteElement &cf_element,
                         int &n_basis_functions,
                         std::shared_ptr<PetscMatrix> &interior_basis_R)
{
#if defined(USE_BOOST)
  boost::timer::cpu_timer timer;
  boost::timer::auto_cpu_timer total_time;
#else
  double t_total_beg = get_wall_time();
  double t_beg = get_wall_time();
#endif

  // first we create a new finite element fine mesh based on the fine mesh of
  // this coarse element
  FEMeshPtr fe_mesh(new LagrangeMesh(cf_element.coarse_element()->fine_mesh(),
                                     fine_mesh_order));
  // we create a specific CG dof handler handling interior dofs only
  CGInteriorDoFHandler int_dof_handler(fe_mesh);
  int_dof_handler.distribute_dofs();
  // compute mass and laplace matrices with no entrance of boundary dofs.
  // we need new pattern for this
  std::vector<std::set<int> > connections;
  int_dof_handler.dofs_connections(connections);
  PatternPtr interior_pattern(new CSRPattern(connections));
  // build the mass and laplace matrices for this new pattern. we
  // automatically eliminate all entries and contributions from boundary dofs
  PetscMatrix *mass_mat_wo_bound = new PetscMatrix(*interior_pattern.get());
  PetscMatrix *lapl_mat_wo_bound = new PetscMatrix(*interior_pattern.get());
  // build the matrices
  int_dof_handler.build_mass_matrix(ConstantFunction(1),
                                    *mass_mat_wo_bound);
  mass_mat_wo_bound->final_assembly();
  int_dof_handler.build_stiffness_matrix(ConstantFunction(1),
                                         *lapl_mat_wo_bound);
  lapl_mat_wo_bound->final_assembly();

  // number of interior dofs = size of local problem
  int n_dofs = int_dof_handler.n_dofs();

  // the number of basis functions that will be used in computations. if it's
  // nonsense (like -1 or so), all interior basis functions are used
  if (n_basis_functions <= 0)
    n_basis_functions = n_dofs;

#if defined(USE_BOOST)
  std::cout << "matrices created, time = "
            << timer.elapsed().wall*1e-9 << std::endl;
  timer.resume();
#else
  double t_end = get_wall_time();
  std::cout << "matrices created, time = " << t_end - t_beg << " sec" << std::endl;
  t_beg = t_end;
#endif
  // -----------------------------------------------------------------

  // solution of the local spectral problem
  EPS eps_interior;
  EPSCreate(MPI_COMM_WORLD, &eps_interior);
  EPSSetOperators(eps_interior,
                  lapl_mat_wo_bound->mat(),
                  mass_mat_wo_bound->mat());
  EPSSetProblemType(eps_interior, EPS_GHEP);
  EPSSetType(eps_interior, eps_type);
  EPSSetDimensions(eps_interior,
                   n_basis_functions,
                   2*n_basis_functions,
                   2*n_basis_functions - 1);
  EPSSetWhichEigenpairs(eps_interior, EPS_SMALLEST_MAGNITUDE);
  EPSSolve(eps_interior);

#if defined(USE_BOOST)
  std::cout << "eigenproblem solved, time = "
            << timer.elapsed().wall*1e-9 << std::endl;
  timer.resume();
#else
  t_end = get_wall_time();
  std::cout << "eigenproblem solved, time = " << t_end - t_beg << " sec" << std::endl;
  t_beg = t_end;
#endif
  // -----------------------------------------------------------------

  int nconv;
  EPSGetConverged(eps_interior, &nconv);
//  expect(nconv == n_basis_functions,
//         "The number of obtained eigenvectors for local spectral problem for "
//         "interior basis (" + d2s<int>(nconv) + ") differs from the requested "
//         "number (" + d2s<int>(n_basis_functions) + ")");
  std::vector<double> eigenval_real(nconv, 0);
  std::vector<double> eigenval_imag(nconv, 0);
  Vec eigenvec_real, eigenvec_imag;
  VecCreateSeq(MPI_COMM_WORLD, n_dofs, &eigenvec_real);
  VecCreateSeq(MPI_COMM_WORLD, n_dofs, &eigenvec_imag);

  const int n_extend_dofs = n_dofs + int_dof_handler.n_boundary_dofs();
  PatternPtr dense_pattern(new DensePattern(nconv, n_extend_dofs));
  interior_basis_R = PetscMatrixPtr(new PetscMatrix(*dense_pattern.get()));

  std::vector<Vector> eigenvectors_re(nconv);
  std::vector<std::string> eigennames(nconv);

#if defined(SHOW_EIG)
  std::cout << "\n\ninterior eigenvalues\n";
#endif

  for (int i = 0; i < nconv; ++i)
  {
    EPSGetEigenpair(eps_interior,
                    i,
                    &eigenval_real[i],
                    &eigenval_imag[i],
                    eigenvec_real,
                    eigenvec_imag);

#if defined(SHOW_EIG)
    std::cout << eigenval_real[i] << "\n";
#endif

    // save for visualization
    eigenvectors_re[i] = Vector(eigenvec_real);
    eigennames[i] = "eigenvector_" + d2s<int>(i);

    // all imaginary parts should be 0
    expect(fabs(eigenval_imag[i]) < math::FLOAT_NUMBERS_EQUALITY_TOLERANCE,
           "The imaginaty part of the eigenvalue is not 0, it's " +
           d2s<double>(fabs(eigenval_imag[i]), 1));

    // save the eigenvectors for R matrix
    const Vector extended_eigvec = int_dof_handler.extend(Vector(eigenvec_real));
    interior_basis_R->set_row(i, extended_eigvec);
  }
  interior_basis_R->final_assembly();

#if defined(USE_BOOST)
  std::cout << "eigenvectors extracted, time = "
            << timer.elapsed().wall*1e-9 << std::endl;
#else
  t_end = get_wall_time();
  std::cout << "eigenvectors extracted, time = " << t_end - t_beg << " sec" << std::endl;
  t_beg = t_end;
#endif
  // -----------------------------------------------------------------

//  // visualization of the interior basis functions
//  const std::string filename = type_coarse_mesh +
//                               "inter_bas_el_" + d2s<int>(el_number) +
//                               "_fine_" + type_fine_mesh + ".vtu";
//  int_dof_handler.write_solution(filename,
//                                 eigenvectors_re,
//                                 eigennames);

  EPSDestroy(&eps_interior);
  VecDestroy(&eigenvec_imag);
  VecDestroy(&eigenvec_real);

#if !defined(USE_BOOST)
  double t_total_end = get_wall_time();
  std::cout << "total wall time: " << t_total_end - t_total_beg << " sec" << std::endl;
#endif
}



// =============================================================================
//
// auxiliary functions definitions
//
// =============================================================================
void get_tats_eigenvalues(int Nx, int Ny,
                          int nx, int ny,
                          int el,
                          std::vector<double> &tats_eigenvalues)
{
  if (Nx == 1 && Nx == 1 && nx == 2 && ny == 2)
  {
    // el doesn't matter, since there is only one coarse element
    tats_eigenvalues.push_back(5.551115123126e-17);
    tats_eigenvalues.push_back(6.339745962156e-01);
    tats_eigenvalues.push_back(6.339745962156e-01);
    tats_eigenvalues.push_back(1.000000000000e+00);
    tats_eigenvalues.push_back(2.000000000000e+00);
    tats_eigenvalues.push_back(2.000000000000e+00);
    tats_eigenvalues.push_back(2.366025403784e+00);
    tats_eigenvalues.push_back(2.366025403784e+00);
  }
  else if (Nx == 1 && Nx == 1 && nx == 3 && ny == 3)
  {
    // el doesn't matter, since there is only one coarse element
    tats_eigenvalues.push_back(-6.406595966714e-16);
    tats_eigenvalues.push_back(4.425462292616e-01);
    tats_eigenvalues.push_back(4.425462292616e-01);
    tats_eigenvalues.push_back(6.666666666667e-01);
    tats_eigenvalues.push_back(1.500000000000e+00);
    tats_eigenvalues.push_back(1.500000000000e+00);
    tats_eigenvalues.push_back(1.627050844183e+00);
    tats_eigenvalues.push_back(1.627050844183e+00);
    tats_eigenvalues.push_back(2.430402926556e+00);
    tats_eigenvalues.push_back(2.430402926556e+00);
    tats_eigenvalues.push_back(2.500000000000e+00);
    tats_eigenvalues.push_back(2.500000000000e+00);
  }
  else if (Nx == 1 && Nx == 1 && nx == 4 && ny == 4)
  {
    // el doesn't matter, since there is only one coarse element
    tats_eigenvalues.push_back(3.400058012915e-16);
    tats_eigenvalues.push_back(3.372468786496e-01);
    tats_eigenvalues.push_back(3.372468786496e-01);
    tats_eigenvalues.push_back(5.000000000000e-01);
    tats_eigenvalues.push_back(1.156929669183e+00);
    tats_eigenvalues.push_back(1.156929669183e+00);
    tats_eigenvalues.push_back(1.223321020787e+00);
    tats_eigenvalues.push_back(1.223321020787e+00);
    tats_eigenvalues.push_back(1.977149942825e+00);
    tats_eigenvalues.push_back(1.977149942825e+00);
    tats_eigenvalues.push_back(2.000000000000e+00);
    tats_eigenvalues.push_back(2.000000000000e+00);
    tats_eigenvalues.push_back(2.593070330817e+00);
    tats_eigenvalues.push_back(2.593070330817e+00);
    tats_eigenvalues.push_back(2.605139300596e+00);
    tats_eigenvalues.push_back(2.605139300596e+00);
  }
  else
    require(false, "Not known");
}

#endif // TEST_FEMPLUS_COARSE_FINITE_ELEMENT_MESH_HPP
