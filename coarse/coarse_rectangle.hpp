#ifndef FEMPLUS_TEST_COARSE_RECTANGLE_HPP
#define FEMPLUS_TEST_COARSE_RECTANGLE_HPP

#include "config.hpp"
#include "gtest/gtest.h"
#include "femplus/coarse_rectangle.hpp"
#include "femplus/rectangular_mesh.hpp"

using namespace femplus;

// =============================================================================
//
//                                 To developer
//
// =============================================================================
// This structure of the CoarseRectangle is crap. Look at the tests below.
// The fine mesh and a rectangle don't have to have anything in common to
// produce working coarse rectangle. That's wrong. A rectangle is a domain where
// coarse rectangle lives, and where the fine mesh should be built. They have to
// be tightly connected.


// =============================================================================
TEST(CoarseRectangle, ctor_0_0)
{
  // we connect the mesh with one geometry and the rectangle as a domain for
  // the coarse element - with another to see what'll happen
  const double x0 = -1;
  const double x1 = 15;
  const double y0 = 0.1;
  const double y1 = 0.11;
  const int    nx = 10;
  const int    ny = 11;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  const Rectangle rectangle; // default constructor

  const int number = 0;
  CoarseRectangle coarse_rect(number, mesh, rectangle);
  EXPECT_EQ(coarse_rect.number(), number);
}

// =============================================================================
TEST(CoarseRectangle, ctor_0_0_build_fine_mesh)
{
  // we connect the mesh with one geometry and the rectangle as a domain for
  // the coarse element - with another to see what'll happen
  const double x0 = -1;
  const double x1 = 15;
  const double y0 = 0.1;
  const double y1 = 0.11;
  const int    nx = 10;
  const int    ny = 11;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  const Rectangle rectangle; // default constructor

  const int number = 0;
  CoarseRectangle coarse_rect(number, mesh, rectangle);
  EXPECT_EQ(coarse_rect.number(), number);

  coarse_rect.build_fine_mesh();
}

// =============================================================================
TEST(CoarseRectangle, ctor_1_0)
{
  // we connect the mesh with one geometry and the rectangle as a domain for
  // the coarse element - with another to see what'll happen
  const double x0 = -1;
  const double x1 = 15;
  const double y0 = 0.1;
  const double y1 = 0.11;
  const int    nx = 10;
  const int    ny = 11;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  std::vector<int> ver;
  ver.push_back(0);
  ver.push_back(1);
  ver.push_back(5);
  ver.push_back(10);
  const int rect_number = 18;
  const int mat_id = -7;
  const int part_id = 98;
  const Rectangle rectangle(ver, rect_number, mat_id, part_id);

  const int number = 0;
  CoarseRectangle coarse_rect(number, mesh, rectangle);
  EXPECT_EQ(coarse_rect.number(), number);
}

// =============================================================================
TEST(CoarseRectangle, ctor_1_0_build_fine_mesh)
{
  // we connect the mesh with one geometry and the rectangle as a domain for
  // the coarse element - with another to see what'll happen
  const double x0 = -1;
  const double x1 = 15;
  const double y0 = 0.1;
  const double y1 = 0.11;
  const int    nx = 10;
  const int    ny = 11;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  std::vector<int> ver;
  ver.push_back(0);
  ver.push_back(1);
  ver.push_back(5);
  ver.push_back(10);
  const int rect_number = 18;
  const int mat_id = -7;
  const int part_id = 98;
  const Rectangle rectangle(ver, rect_number, mat_id, part_id);

  const int number = 0;
  CoarseRectangle coarse_rect(number, mesh, rectangle);
  EXPECT_EQ(coarse_rect.number(), number);

  coarse_rect.build_fine_mesh();
}

// =============================================================================
TEST(CoarseRectangle, ctor_2_0)
{
  // we connect the mesh with one geometry and the rectangle as a domain for
  // the coarse element - with another to see what'll happen
  const double x0 = -1;
  const double x1 = 15;
  const double y0 = 0.1;
  const double y1 = 0.11;
  const int    nx = 10;
  const int    ny = 11;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  const int v1 = 0;
  const int v2 = 1;
  const int v3 = 5;
  const int v4 = 10;
  const int rect_number = 18;
  const int mat_id = -7;
  const int part_id = 98;
  const Rectangle rectangle(v1, v2, v3, v4, rect_number, mat_id, part_id);

  const int number = 0;
  CoarseRectangle coarse_rect(number, mesh, rectangle);
  EXPECT_EQ(coarse_rect.number(), number);
}

// =============================================================================
TEST(CoarseRectangle, ctor_2_0_build_fine_mesh)
{
  // we connect the mesh with one geometry and the rectangle as a domain for
  // the coarse element - with another to see what'll happen
  const double x0 = -1;
  const double x1 = 15;
  const double y0 = 0.1;
  const double y1 = 0.11;
  const int    nx = 10;
  const int    ny = 11;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  const int v1 = 0;
  const int v2 = 1;
  const int v3 = 5;
  const int v4 = 10;
  const int rect_number = 18;
  const int mat_id = -7;
  const int part_id = 98;
  const Rectangle rectangle(v1, v2, v3, v4, rect_number, mat_id, part_id);

  const int number = 0;
  CoarseRectangle coarse_rect(number, mesh, rectangle);
  EXPECT_EQ(coarse_rect.number(), number);

  coarse_rect.build_fine_mesh();
}

#endif // FEMPLUS_TEST_COARSE_RECTANGLE_HPP
