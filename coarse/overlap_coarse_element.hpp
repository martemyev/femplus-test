#ifndef FEMPLUS_TEST_OVERLAP_COARSE_ELEMENT_HPP
#define FEMPLUS_TEST_OVERLAP_COARSE_ELEMENT_HPP

#include "config.hpp"
#include "gtest/gtest.h"
#include "compare_functions.hpp"

#include "femplus/overlap_coarse_element.hpp"
#include "femplus/mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/rectangle.hpp"

using namespace femplus;

// =============================================================================
TEST(OverlapCoarseRectangle, build_in_extended_domain_overlap1)
{
  const double x0 = 0;
  const double x1 = 15;
  const double y0 = -10;
  const double y1 = 5;
  const int    nx = 10;
  const int    ny = 10;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  const int n_elems_overlap = 1;
  OverlapCoarseElement over_coarse_rect(number, mesh, n_elems_overlap);
  EXPECT_EQ(over_coarse_rect.number(), number);

  over_coarse_rect.build_fine_mesh();

  const std::string fname = "overlap_coarse_rectangle_0.msh";
  over_coarse_rect.fine_mesh()->write_msh(fname);
}

// =============================================================================
TEST(OverlapCoarseRectangle, build_in_extended_domain_overlap4)
{
  const double x0 = 0;
  const double x1 = 15;
  const double y0 = -10;
  const double y1 = 5;
  const int    nx = 10;
  const int    ny = 10;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  const int n_elems_overlap = 4;
  OverlapCoarseElement over_coarse_rect(number, mesh, n_elems_overlap);
  EXPECT_EQ(over_coarse_rect.number(), number);

  over_coarse_rect.build_fine_mesh();

  const std::string fname = "overlap_coarse_rectangle_1.msh";
  over_coarse_rect.fine_mesh()->write_msh(fname);
}

// =============================================================================
TEST(OverlapCoarsePolygon, build_in_extended_domain_overlap1)
{
  const double x0 = 0;
  const double x1 = 15;
  const double y0 = -10;
  const double y1 = 5;
  const int    nx = 10;

  std::vector<Point> geo_vertices;
  geo_vertices.push_back(Point(x0, y0));
  geo_vertices.push_back(Point(x1, y0));
  geo_vertices.push_back(Point(x1, y1));
  geo_vertices.push_back(Point(x0, y1));

  std::shared_ptr<Mesh> mesh(new TriangularMesh(geo_vertices, nx));

  const int number = 0;
  const int n_elems_overlap = 1;
  OverlapCoarseElement over_coarse_poly(number, mesh, n_elems_overlap);
  EXPECT_EQ(over_coarse_poly.number(), number);

  over_coarse_poly.build_fine_mesh();

  const std::string fname = "overlap_coarse_polygon_0.msh";
  over_coarse_poly.fine_mesh()->write_msh(fname);
}

// =============================================================================
TEST(OverlapCoarsePolygon, build_in_extended_domain_overlap2)
{
  const double x0 = 0;
  const double x1 = 15;
  const double y0 = -10;
  const double y1 = 5;
  const int    nx = 10;

  std::vector<Point> geo_vertices;
  geo_vertices.push_back(Point(x0, y0));
  geo_vertices.push_back(Point(x1, y0));
  geo_vertices.push_back(Point(x1, y1));
  geo_vertices.push_back(Point(x0, y1));

  std::shared_ptr<Mesh> mesh(new TriangularMesh(geo_vertices, nx));

  const int number = 0;
  const int n_elems_overlap = 2;
  OverlapCoarseElement over_coarse_poly(number, mesh, n_elems_overlap);
  EXPECT_EQ(over_coarse_poly.number(), number);

  over_coarse_poly.build_fine_mesh();

  const std::string fname = "overlap_coarse_polygon_1.msh";
  over_coarse_poly.fine_mesh()->write_msh(fname);
}

// =============================================================================
TEST(OverlapCoarsePolygon, build_in_extended_domain_overlap4)
{
  const double x0 = 0;
  const double x1 = 15;
  const double y0 = -10;
  const double y1 = 5;
  const int    nx = 10;

  std::vector<Point> geo_vertices;
  geo_vertices.push_back(Point(x0, y0));
  geo_vertices.push_back(Point(x1, y0));
  geo_vertices.push_back(Point(x1, y1));
  geo_vertices.push_back(Point(x0, y1));

  std::shared_ptr<Mesh> mesh(new TriangularMesh(geo_vertices, nx));

  const int number = 0;
  const int n_elems_overlap = 4;
  OverlapCoarseElement over_coarse_poly(number, mesh, n_elems_overlap);
  EXPECT_EQ(over_coarse_poly.number(), number);

  over_coarse_poly.build_fine_mesh();

  const std::string fname = "overlap_coarse_polygon_2.msh";
  over_coarse_poly.fine_mesh()->write_msh(fname);
}

// =============================================================================
TEST(OverlapCoarsePolygon, build_in_extended_curved_domain_overlap2)
{
  std::vector<Point> p;
  p.push_back(Point(-1.1, 0.5));
  p.push_back(Point(-0.9, 0.6));
  p.push_back(Point(-0.8, 0.8));
  p.push_back(Point(-0.6, 0.7));
  p.push_back(Point(-0.6, 0.6));
  p.push_back(Point(-0.4, 0.4));
  p.push_back(Point(-0.3, 0.2));
  p.push_back(Point(-0.1, 0.2));
  p.push_back(Point(0, 0.4));
  p.push_back(Point(0.1, 0.5));
  p.push_back(Point(0.2, 0.8));
  p.push_back(Point(0.4, 0.9));
  p.push_back(Point(0.5, 0.8));
  p.push_back(Point(0.6, 0.6));
  p.push_back(Point(0.7, 0.5));
  p.push_back(Point(0.8, 0.3));
  p.push_back(Point(0.9, 0.4));
  p.push_back(Point(1.1, 0.5));
  p.push_back(Point(1.3, 0.6));
  p.push_back(Point(1.4, 0.6));
  p.push_back(Point(1.5, 0.4));
  p.push_back(Point(1.5, -0.9));
  p.push_back(Point(-1.2, -0.9));

  const int nx = 10;

  std::shared_ptr<Mesh> mesh(new TriangularMesh(p, nx));

  const int number = 0;
  const int n_elems_overlap = 2;
  OverlapCoarseElement over_coarse_poly(number, mesh, n_elems_overlap);
  EXPECT_EQ(over_coarse_poly.number(), number);

  over_coarse_poly.build_fine_mesh();

  const std::string fname = "overlap_coarse_curved_polygon.msh";
  over_coarse_poly.fine_mesh()->write_msh(fname);
}

// =============================================================================
TEST(OverlapCoarseRectangle, extract_original_mesh_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  const int n_elems_overlap = 1;
  OverlapCoarseElement over_coarse_rect(number, mesh, n_elems_overlap);
  EXPECT_EQ(over_coarse_rect.number(), number);

  over_coarse_rect.build_fine_mesh();
  std::shared_ptr<Mesh> orig_mesh(over_coarse_rect.extract_original_mesh());

  std::string fname = "overlap_coarse_rectangle_after_extraction_0.msh";
  over_coarse_rect.fine_mesh()->write_msh(fname);

  fname = "overlap_coarse_rectangle_extracted_0.msh";
  orig_mesh->write_msh(fname);

  std::vector<Point> vertices;
  vertices.push_back(Point(0, 0));
  vertices.push_back(Point(1, 0));
  vertices.push_back(Point(0, 1));
  vertices.push_back(Point(1, 1));
  compare_points(*orig_mesh.get(), vertices);

  std::vector<MeshElementPtr> elements;
  elements.push_back(MeshElementPtr(new Rectangle(0, 1, 2, 3)));
  const int start_from = 0;
  compare_elements(*orig_mesh.get(), elements, vertices, start_from);
}

// =============================================================================
TEST(OverlapCoarseRectangle, extract_original_mesh_1)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  const int n_elems_overlap = 10;
  OverlapCoarseElement over_coarse_rect(number, mesh, n_elems_overlap);
  EXPECT_EQ(over_coarse_rect.number(), number);

  over_coarse_rect.build_fine_mesh();
  std::shared_ptr<Mesh> orig_mesh(over_coarse_rect.extract_original_mesh());

  std::string fname = "overlap_coarse_rectangle_after_extraction_1.msh";
  over_coarse_rect.fine_mesh()->write_msh(fname);

  fname = "overlap_coarse_rectangle_extracted_1.msh";
  orig_mesh->write_msh(fname);

  std::vector<Point> vertices;
  vertices.push_back(Point(0, 0));
  vertices.push_back(Point(1, 0));
  vertices.push_back(Point(0, 1));
  vertices.push_back(Point(1, 1));
  compare_points(*orig_mesh.get(), vertices);

  std::vector<MeshElementPtr> elements;
  elements.push_back(MeshElementPtr(new Rectangle(0, 1, 2, 3)));
  const int start_from = 0;
  compare_elements(*orig_mesh.get(), elements, vertices, start_from);
}

// =============================================================================
TEST(OverlapCoarseRectangle, extract_original_mesh_2)
{
  const double x0 = 0;
  const double x1 = 2;
  const double y0 = 0;
  const double y1 = 2;
  const int    nx = 2;
  const int    ny = 2;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  const int n_elems_overlap = 10;
  OverlapCoarseElement over_coarse_rect(number, mesh, n_elems_overlap);
  EXPECT_EQ(over_coarse_rect.number(), number);

  over_coarse_rect.build_fine_mesh();
  std::shared_ptr<Mesh> orig_mesh(over_coarse_rect.extract_original_mesh());

  std::string fname = "overlap_coarse_rectangle_after_extraction_2.msh";
  over_coarse_rect.fine_mesh()->write_msh(fname);

  fname = "overlap_coarse_rectangle_extracted_2.msh";
  orig_mesh->write_msh(fname);

  std::vector<Point> vertices;
  vertices.push_back(Point(0, 0));
  vertices.push_back(Point(1, 0));
  vertices.push_back(Point(0, 1));
  vertices.push_back(Point(1, 1));
  vertices.push_back(Point(2, 0));
  vertices.push_back(Point(2, 1));
  vertices.push_back(Point(0, 2));
  vertices.push_back(Point(1, 2));
  vertices.push_back(Point(2, 2));
  compare_points(*orig_mesh.get(), vertices);

  std::vector<MeshElementPtr> elements;
  elements.push_back(MeshElementPtr(new Rectangle(0, 1, 2, 3)));
  elements.push_back(MeshElementPtr(new Rectangle(1, 4, 3, 5)));
  elements.push_back(MeshElementPtr(new Rectangle(2, 3, 6, 7)));
  elements.push_back(MeshElementPtr(new Rectangle(3, 5, 7, 8)));
  const int start_from = 0;
  compare_elements(*orig_mesh.get(), elements, vertices, start_from);
}

// =============================================================================
TEST(OverlapCoarsePolygon, extract_original_mesh)
{
  std::vector<Point> p;
  p.push_back(Point(-1.1, 0.5));
  p.push_back(Point(-0.9, 0.6));
  p.push_back(Point(-0.8, 0.8));
  p.push_back(Point(-0.6, 0.7));
  p.push_back(Point(-0.6, 0.6));
  p.push_back(Point(-0.4, 0.4));
  p.push_back(Point(-0.3, 0.2));
  p.push_back(Point(-0.1, 0.2));
  p.push_back(Point(0, 0.4));
  p.push_back(Point(0.1, 0.5));
  p.push_back(Point(0.2, 0.8));
  p.push_back(Point(0.4, 0.9));
  p.push_back(Point(0.5, 0.8));
  p.push_back(Point(0.6, 0.6));
  p.push_back(Point(0.7, 0.5));
  p.push_back(Point(0.8, 0.3));
  p.push_back(Point(0.9, 0.4));
  p.push_back(Point(1.1, 0.5));
  p.push_back(Point(1.3, 0.6));
  p.push_back(Point(1.4, 0.6));
  p.push_back(Point(1.5, 0.4));
  p.push_back(Point(1.5, -0.9));
  p.push_back(Point(-1.2, -0.9));

  const int nx = 10;

  std::shared_ptr<Mesh> mesh(new TriangularMesh(p, nx));

  const int number = 0;
  const int n_elems_overlap = 2;
  OverlapCoarseElement over_coarse_poly(number, mesh, n_elems_overlap);
  EXPECT_EQ(over_coarse_poly.number(), number);

  over_coarse_poly.build_fine_mesh();
  std::shared_ptr<Mesh> orig_mesh(over_coarse_poly.extract_original_mesh());

  std::string fname = "overlap_coarse_poly_after_extraction.msh";
  over_coarse_poly.fine_mesh()->write_msh(fname);

  fname = "overlap_coarse_poly_extracted.msh";
  orig_mesh->write_msh(fname);
}

#endif // FEMPLUS_TEST_OVERLAP_COARSE_ELEMENT_HPP
