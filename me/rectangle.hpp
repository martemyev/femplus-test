#ifndef TEST_FEMPLUS_RECTANGLE_HPP
#define TEST_FEMPLUS_RECTANGLE_HPP

#include "config.hpp"
#include "femplus/mesh.hpp"
#include "femplus/rectangle.hpp"
#include <gtest/gtest.h>


using namespace femplus;

// =======================================================
TEST(Rectangle, ctor_0)
{
  Rectangle rect;
  EXPECT_EQ(rect.dim(), 2);
  EXPECT_EQ(rect.n_vertices(), 4);
  EXPECT_EQ(rect.n_edges(), 4);
  EXPECT_EQ(rect.n_faces(), 1);
  EXPECT_EQ(rect.type_id("gmsh"), 3);
  EXPECT_EQ(rect.material_id(), 0);
  EXPECT_EQ(rect.partition_id(), 0);
  EXPECT_EQ(rect.number(), 0);
}

// =======================================================
TEST(Rectangle, ctor_0_numbers)
{
  Rectangle rect;
  for (int i = 0; i < 4; ++i)
  {
    EXPECT_ANY_THROW(rect.vertex_number(i)); // vertices are not initialized
    EXPECT_ANY_THROW(rect.edge_number(i)); // edges are not initialized
  }
  EXPECT_EQ(rect.face_number(0), 0);
}

// =======================================================
TEST(Rectangle, ctor_0_copyctor)
{
  Rectangle r1;
  Rectangle r2(r1);
  EXPECT_EQ(r1.dim(),           r2.dim());
  EXPECT_EQ(r1.n_vertices(),    r2.n_vertices());
  EXPECT_EQ(r1.n_edges(),       r2.n_edges());
  EXPECT_EQ(r1.n_faces(),       r2.n_faces());
  EXPECT_EQ(r1.type_id("gmsh"), r2.type_id("gmsh"));
  EXPECT_EQ(r1.material_id(),   r2.material_id());
  EXPECT_EQ(r1.partition_id(),  r2.partition_id());
  Rectangle r3 = r1;
  EXPECT_EQ(r1.dim(),           r3.dim());
  EXPECT_EQ(r1.n_vertices(),    r3.n_vertices());
  EXPECT_EQ(r1.n_edges(),       r3.n_edges());
  EXPECT_EQ(r1.n_faces(),       r3.n_faces());
  EXPECT_EQ(r1.type_id("gmsh"), r3.type_id("gmsh"));
  EXPECT_EQ(r1.material_id(),   r3.material_id());
  EXPECT_EQ(r1.partition_id(),  r3.partition_id());
}

// =======================================================
TEST(Rectangle, ctor_0_assingment)
{
  Rectangle r1;
  Rectangle r2;
  r2 = r1;
  EXPECT_EQ(r1.dim(),           r2.dim());
  EXPECT_EQ(r1.n_vertices(),    r2.n_vertices());
  EXPECT_EQ(r1.n_edges(),       r2.n_edges());
  EXPECT_EQ(r1.n_faces(),       r2.n_faces());
  EXPECT_EQ(r1.type_id("gmsh"), r2.type_id("gmsh"));
  EXPECT_EQ(r1.material_id(),   r2.material_id());
  EXPECT_EQ(r1.partition_id(),  r2.partition_id());
}

// =======================================================
TEST(Rectangle, ctor_1)
{
  std::vector<int> vertices;
  vertices.push_back(1);
  vertices.push_back(12);
  vertices.push_back(31);
  vertices.push_back(621);
  const int mat_id = 18;
  const int par_id = -45;

  Rectangle rect(vertices, mat_id, par_id);
  for (int i = 0; i < 4; ++i)
  {
    EXPECT_EQ(rect.vertex_number(i), vertices[i]);
    EXPECT_ANY_THROW(rect.edge_number(i)); // edges are not initialized
  }
  EXPECT_EQ(rect.face_number(0), 0);
}

// =======================================================
TEST(Rectangle, build1)
{
  Rectangle *rect;

  std::vector<int> vertices;
  vertices.push_back(0);
  vertices.push_back(1);
  vertices.push_back(2);

  // test: too few vertices
  EXPECT_ANY_THROW(rect = new Rectangle(vertices));

  vertices.push_back(3);

  // should be okay
  EXPECT_NO_THROW(rect = new Rectangle(vertices));

  std::vector<int>::const_iterator iter = vertices.begin();
  for (int i = 0; iter != vertices.end(); ++iter, ++i)
    EXPECT_EQ(*iter, rect->vertex_number(i));

  delete rect;

  vertices.push_back(4);

  // test: too many vertices
  EXPECT_ANY_THROW(rect = new Rectangle(vertices));
}

// =======================================================
TEST(Rectangle, build2)
{
  Rectangle rect;

  std::vector<int> vertices;
  vertices.push_back(0);
  vertices.push_back(1);
  vertices.push_back(2);

  // test: too few vertices
  EXPECT_ANY_THROW(rect.set_vertices_numbers(vertices));

  vertices.push_back(3);

  // should be okay
  EXPECT_NO_THROW(rect.set_vertices_numbers(vertices));

  std::vector<int>::const_iterator iter = vertices.begin();
  for (int i = 0; iter != vertices.end(); ++iter, ++i)
    EXPECT_EQ(*iter, rect.vertex_number(i));

  vertices.push_back(4);

  // test: too many vertices
  EXPECT_ANY_THROW(rect.set_vertices_numbers(vertices));
}

// =======================================================
TEST(Rectangle, counteclockwise1)
{
  std::vector<int> vertices;
  vertices.push_back(0);
  vertices.push_back(1);
  vertices.push_back(2);
  vertices.push_back(3);
  Rectangle rect(vertices);

  std::vector<Point> points;
  points.push_back(Point(0, 0));
  points.push_back(Point(1, 0));
  points.push_back(Point(1, 1));
  points.push_back(Point(0, 1));

  std::vector<int> vert_sorted;
  rect.vertices_counterclockwise(points, vert_sorted);

  const int vert_true[] = { 2, 3, 0, 1 };
  for (int i = 0; i < rect.n_vertices(); ++i)
    EXPECT_EQ(vert_true[i], vert_sorted[i]);
}

// =======================================================
TEST(Rectangle, counteclockwise2)
{
  std::vector<int> vertices;
  vertices.push_back(0);
  vertices.push_back(2);
  vertices.push_back(1);
  vertices.push_back(3);
  Rectangle rect(vertices);

  std::vector<Point> points;
  points.push_back(Point(0, 0));
  points.push_back(Point(1, 0));
  points.push_back(Point(1, 1));
  points.push_back(Point(0, 1));

  std::vector<int> vert_sorted;
  rect.vertices_counterclockwise(points, vert_sorted);

  const int vert_true[] = { 2, 3, 0, 1 };
  for (int i = 0; i < rect.n_vertices(); ++i)
    EXPECT_EQ(vert_true[i], vert_sorted[i]);
}

// =============================================================================
TEST(Rectangle, clone)
{
  std::vector<int> v_numbers;
  v_numbers.push_back(0);
  v_numbers.push_back(1);
  v_numbers.push_back(6);
  v_numbers.push_back(7);
  const int number = 45;
  const int matID = 17;
  const int parID = 78;
  MeshElement *r1 = new Rectangle(v_numbers, number, matID, parID);
  std::vector<int> e_numbers;
  e_numbers.push_back(3);
  e_numbers.push_back(7);
  e_numbers.push_back(2);
  e_numbers.push_back(1);
  r1->set_edges_numbers(e_numbers);
  std::vector<int> f_numbers;
  f_numbers.push_back(13);
  r1->set_faces_numbers(f_numbers);

  MeshElement *r2 = r1->clone();

  EXPECT_EQ(r1->dim(),           r2->dim());
  EXPECT_EQ(r1->n_vertices(),    r2->n_vertices());
  EXPECT_EQ(r1->n_edges(),       r2->n_edges());
  EXPECT_EQ(r1->n_faces(),       r2->n_faces());
  EXPECT_EQ(r1->type_id("gmsh"), r2->type_id("gmsh"));
  EXPECT_EQ(r1->material_id(),   r2->material_id());
  EXPECT_EQ(r1->partition_id(),  r2->partition_id());
  EXPECT_EQ(r1->number(),        r2->number());
  for (int i = 0; i < Rectangle::N_VERTICES; ++i)
    EXPECT_EQ(r1->vertex_number(i), r2->vertex_number(i));
  for (int i = 0; i < Rectangle::N_EDGES; ++i)
    EXPECT_EQ(r1->edge_number(i), r2->edge_number(i));
  for (int i = 0; i < Rectangle::N_FACES; ++i)
    EXPECT_EQ(r1->face_number(i), r2->face_number(i));

  // check that after deleting of one of the rectangles, another one is
  // accessible
  delete r1;

  for (int i = 0; i < Rectangle::N_VERTICES; ++i)
    EXPECT_EQ(r2->vertex_number(i), v_numbers[i]);
  for (int i = 0; i < Rectangle::N_EDGES; ++i)
    EXPECT_EQ(r2->edge_number(i), e_numbers[i]);
  for (int i = 0; i < Rectangle::N_FACES; ++i)
    EXPECT_EQ(r2->face_number(i), f_numbers[i]);

  delete r2;
}



#endif // TEST_FEMPLUS_RECTANGLE_HPP
