#ifndef TEST_FEMPLUS_ANALYTIC_FUNCTIONS_HPP
#define TEST_FEMPLUS_ANALYTIC_FUNCTIONS_HPP

#include "femplus/function.hpp"
#include "femplus/point.hpp"

double gauss(double mu, double s, double x);

// =============================================================================
//
// analytical functions of solution and right hand side (RHS)
//
// =============================================================================
class an_solution_0: public femplus::Function
{
public:
  virtual double value(const femplus::Point &p) const;
};
class rhs_function_0: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};
// -----------------------------------------------------------------------------
class an_solution_1: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};
class rhs_function_1: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};
// -----------------------------------------------------------------------------
class an_solution_2: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};
class rhs_function_2: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};
// -----------------------------------------------------------------------------
class an_solution_3: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};
class rhs_function_3: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};
// -----------------------------------------------------------------------------
class an_solution_4: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};
class rhs_function_4: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};
// -----------------------------------------------------------------------------
class an_solution_5: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};
class rhs_function_5: public femplus::Function
{
public:
  double value(const femplus::Point &p) const;
};


#endif // TEST_FEMPLUS_ANALYTIC_FUNCTIONS_HPP
