#ifndef FEMPLUS_TEST_AUXILIARY_FUNCTIONS_HPP
#define FEMPLUS_TEST_AUXILIARY_FUNCTIONS_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/auxiliary_functions.hpp"

#include <sstream>
#include <string>

// =============================================================================
TEST(Aux_functions, d2s_pointer)
{
  int a = 5; // some number
  int *p0 = &a;

  // visual output
  std::cout << "pointer address: " << p0 << std::endl;
  std::cout << "same with d2s  : " << d2s<int*>(p0) << std::endl;

  // rigorous
  std::ostringstream ss1, ss2;
  ss1 << p0;
  ss2 << d2s<int*>(p0);
  EXPECT_EQ(ss1.str(), ss2.str());

  double *data = new double[10];
  // visual output
  std::cout << "pointer address: " << &data[0] << std::endl;
  std::cout << "same with d2s  : " << d2s<double*>(data) << std::endl;

  // rigorous
  std::ostringstream ss3, ss4;
  ss3 << &data[0];
  ss4 << d2s<double*>(data);
  EXPECT_EQ(ss3.str(), ss4.str());

  delete[] data;
}

// =============================================================================
TEST(Aux_functions, file_name)
{
  std::string path = "/home/user/dir/filename.txt";
  std::string true_fname = "filename.txt";
  EXPECT_EQ(file_name(path), true_fname);

  path = "/home/user/dir/file.name.txt";
  true_fname = "file.name.txt";
  EXPECT_EQ(file_name(path), true_fname);
}

// =============================================================================
TEST(Aux_functions, file_stem)
{
  std::string path = "/home/user/dir/filename.txt";
  std::string true_stem = "filename";
  EXPECT_EQ(file_stem(path), true_stem);

  path = "/home/user/dir/file.name.txt";
  true_stem = "file.name";
  EXPECT_EQ(file_stem(path), true_stem);
}

// =============================================================================
TEST(Aux_functions, file_extension)
{
  std::string path = "/home/user/dir/filename.txt";
  std::string true_ext = ".txt";
  EXPECT_EQ(file_extension(path), true_ext);

  path = "/home/user/dir/file.name.txt";
  EXPECT_EQ(file_extension(path), true_ext);
}

// =============================================================================
TEST(Aux_functions, file_exists)
{
  EXPECT_TRUE(file_exists(PROJECT_DIR + "/tests.hpp"));
  EXPECT_FALSE(file_exists(PROJECT_DIR + "/tests_that_cant_exist.hpp"));
}

// =============================================================================
TEST(Aux_functions, file_path)
{
  std::string path = "/home/user/dir/filename.txt";
  EXPECT_EQ(file_path(path), "/home/user/dir/");

  path = "./file.name.txt";
  EXPECT_EQ(file_path(path), "./");

  path = "file.name.txt";
  EXPECT_EQ(file_path(path), "");
}

// =============================================================================
TEST(Aux_functions, add_space)
{
  const std::string str0 = "asd    ";
  const std::string str1 = "asd";

  EXPECT_EQ(add_space("asd", 7), str0);
  EXPECT_EQ(add_space("asd", 3), str1);
  EXPECT_EQ(add_space("asd", 2), str1);
}


#endif // FEMPLUS_TEST_AUXILIARY_FUNCTIONS_HPP
