#ifndef FEMPLUS_TEST_GRID_FUNCTION_HPP
#define FEMPLUS_TEST_GRID_FUNCTION_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/grid_function.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/math_functions.hpp"
#include "femplus/mesh_element.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/constant_function.hpp"

#include <fstream>

using namespace femplus;

//==============================================================================
TEST(GridFunction, read_data_at_cells)
{
  class SomeFunction: public Function {
  public:
    SomeFunction(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
      : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
    virtual double value(const Point &point) const {
      const double x = point.x(), y = point.y();
      const double pi = math::PI;
      return sin(k*pi*x/(x1-x0)) * sin(m*pi*y/(y1-y0));
    }
  private:
    double x0, x1, y0, y1;
    int k, m;
  };

  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int nx = 97;
  const int ny = 95;

  const std::string fname = TESTOUT_DIR + "grid_function_at_cells.dat";

  MeshPtr grid(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  grid->build();

  const int k = 10;
  const int m = 10;
  SomeFunction some_function(x0, x1, y0, y1, k, m);

  std::vector<double> values(grid->n_elements());
  for (int i = 0; i < grid->n_elements(); ++i)
  {
    const Point center = grid->element(i)->center(grid->vertices());
    values[i] = some_function.value(center);
  }

  std::ofstream out(fname.c_str());
  require(out, "File '" + fname + "' can't be opened");
  out.setf(std::ios::scientific);
  out.precision(16);
  out << nx << " " << ny << "\n";
  for (int i = 0; i < grid->n_elements(); ++i)
    out << values[i] << "\n";
  out.close();

  GridFunction grid_function(fname);
  // when we read the values at vertices, an exception is thrown
  EXPECT_ANY_THROW(grid_function.read_data(grid->min_coord(), grid->max_coord(),
                                           nx, ny, GridFunction::AT_VERTICES));
  // when we read the values at cells, it's okay
  grid_function.read_data(grid->min_coord(), grid->max_coord(),
                          nx, ny, GridFunction::AT_CELLS);

  for (int i = 0; i < grid->n_elements(); ++i)
  {
    const MeshElement *cell = grid->element(i);
    const Point center = cell->center(grid->vertices());
    grid_function.set_material_id(cell->material_id());
    EXPECT_DOUBLE_EQ(grid_function.value(center), values[i]);
  }
}

//==============================================================================
TEST(GridFunction, read_data_at_vertices)
{
  class SomeFunction: public Function {
  public:
    SomeFunction(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
      : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
    virtual double value(const Point &point) const {
      const double x = point.x(), y = point.y();
      const double pi = math::PI;
      return sin(k*pi*x/(x1-x0)) * sin(m*pi*y/(y1-y0));
    }
  private:
    double x0, x1, y0, y1;
    int k, m;
  };

  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int nx = 98;
  const int ny = 99;

  const std::string fname = TESTOUT_DIR + "grid_function_at_vertices.dat";

  MeshPtr grid(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  grid->build();

  const int k = 10;
  const int m = 10;
  SomeFunction some_function(x0, x1, y0, y1, k, m);

  std::vector<double> values(grid->n_vertices());
  for (int i = 0; i < grid->n_vertices(); ++i)
    values[i] = some_function.value(grid->vertex(i));

  std::ofstream out(fname.c_str());
  require(out, "File '" + fname + "' can't be opened");
  out.setf(std::ios::scientific);
  out.precision(16);
  out << nx + 1 << " " << ny + 1 << "\n";
  for (int i = 0; i < (int)values.size(); ++i)
    out << values[i] << "\n";
  out.close();

  GridFunction grid_function(fname);
  // when we read the values at cells, an exception is thrown
  EXPECT_ANY_THROW(grid_function.read_data(grid->min_coord(), grid->max_coord(),
                                           nx, ny, GridFunction::AT_CELLS));
  // when we read the values at vertices, it's okay
  grid_function.read_data(grid->min_coord(), grid->max_coord(),
                          nx, ny, GridFunction::AT_VERTICES);

  for (int i = 0; i < grid->n_elements(); ++i)
  {
    const MeshElement *cell = grid->element(i);
    const Point center = cell->center(grid->vertices());
    double value = 0;
    for (int i = 0; i < cell->n_vertices(); ++i)
      value += values[cell->vertex_number(i)];
    grid_function.set_material_id(cell->material_id());
    EXPECT_DOUBLE_EQ(grid_function.value(center), 0.25*value);
  }
}

//==============================================================================
TEST(GridFunction, read_data_at_vertices_const_func)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int nx = 25;
  const int ny = 26;

  const std::string fname = TESTOUT_DIR + "grid_function_at_vertices_const_func.dat";

  MeshPtr grid(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  grid->build();

  const double const_value = 25.;
  ConstantFunction some_function(const_value);

  std::vector<double> values(grid->n_vertices());
  for (int i = 0; i < grid->n_vertices(); ++i)
    values[i] = some_function.value(grid->vertex(i));

  std::ofstream out(fname.c_str());
  require(out, "File '" + fname + "' can't be opened");
  out.setf(std::ios::scientific);
  out.precision(16);
  out << nx + 1 << " " << ny + 1 << "\n";
  for (int i = 0; i < (int)values.size(); ++i)
    out << values[i] << "\n";
  out.close();

  GridFunction grid_function(fname);
  // when we read the values at cells, an exception is thrown
  EXPECT_ANY_THROW(grid_function.read_data(grid->min_coord(), grid->max_coord(),
                                           nx, ny, GridFunction::AT_CELLS));
  // when we read the values at vertices, it's okay
  grid_function.read_data(grid->min_coord(), grid->max_coord(),
                          nx, ny, GridFunction::AT_VERTICES);

  for (int i = 0; i < grid->n_elements(); ++i)
  {
    const MeshElement *cell = grid->element(i);
    const Point center = cell->center(grid->vertices());
    grid_function.set_material_id(cell->material_id());
    EXPECT_DOUBLE_EQ(grid_function.value(center), const_value);
  }
}

#endif // FEMPLUS_TEST_GRID_FUNCTION_HPP
