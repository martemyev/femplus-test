#include "analytic_functions.hpp"
#include "femplus/math_functions.hpp"

using namespace femplus;



double gauss(double mu, double s, double x)
{
  return (1./(s * sqrt(2.*math::PI)) * exp(-(x-mu)*(x-mu)/(2.*s*s)));
}



// -----------------------------------------------------------------------------
double an_solution_0::value(const Point &p) const { return 1; }

double rhs_function_0::value(const Point &p) const { return 0; }

// -----------------------------------------------------------------------------
double an_solution_1::value(const Point &p) const
{
  const double x = p.coord(0);
  const double y = p.coord(1);
  return x + y;
}

double rhs_function_1::value(const Point &p) const { return 0; }

// -----------------------------------------------------------------------------
double an_solution_2::value(const Point &p) const
{
  const double x = p.coord(0);
  const double y = p.coord(1);
  return x * y;
}

double rhs_function_2::value(const Point &p) const { return 0; }

// -----------------------------------------------------------------------------
double an_solution_3::value(const Point &p) const
{
  const double x = p.coord(0);
  const double y = p.coord(1);
  return x*x + y*y;
}

double rhs_function_3::value(const Point &p) const { return -4.; }

// -----------------------------------------------------------------------------
double an_solution_4::value(const Point &p) const
{
  const double x = p.coord(0);
  const double y = p.coord(1);
  return sin(x) + sin(y);
}

double rhs_function_4::value(const Point &p) const
{
  const double x = p.coord(0);
  const double y = p.coord(1);
  return sin(x) + sin(y);
}

// -----------------------------------------------------------------------------
double an_solution_5::value(const Point &p) const
{
  const double x = p.coord(0);
  return exp(x);
}

double rhs_function_5::value(const Point &p) const
{
  const double x = p.coord(0);
  return -exp(x);
}





