#include <iostream>
#include "tests.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "slepc.h"



int main(int argc, char **argv)
{
  SlepcInitialize(&argc, &argv, NULL, NULL);

  // time measurement
  double t_begin = get_wall_time();

  // launch all tests
  std::cout << "\n\nTESTING\n";
  ::testing::InitGoogleTest(&argc, argv);
  int test_ret = RUN_ALL_TESTS();
  std::cout << "\nTesting procedures finished ("
            << test_ret << " is returned)\n\n";

  double t_end = get_wall_time();
  std::cout << "TOTAL TIME: " << t_end - t_begin << " sec" << std::endl;

  SlepcFinalize();

  return 0;
}

