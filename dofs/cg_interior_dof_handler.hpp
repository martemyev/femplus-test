#ifndef TEST_FEMPLUS_CG_INTERIOR_DOF_HANDLER_HPP
#define TEST_FEMPLUS_CG_INTERIOR_DOF_HANDLER_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/cg_interior_dof_handler.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/vector.hpp"
#include "femplus/finite_element.hpp"
#include "femplus/boundary_node.hpp"

using namespace femplus;

// =============================================================================
TEST(CGInteriorDoFHandler, distribute_dofs_scalar_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 1;
  const int ny = 1;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  CGInteriorDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  // there are no dofs, because there should be only interior dofs, but here
  // there are only boundary ones
  EXPECT_EQ(dof_handler.n_dofs(), 0);
  EXPECT_EQ(dof_handler.n_boundary_dofs(), 4);

  const int dofs[][4] = { { 0, -1, -2, -3 } };
  const int b_dofs[] = { 0, -1, -2, -3 };

  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    for (int d = 0; d < fe_mesh->element(el)->n_dofs(); ++d)
      EXPECT_EQ(fe_mesh->element(el)->dof_number(d), dofs[el][d]);
  }

  for (int bd = 0; bd < dof_handler.n_boundary_dofs(); ++bd)
    EXPECT_EQ(dof_handler.boundary_dof(bd).number(), b_dofs[bd]);
}

// =============================================================================
TEST(CGInteriorDoFHandler, distribute_dofs_vecto2_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 1;
  const int ny = 1;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGInteriorDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  // there are no dofs, because there should be only interior dofs, but here
  // there are only boundary ones
  EXPECT_EQ(dof_handler.n_dofs(), 0);
  EXPECT_EQ(dof_handler.n_boundary_dofs(), 8);

  const int dofs[][8] = { { 0, -1, -2, -3, -4, -5, -6, -7 } };
  const int b_dofs[] = { 0, -1, -2, -3, -4, -5, -6, -7 };

  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    for (int d = 0; d < fe_mesh->element(el)->n_dofs(); ++d)
      EXPECT_EQ(fe_mesh->element(el)->dof_number(d), dofs[el][d]);
  }

  for (int bd = 0; bd < dof_handler.n_boundary_dofs(); ++bd)
    EXPECT_EQ(dof_handler.boundary_dof(bd).number(), b_dofs[bd]);
}

// =============================================================================
TEST(CGInteriorDoFHandler, distribute_dofs_scalar_1)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 2;
  const int ny = 1;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  std::shared_ptr<DoFHandler> dof_handler(new CGInteriorDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();

  // there are no dofs, because there should be only interior dofs, but here
  // there are only boundary ones
  EXPECT_EQ(dof_handler->n_dofs(), 0);
  EXPECT_EQ(dof_handler->n_boundary_dofs(), 6);

  const int dofs[][4] = { { 0, -1, -3, -4 },
                          { -1, -2, -4, -5 } };
  const int b_dofs[] = { 0, -1, -2, -3, -4, -5 };

  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    for (int d = 0; d < fe_mesh->element(el)->n_dofs(); ++d)
      EXPECT_EQ(fe_mesh->element(el)->dof_number(d), dofs[el][d]);
  }

  for (int bd = 0; bd < dof_handler->n_boundary_dofs(); ++bd)
    EXPECT_EQ(dof_handler->boundary_dof(bd).number(), b_dofs[bd]);
}

// =============================================================================
TEST(CGInteriorDoFHandler, distribute_dofs_vector2_1)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 2;
  const int ny = 1;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  std::shared_ptr<DoFHandler> dof_handler(new CGInteriorDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();

  // there are no dofs, because there should be only interior dofs, but here
  // there are only boundary ones
  EXPECT_EQ(dof_handler->n_dofs(), 0);
  EXPECT_EQ(dof_handler->n_boundary_dofs(), 12);

  const int dofs[][8] = { { 0, -1, -2, -3, -6, -7, -8, -9 },
                          { -2, -3, -4, -5, -8, -9, -10, -11 } };
  const int b_dofs[] = { 0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11 };

  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    for (int d = 0; d < fe_mesh->element(el)->n_dofs(); ++d)
      EXPECT_EQ(fe_mesh->element(el)->dof_number(d), dofs[el][d]);
  }

  for (int bd = 0; bd < dof_handler->n_boundary_dofs(); ++bd)
    EXPECT_EQ(dof_handler->boundary_dof(bd).number(), b_dofs[bd]);
}

// =============================================================================
TEST(CGInteriorDoFHandler, distribute_dofs_scalar_2)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 1;
  const int ny = 10;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  DoFHandlerPtr dof_handler(new CGInteriorDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();

  // there are no dofs, because there should be only interior dofs, but here
  // there are only boundary ones
  EXPECT_EQ(dof_handler->n_dofs(), 0);

  // all boundary dofs have indices -1 (and since all dofs are boudnary, all of
  // them have dofs with number -1)
  for (int el = 0; el < fe_mesh->n_elements(); ++el)
    for (int d = 0; d < fe_mesh->element(el)->n_dofs(); ++d)
      EXPECT_EQ(fe_mesh->element(el)->dof_number(d), -1);
}

// =============================================================================
TEST(CGInteriorDoFHandler, scalar_distribute_dofs_3_and_extend)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 2;
  const int ny = 2;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  CGInteriorDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  EXPECT_EQ(dof_handler.n_dofs(), 1);
  EXPECT_EQ(dof_handler.n_boundary_dofs(), 8);

  const int dofs[][4] = { { -1, -2, -4, 0 },
                          { -2, -3, 0, -5 },
                          { -4, 0, -6, -7 },
                          { 0, -5, -7, -8 } };
  const int b_dofs[] = { -1, -2, -3, -4, -5, -6, -7, -8 };

  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    for (int d = 0; d < fe_mesh->element(el)->n_dofs(); ++d)
      EXPECT_EQ(fe_mesh->element(el)->dof_number(d), dofs[el][d]);
  }

  for (int bd = 0; bd < dof_handler.n_boundary_dofs(); ++bd)
    EXPECT_EQ(dof_handler.boundary_dof(bd).number(), b_dofs[bd]);

  std::vector<double> inter_values(1, 14);
  const Vector in_values(inter_values);

  const Vector ex_values = dof_handler.extend(in_values);
  std::vector<double> extend_values = ex_values.std_vector();

  std::vector<double> check_values(9, 0);
  check_values[4] = 14;
  EXPECT_EQ(check_values.size(), extend_values.size());
  for (unsigned i = 0; i < extend_values.size(); ++i)
    EXPECT_DOUBLE_EQ(check_values[i], extend_values[i]);
}

// =============================================================================
TEST(CGInteriorDoFHandler, vector2_distribute_dofs_3_and_extend)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 2;
  const int ny = 2;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGInteriorDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  EXPECT_EQ(dof_handler.n_dofs(), 2);
  EXPECT_EQ(dof_handler.n_boundary_dofs(), 16);

  const int dofs[][8] = { { -2, -3, -4, -5, -8, -9, 0, 1 },
                          { -4, -5, -6, -7, 0, 1, -10, -11 },
                          { -8, -9, 0, 1, -12, -13, -14, -15 },
                          { 0, 1, -10, -11, -14, -15, -16, -17 } };
  const int b_dofs[] = { -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12, -13,
                         -14, -15, -16, -17 };

  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    for (int d = 0; d < fe_mesh->element(el)->n_dofs(); ++d)
      EXPECT_EQ(fe_mesh->element(el)->dof_number(d), dofs[el][d]);
  }

  for (int bd = 0; bd < dof_handler.n_boundary_dofs(); ++bd)
    EXPECT_EQ(dof_handler.boundary_dof(bd).number(), b_dofs[bd]);

  Vector interior_values(2);
  interior_values(0) = 42.0;
  interior_values(1) = 24.0;

  const Vector extended_values = dof_handler.extend(interior_values);

  std::vector<double> check_values(18, 0);
  check_values[8] = 42.0;
  check_values[9] = 24.0;
  EXPECT_EQ(check_values.size(), extended_values.size());
  for (int i = 0; i < extended_values.size(); ++i)
    EXPECT_DOUBLE_EQ(check_values[i], extended_values(i));
}

// =============================================================================
TEST(CGInteriorDoFHandler, scalar_distribute_dofs_4_and_extend)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 3;
  const int ny = 3;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  CGInteriorDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  EXPECT_EQ(dof_handler.n_dofs(), 4);
  EXPECT_EQ(dof_handler.n_boundary_dofs(), 12);

  const int dofs[][4] = { { -4, -5, -8, 0 },
                          { -5, -6, 0, 1 },
                          { -6, -7, 1, -9 },
                          { -8, 0, -10, 2 },
                          { 0, 1, 2, 3 },
                          { 1, -9, 3, -11 },
                          { -10, 2, -12, -13 },
                          { 2, 3, -13, -14 },
                          { 3, -11, -14, -15 } };
  const int b_dofs[] = { -4, -5, -6, -7, -8, -9, -10, -11, -12, -13, -14, -15 };

  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    for (int d = 0; d < fe_mesh->element(el)->n_dofs(); ++d)
      EXPECT_EQ(fe_mesh->element(el)->dof_number(d), dofs[el][d]);
  }

  for (int bd = 0; bd < dof_handler.n_boundary_dofs(); ++bd)
    EXPECT_EQ(dof_handler.boundary_dof(bd).number(), b_dofs[bd]);

  std::vector<double> inter_values(4);
  inter_values[0] = -5;
  inter_values[1] = 7;
  inter_values[2] = 1.6;
  inter_values[3] = 45;
  const Vector in_values(inter_values);

  const Vector ex_values = dof_handler.extend(in_values);
  std::vector<double> extend_values = ex_values.std_vector();

  std::vector<double> check_values(16, 0);
  check_values[5] = -5;
  check_values[6] = 7;
  check_values[9] = 1.6;
  check_values[10] = 45;
  // std::vector version
  EXPECT_EQ(check_values.size(), extend_values.size());
  for (unsigned i = 0; i < extend_values.size(); ++i)
    EXPECT_EQ(check_values[i], extend_values[i]);
  // original Vector version
  EXPECT_EQ(check_values.size(), ex_values.size());
  for (int i = 0; i < ex_values.size(); ++i)
    EXPECT_EQ(check_values[i], ex_values(i));
}

// =============================================================================
// This test is visual - not rigorous. It produces a picture, which can be
// looked at ParaView.
// =============================================================================
TEST(CGInteriorDoFHandler, write_solution_vtu_scalar)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 3;
  const int ny = 3;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  std::shared_ptr<DoFHandler> dof_handler(new CGInteriorDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();

  EXPECT_EQ(dof_handler->n_dofs(), 4);
  EXPECT_EQ(dof_handler->n_boundary_dofs(), 12);

  // we provide solution only on the interior dofs, because it's 0 on the
  // boundary
  std::vector<Vector> solutions(2);
  Vector sol1(dof_handler->n_dofs());
  Vector sol2(dof_handler->n_dofs());
  for (int i = 0; i < dof_handler->n_dofs(); ++i)
  {
    sol1(i) = 1.;
    sol2(i) = -1.;
  }

  solutions[0] = sol1;
  solutions[1] = sol2;

  std::vector<std::string> sol_names(2);
  sol_names[0] = "sol_positive";
  sol_names[1] = "sol_negative";

  const std::string fname = TESTOUT_DIR +
                            "test_write_sol_by_inter_dof_handler.vtu";

  dof_handler->write_solution(fname, solutions, sol_names);
}


#endif // TEST_FEMPLUS_CG_INTERIOR_DOF_HANDLER_HPP
