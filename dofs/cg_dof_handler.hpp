#ifndef TEST_FEMPLUS_CG_DOF_HANDLER_HPP
#define TEST_FEMPLUS_CG_DOF_HANDLER_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/rectangle.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/cg_dof_handler.hpp"
#include "femplus/finite_element_mesh.hpp"
#include "femplus/finite_element.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/dense_pattern.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/solver.hpp"
#include "femplus/cg_interior_dof_handler.hpp"
#include "femplus/boundary_node.hpp"

#include "compare_functions.hpp"
#include "mat_coefficients.hpp"
#include "test_global_matrices.hpp"

#include <fstream>


using namespace femplus;

// =============================================================================
TEST(CGDoFHandler, distribute_dofs_scalar_fe_rect)
{
  const double x0 = 5;
  const double x1 = 10;
  const double y0 = -8;
  const double y1 = 17;
  const int    nx = 2;
  const int    ny = 3;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  const int n_dofs = (nx+1)*(ny+1);
  EXPECT_EQ(dof_handler.n_dofs(), n_dofs);

  const int dofs_in_elements[][Rectangle::N_VERTICES] =
  { { 0, 1, 3, 4 },
    { 1, 2, 4, 5 },
    { 3, 4, 6, 7 },
    { 4, 5, 7, 8 },
    { 6, 7, 9, 10 },
    { 7, 8, 10, 11 } };

  // compare the dofs indices in finite elements
  for (int i = 0; i < fe_mesh->n_elements(); ++i)
  {
    const FiniteElement *elem = fe_mesh->element(i);
    for (int j = 0; j < elem->n_dofs(); ++j)
      EXPECT_EQ(elem->dof_number(j), dofs_in_elements[i][j]);
  }

  const int bound_dofs[] = { 0, 1, 2, 3, 5, 6, 8, 9, 10, 11 };
  std::vector<BoundaryNode> bdofs;
  dof_handler.boundary_dofs(bdofs);
  EXPECT_EQ(bdofs.size(), 10);
  for (int i = 0; i < (int)bdofs.size(); ++i) {
    EXPECT_EQ(bdofs[i].number(), bound_dofs[i]);
    const int mat_id = bdofs[i].material_id();
    EXPECT_TRUE(mat_id == Mesh::LEFT_LINE ||
                mat_id == Mesh::RIGHT_LINE ||
                mat_id == Mesh::BOTTOM_LINE ||
                mat_id == Mesh::TOP_LINE);
  }
}

// =============================================================================
TEST(CGDoFHandler, distribute_dofs_vector2_fe_rect)
{
  const double x0 = 5;
  const double x1 = 10;
  const double y0 = -8;
  const double y1 = 17;
  const int    nx = 2;
  const int    ny = 3;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2; // vector FE with 2 dofs per one node
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  const int n_dofs = n_dofs_per_node*(nx+1)*(ny+1);
  EXPECT_EQ(dof_handler.n_dofs(), n_dofs);

  const int dofs_in_elements[][n_dofs_per_node*Rectangle::N_VERTICES] =
  { { 0, 1, 2, 3, 6, 7, 8, 9 },
    { 2, 3, 4, 5, 8, 9, 10, 11 },
    { 6, 7, 8, 9, 12, 13, 14, 15 },
    { 8, 9, 10, 11, 14, 15, 16, 17 },
    { 12, 13, 14, 15, 18, 19, 20, 21 },
    { 14, 15, 16, 17, 20, 21, 22, 23 } };

  // compare the dofs indices in finite elements
  for (int i = 0; i < fe_mesh->n_elements(); ++i)
  {
    const FiniteElement *elem = fe_mesh->element(i);
    EXPECT_EQ(elem->n_dofs(), n_dofs_per_node * Rectangle::N_VERTICES);
    for (int j = 0; j < elem->n_dofs(); ++j)
      EXPECT_EQ(elem->dof_number(j), dofs_in_elements[i][j]);
  }

  const int bound_dofs[] = { 0, 1, 2, 3, 4, 5, 6, 7, 10, 11,
                             12, 13, 16, 17, 18, 19, 20, 21, 22, 23 };
  std::vector<BoundaryNode> bdofs;
  dof_handler.boundary_dofs(bdofs);
  EXPECT_EQ(bdofs.size(), 20);
  for (int i = 0; i < (int)bdofs.size(); ++i) {
    EXPECT_EQ(bdofs[i].number(), bound_dofs[i]);
    const int mat_id = bdofs[i].material_id();
    EXPECT_TRUE(mat_id == Mesh::LEFT_LINE ||
                mat_id == Mesh::RIGHT_LINE ||
                mat_id == Mesh::BOTTOM_LINE ||
                mat_id == Mesh::TOP_LINE);
  }
}

// =============================================================================
TEST(CGDoFHandler, distribute_dofs_vector3_fe_rect)
{
  const double x0 = 5;
  const double x1 = 10;
  const double y0 = -8;
  const double y1 = 17;
  const int    nx = 2;
  const int    ny = 3;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 3; // vector FE with 3 dofs per one node
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  const int n_dofs = n_dofs_per_node*(nx+1)*(ny+1);
  EXPECT_EQ(dof_handler.n_dofs(), n_dofs);

  const int dofs_in_elements[][n_dofs_per_node*Rectangle::N_VERTICES] =
  { { 0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 13, 14 },
    { 3, 4, 5, 6, 7, 8, 12, 13, 14, 15, 16, 17 },
    { 9, 10, 11, 12, 13, 14, 18, 19, 20, 21, 22, 23 },
    { 12, 13, 14, 15, 16, 17, 21, 22, 23, 24, 25, 26 },
    { 18, 19, 20, 21, 22, 23, 27, 28, 29, 30, 31, 32 },
    { 21, 22, 23, 24, 25, 26, 30, 31, 32, 33, 34, 35 } };

  // compare the dofs indices in finite elements
  for (int i = 0; i < fe_mesh->n_elements(); ++i)
  {
    const FiniteElement *elem = fe_mesh->element(i);
    EXPECT_EQ(elem->n_dofs(), n_dofs_per_node * Rectangle::N_VERTICES);
    for (int j = 0; j < elem->n_dofs(); ++j)
      EXPECT_EQ(elem->dof_number(j), dofs_in_elements[i][j]);
  }

  const int bound_dofs[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                             10, 11, 15, 16, 17, 18, 19, 20, 24, 25,
                             26, 27, 28, 29, 30, 31, 32, 33, 34, 35 };
  std::vector<BoundaryNode> bdofs;
  dof_handler.boundary_dofs(bdofs);
  EXPECT_EQ(bdofs.size(), 30);
  for (int i = 0; i < (int)bdofs.size(); ++i) {
    EXPECT_EQ(bdofs[i].number(), bound_dofs[i]);
    const int mat_id = bdofs[i].material_id();
    EXPECT_TRUE(mat_id == Mesh::LEFT_LINE ||
                mat_id == Mesh::RIGHT_LINE ||
                mat_id == Mesh::BOTTOM_LINE ||
                mat_id == Mesh::TOP_LINE);
  }
}

// =============================================================================
TEST(CGDoFHandler, dofs_connections_rect)
{
  const double x0 = -1;
  const double x1 = 3;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  std::shared_ptr<FiniteElementMesh> fe_mesh(new LagrangeMesh(mesh, order));

  CGDoFHandler dof_handler(fe_mesh);
  std::vector<std::set<int> > connections;
  // because dofs are not distributed
  EXPECT_ANY_THROW(dof_handler.dofs_connections(connections));

  dof_handler.distribute_dofs();
  dof_handler.dofs_connections(connections);

  // compare connections
  std::vector<std::set<int> > expected((nx+1)*(ny+1));
  expected[0].insert(0); expected[0].insert(1);
  expected[0].insert(3); expected[0].insert(4);
  expected[1].insert(0); expected[1].insert(1);
  expected[1].insert(2); expected[1].insert(3);
  expected[1].insert(4); expected[1].insert(5);
  expected[2].insert(1); expected[2].insert(2);
  expected[2].insert(4); expected[2].insert(5);
  expected[3].insert(0); expected[3].insert(1);
  expected[3].insert(3); expected[3].insert(4);
  expected[3].insert(6); expected[3].insert(7);
  expected[4].insert(0); expected[4].insert(1);
  expected[4].insert(2); expected[4].insert(3);
  expected[4].insert(4); expected[4].insert(5);
  expected[4].insert(6); expected[4].insert(7); expected[4].insert(8);
  expected[5].insert(1); expected[5].insert(2);
  expected[5].insert(4); expected[5].insert(5);
  expected[5].insert(7); expected[5].insert(8);
  expected[6].insert(3); expected[6].insert(4);
  expected[6].insert(6); expected[6].insert(7);
  expected[7].insert(3); expected[7].insert(4);
  expected[7].insert(5); expected[7].insert(6);
  expected[7].insert(7); expected[7].insert(8);
  expected[8].insert(4); expected[8].insert(5);
  expected[8].insert(7); expected[8].insert(8);
  compare_connections(connections, expected);
}

// =============================================================================
TEST(CGDoFHandler, build_mass_matrix_scalar_fe)
{
  const double x0 = -1;
  const double x1 = 3;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  std::shared_ptr<FiniteElementMesh> fe_mesh(new LagrangeMesh(mesh, order));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);
  ConstantFunction coef(-3.5);

  dof_handler.build_mass_matrix(coef, matrix);
}

// =============================================================================
TEST(CGDoFHandler, build_stif_matrix_scalar_fe)
{
  const double x0 = -1;
  const double x1 = 3;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  std::shared_ptr<FiniteElementMesh> fe_mesh(new LagrangeMesh(mesh, order));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);
  ConstantFunction coef(-3.5);

  dof_handler.build_stiffness_matrix(coef, matrix);
}

// =============================================================================
//
//                                TEST CG MATRICES
//
// =============================================================================
//
// Mass matrix, vector2, CG, rectangles, coefficient = 1
//
// =============================================================================
TEST(CGDoFHandler, build_mass_matrix_vector2_coef1_n_2_2_h_5_5)
{
  const double x0 = -1;
  const double x1 = 9;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_CG_coef1_n_2_2_h_5_5.txt";
  test_mass_matrix_CG(mesh, order, n_dofs_per_node,
                      ConstantFunction(1.), fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_mass_matrix_vector2_coef1_n_2_2_h_3_7)
{
  const double x0 = 1;
  const double x1 = 7;
  const double y0 = 0;
  const double y1 = 14;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_CG_coef1_n_2_2_h_3_7.txt";
  test_mass_matrix_CG(mesh, order, n_dofs_per_node,
                      ConstantFunction(1.), fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_mass_matrix_vector2_coef1_n_10_10_h_5_5)
{
  const double x0 = -1;
  const double x1 = 49;
  const double y0 = 0;
  const double y1 = 50;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_CG_coef1_n_10_10_h_5_5.txt";
  test_mass_matrix_CG(mesh, order, n_dofs_per_node,
                      ConstantFunction(1.), fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_mass_matrix_vector2_coef1_n_10_10_h_3_7)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_CG_coef1_n_10_10_h_3_7.txt";
  test_mass_matrix_CG(mesh, order, n_dofs_per_node,
                      ConstantFunction(1.), fname, tolerance);
}

// =============================================================================
//
// EdgeMass matrix, vector2, CG, rectangles, coefficient = 1
//
// =============================================================================
TEST(CGDoFHandler, build_edge_mass_matrix_vector2_coef1_n_1_1_h_1_1)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  const double hx = 1;
  const double hy = 1;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  mesh->numerate_edges();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_EM_CG_coef1_n_1_1_h_1_1.txt";
  bool show_mat = false;
  test_edge_mass_matrix_CG(mesh, order, n_dofs_per_node,
                           ConstantFunction(1.), fname, tolerance, show_mat);
}
// =============================================================================
TEST(CGDoFHandler, build_edge_mass_matrix_vector2_coef1_n_2_2_h_5_5)
{
  const double x0 = -1;
  const double x1 = 9;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  mesh->numerate_edges();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_EM_CG_coef1_n_2_2_h_5_5.txt";
  test_edge_mass_matrix_CG(mesh, order, n_dofs_per_node,
                           ConstantFunction(1.), fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_edge_mass_matrix_vector2_coef1_n_2_2_h_3_7)
{
  const double x0 = 1;
  const double x1 = 7;
  const double y0 = 0;
  const double y1 = 14;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  mesh->numerate_edges();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_EM_CG_coef1_n_2_2_h_3_7.txt";
  test_edge_mass_matrix_CG(mesh, order, n_dofs_per_node,
                           ConstantFunction(1.), fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_edge_mass_matrix_vector2_coef1_n_10_10_h_5_5)
{
  const double x0 = -1;
  const double x1 = 49;
  const double y0 = 0;
  const double y1 = 50;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  mesh->numerate_edges();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_EM_CG_coef1_n_10_10_h_5_5.txt";
  test_edge_mass_matrix_CG(mesh, order, n_dofs_per_node,
                           ConstantFunction(1.), fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_edge_mass_matrix_vector2_coef1_n_10_10_h_3_7)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  mesh->numerate_edges();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_EM_CG_coef1_n_10_10_h_3_7.txt";
  test_edge_mass_matrix_CG(mesh, order, n_dofs_per_node,
                           ConstantFunction(1.), fname, tolerance);
}


// =============================================================================
//
// Stiffness matrix, vector2, CG, rectangles, coefficient = 1
//
// =============================================================================
TEST(CGDoFHandler, build_stif_matrix_vector2_coef1_n_2_2_h_5_5)
{
  const double x0 = -1;
  const double x1 = 9;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;
  const Vector coef(6, 1.0);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_S_CG_coef1_n_2_2_h_5_5.txt";
  test_stif_matrix_CG(mesh, order, n_dofs_per_node,
                      ConstantFunction(coef), fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_stif_matrix_vector2_coef1_n_2_2_h_3_7)
{
  const double x0 = 1;
  const double x1 = 7;
  const double y0 = 0;
  const double y1 = 14;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;
  const Vector coef(6, 1.0);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_S_CG_coef1_n_2_2_h_3_7.txt";
  test_stif_matrix_CG(mesh, order, n_dofs_per_node,
                      ConstantFunction(coef), fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_stif_matrix_vector2_coef1_n_10_10_h_5_5)
{
  const double x0 = -1;
  const double x1 = 49;
  const double y0 = 0;
  const double y1 = 50;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;
  const Vector coef(6, 1.0);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_S_CG_coef1_n_10_10_h_5_5.txt";
  test_stif_matrix_CG(mesh, order, n_dofs_per_node,
                      ConstantFunction(coef), fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_stif_matrix_vector2_coef1_n_10_10_h_3_7)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;
  const Vector coef(6, 1.0);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_S_CG_coef1_n_10_10_h_3_7.txt";
  test_stif_matrix_CG(mesh, order, n_dofs_per_node,
                      ConstantFunction(coef), fname, tolerance);
}

// =============================================================================
//
// Mass matrix, vector2, CG, rectangles, coefficient is not constant
//
// =============================================================================
TEST(CGDoFHandler, build_mass_matrix_vector2_n_2_2_h_5_5)
{
  const double x0 = -1;
  const double x1 = 9;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 4e-12;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_CG_n_2_2_h_5_5.txt";
  test_mass_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_mass_matrix_vector2_n_2_2_h_3_7)
{
  const double x0 = 1;
  const double x1 = 7;
  const double y0 = 0;
  const double y1 = 14;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 8e-12;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_CG_n_2_2_h_3_7.txt";
  test_mass_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_mass_matrix_vector2_n_10_10_h_5_5)
{
  const double x0 = -1;
  const double x1 = 49;
  const double y0 = 0;
  const double y1 = 50;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 3e-10;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_CG_n_10_10_h_5_5.txt";
  test_mass_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_mass_matrix_vector2_n_10_10_h_3_7)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 3e-10;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_CG_n_10_10_h_3_7.txt";
  test_mass_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}

// =============================================================================
//
// EdgeMass matrix, vector2, CG, rectangles, coefficient is not constant
//
// =============================================================================
TEST(CGDoFHandler, build_edge_mass_matrix_vector2_n_2_2_h_5_5)
{
  const double x0 = -1;
  const double x1 = 9;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 4e-12;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  mesh->numerate_edges();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_EM_CG_n_2_2_h_5_5.txt";
  test_edge_mass_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_edge_mass_matrix_vector2_n_2_2_h_3_7)
{
  const double x0 = 1;
  const double x1 = 7;
  const double y0 = 0;
  const double y1 = 14;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 8e-12;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  mesh->numerate_edges();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_EM_CG_n_2_2_h_3_7.txt";
  test_edge_mass_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_edge_mass_matrix_vector2_n_10_10_h_5_5)
{
  const double x0 = -1;
  const double x1 = 49;
  const double y0 = 0;
  const double y1 = 50;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 3e-10;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  mesh->numerate_edges();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_EM_CG_n_10_10_h_5_5.txt";
  test_edge_mass_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_edge_mass_matrix_vector2_n_10_10_h_3_7)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 3e-10;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  mesh->numerate_edges();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_EM_CG_n_10_10_h_3_7.txt";
  test_edge_mass_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}


// =============================================================================
//
// Stiffness matrix, vector2, CG, rectangles, coefficient is not constant
//
// =============================================================================
TEST(CGDoFHandler, build_stif_matrix_vector2_n_2_2_h_5_5)
{
  const double x0 = -1;
  const double x1 = 9;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 2e-12;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefElast_v0 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_S_CG_n_2_2_h_5_5.txt";
  test_stif_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_stif_matrix_vector2_n_2_2_h_3_7)
{
  const double x0 = 1;
  const double x1 = 7;
  const double y0 = 0;
  const double y1 = 14;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 4e-12;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefElast_v0 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_S_CG_n_2_2_h_3_7.txt";
  test_stif_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_stif_matrix_vector2_n_10_10_h_5_5)
{
  const double x0 = -1;
  const double x1 = 49;
  const double y0 = 0;
  const double y1 = 50;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 6e-11;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefElast_v0 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_S_CG_n_10_10_h_5_5.txt";
  test_stif_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_stif_matrix_vector2_n_10_10_h_3_7)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 6e-11;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefElast_v0 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_S_CG_n_10_10_h_3_7.txt";
  test_stif_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
//
// Special cases with physical coefficients
//
// =============================================================================
TEST(CGDoFHandler, build_mass_matrix_vector2_n_3_3_h_10_10_real_rho)
{
  const double x0 = 0;
  const double x1 = 30;
  const double y0 = 0;
  const double y1 = 30;
  const int    nx = 3;
  const int    ny = 3;
  const double hx = 10;
  const double hy = 10;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 2e-11;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRealRho_3_3 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_CG_n_3_3_h_10_10_real_rho.txt";
  test_mass_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_edge_mass_matrix_vector2_n_3_3_h_10_10_real_rho)
{
  const double x0 = 0;
  const double x1 = 30;
  const double y0 = 0;
  const double y1 = 30;
  const int    nx = 3;
  const int    ny = 3;
  const double hx = 10;
  const double hy = 10;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 2e-11;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  mesh->numerate_edges();
  const CoefRealRho_3_3 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_EM_CG_n_3_3_h_10_10_real_rho.txt";
  test_edge_mass_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(CGDoFHandler, build_stif_matrix_vector2_n_3_3_h_10_10_real_elast)
{
  const double x0 = 0;
  const double x1 = 30;
  const double y0 = 0;
  const double y1 = 30;
  const int    nx = 3;
  const int    ny = 3;
  const double hx = 10;
  const double hy = 10;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRealElast_3_3 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_S_CG_n_3_3_h_10_10_real_elast.txt";
  test_stif_matrix_CG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}


// =============================================================================
//
// Values in cells based on values at dofs
//
// =============================================================================
TEST(CGDoFHandler, average_per_cells_scalar_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  const double vec[] = { 1, 2, 3, 4 };
  const std::vector<double> val_dofs(vec, vec+4);
  Vector val_cells = dof_handler.average_per_cells(Vector(val_dofs));

  EXPECT_EQ(val_cells.size(), 1);
  EXPECT_DOUBLE_EQ(val_cells(0), 2.5);
}
// =============================================================================
TEST(CGDoFHandler, average_per_cells_scalar_1)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 2;
  const int    ny = 2;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  const double vec[] = { 1, 2, 3, 4, 3, 5, 8, 0, -1 };
  const std::vector<double> val_dofs(vec, vec+9);
  Vector val_cells = dof_handler.average_per_cells(Vector(val_dofs));

  const double v_cells[] = { 2.5, 3.25, 3.75, 1.75 };
  EXPECT_EQ(val_cells.size(), 4);
  for (int i = 0; i < val_cells.size(); ++i)
    EXPECT_DOUBLE_EQ(val_cells(i), v_cells[i]);
}
// =============================================================================
TEST(CGDoFHandler, average_per_cells_vector2_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2; // vector 2D FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  const double vec[] = { 1, 2, 3, 4, -2, 5, -6, 1 };
  const std::vector<double> val_dofs(vec, vec+8);
  Vector val_cells = dof_handler.average_per_cells(Vector(val_dofs));

  const double v_cells[] = { -1, 3 };
  EXPECT_EQ(val_cells.size(), 2);
  for (int i = 0; i < val_cells.size(); ++i)
    EXPECT_DOUBLE_EQ(val_cells(i), v_cells[i]);
}
// =============================================================================
TEST(CGDoFHandler, average_per_cells_vector2_1)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 2;
  const int    ny = 2;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2; // vector 2D FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  const double vec[] = { 1, 2, 3, 4, 3, 5, 8, 0, -1,
                         4, -6, 1, 4, 3, -2, 3, -4, 0 };
  const std::vector<double> val_dofs(vec, vec+18);
  Vector val_cells = dof_handler.average_per_cells(Vector(val_dofs));

  const double v_cells[] = { 2.75, 2.5, -0.25, 3.5, 2.25, 2.5, -3.25, 2 };
  EXPECT_EQ(val_cells.size(), 8);
  for (int i = 0; i < val_cells.size(); ++i)
    EXPECT_DOUBLE_EQ(val_cells(i), v_cells[i]);
}


// =============================================================================
//
// Boundary eigenproblem caused errors, so we checked what was going on. Since
// the problem was resolved, it's not so important anymore, but we still use
// this test from the regression viewpoint - not to introduce errors with new
// features to existing code (or correct existing code according to those new
// features).
// =============================================================================
TEST(CGDoFHandler, boun_bf_weights_scalar_0)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const int    order = 1;
  const int    n_dofs_per_node = 1;
  const ConstantFunction coef(1.);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);
  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);
  dof_handler.build_stiffness_matrix(coef, matrix);
  matrix.final_assembly();

  const int n_boundary_dofs = dof_handler.n_boundary_dofs();
  const int n_dofs = dof_handler.n_dofs();
  DensePattern dense_pattern(n_boundary_dofs, n_dofs);
  PetscMatrixPtr all_bound_weights(new PetscMatrix(dense_pattern));

  PetscMatrix stiff_mat_with_bc(matrix); // copy the matrix
  dof_handler.dirichlet(stiff_mat_with_bc); // Dirichlet boundary condition
  Solver solver(stiff_mat_with_bc); // SLAE solver

  for (int bd = 0; bd < n_boundary_dofs; ++bd)
  {
    // calculate ms_weights coefficients.
    // at one boundary node the function is 1, at others - is 0.
    // number of the current boundary dof, at which the basis function is 1
    const int b_dof_index = dof_handler.boundary_dof(bd).number();
    Vector system_rhs(n_dofs); // create zero vector
    system_rhs(b_dof_index) = 1.; // set the value corresponding to the number of the boundary dof
    Vector ms_weights(n_dofs); // solution of the SLAE for each boundary dof
    solver.solve(system_rhs, ms_weights); //, true); // solve the SLAE
    // save the vector ms_weights in the matrix of all weights of multiscale
    // boundary basis functions
    all_bound_weights->set_row(bd,          // number of the row
                               ms_weights); // the values
  } // we were passing through all boundary dofs
  all_bound_weights->final_assembly();
}
// =============================================================================
TEST(CGDoFHandler, boun_bf_weights_vector2_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 10;
  const int    ny = 10;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double rho = 1.;
  const double vp = 1.;
  const double vs = 1.;
  Vector coefvalues(6);
  coefvalues(0) = rho*vp*vp;
  coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
  coefvalues(2) = 0;
  coefvalues(3) = rho*vp*vp;
  coefvalues(4) = 0;
  coefvalues(5) = rho*vs*vs;
  const ConstantFunction coef(coefvalues);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);
  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);
  dof_handler.build_stiffness_matrix(coef, matrix);
  matrix.final_assembly();

  const int n_boundary_dofs = dof_handler.n_boundary_dofs();
  const int n_dofs = dof_handler.n_dofs();
  DensePattern dense_pattern(n_boundary_dofs, n_dofs);
  PetscMatrixPtr all_bound_weights(new PetscMatrix(dense_pattern));
  PetscMatrix stiff_mat_with_bc(matrix); // copy the matrix
  dof_handler.dirichlet(stiff_mat_with_bc); // Dirichlet boundary condition
  Solver solver(stiff_mat_with_bc); // SLAE solver

  // pass through all boundary dofs
  for (int bd = 0; bd < n_boundary_dofs; ++bd)
  {
    // calculate ms_weights coefficients.
    // at one boundary node the function is 1, at others - is 0

    // number of the current boundary dof, at which the basis function is 1
    const int b_dof_index = dof_handler.boundary_dof(bd).number();

    // create zero vector
    Vector system_rhs(n_dofs);
    // set the value corresponding to the number of the boundary dof
    system_rhs(b_dof_index) = 1.;

    // solution of the SLAE for each boundary dof
    Vector ms_weights(n_dofs);

    // solve the SLAE
    solver.solve(system_rhs, ms_weights); //, true);

    // save the vector ms_weights in the matrix of all weights of multiscale
    // boundary basis functions
    all_bound_weights->set_row(bd,          // number of the row
                               ms_weights); // the values

  } // we were passing through all boundary dofs

  // final assembly for the matrix of coefficients (weights) is needed for
  // further usage
  all_bound_weights->final_assembly();
}
//==============================================================================
// There is no real tests here - just ensuring, that the implementation doesn't
// fail. That's useful from a regression viewpoint - we make sure the new
// features don't break anything previously worked. This test exists for this
// reason.
//==============================================================================
TEST(CGDoFHandler, build_damping_matrix_vector2)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 10;
  const int    ny = 10;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double rho = 1.;
  const double vp = 1.;
  const double vs = 1.;
  Vector coefvalues(6);
  coefvalues(0) = rho*vp*vp;
  coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
  coefvalues(2) = 0;
  coefvalues(3) = rho*vp*vp;
  coefvalues(4) = 0;
  coefvalues(5) = rho*vs*vs;
  const ConstantFunction coef_stif(coefvalues);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);
  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);

  const double hx = (x1 - x0) / nx;
  const double abc_layer_width = 2. * hx;
  const double source_frequency = 20.;
  std::vector<double> weights; // weights of the damping matrix
  dof_handler.build_damping_matrix(ConstantFunction(rho), coef_stif,
                                   abc_layer_width, source_frequency,
                                   x0, x1, y0, y1,
                                   matrix, &weights);
  matrix.final_assembly();
}


#endif // TEST_FEMPLUS_CG_DOF_HANDLER_HPP
