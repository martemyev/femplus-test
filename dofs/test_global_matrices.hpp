#ifndef FEMPLUS_TEST_TEST_GLOBAL_MATRICES_HPP
#define FEMPLUS_TEST_TEST_GLOBAL_MATRICES_HPP

#include "femplus/function.hpp"
#include "femplus/mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/finite_element_mesh.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/cg_dof_handler.hpp"
#include "femplus/cg_overlap_dof_handler.hpp"
#include "femplus/dg_dof_handler.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/dg_finite_element_mesh.hpp"
#include "femplus/coarse_finite_element_mesh.hpp"

#include "gtest/gtest.h"
#include <fstream>

using namespace femplus;

// comparison of the computed matrix and the one read from the file
void compare_matrices(const PetscMatrix &matrix,
                      const std::string &compare_file,
                      double tolerance,
                      bool absolute = false)
{
  // comparison with the matrix computed by Kai Gao - the matrix is in the file
  std::ifstream in(compare_file.c_str());
  require(in, "File " + compare_file + " can't be opened");

  int N;
  in >> N;
  if (matrix.size() != N)
  {
    // let know that the test failed in the very beginning
    EXPECT_EQ(matrix.size(), N);
  }
  else
  {
    double mat_value;
    for (int i = 0; i < N; ++i)
      for (int j = 0; j < N; ++j)
      {
        in >> mat_value;
        if (fabs(mat_value) < math::FLOAT_NUMBERS_EQUALITY_TOLERANCE ||
            absolute)
          EXPECT_NEAR(matrix(i, j), mat_value, tolerance);
        else
          EXPECT_NEAR(fabs((mat_value-matrix(i, j))/mat_value), 0., tolerance);
      }
  }

  in.close();
}

// comparison of the computed matrix with the submatrix of a matrix read from a
// file
void compare_submatrices(const PetscMatrix &matrix,
                         const std::string &compare_file,
                         int row_0, int row_1, int col_0, int col_1,
                         double tolerance,
                         bool absolute = false)
{
  // comparison with the matrix computed by Kai Gao - the matrix is in the file
  std::ifstream in(compare_file.c_str());
  require(in, "File " + compare_file + " can't be opened");

  int N;
  in >> N;
  if (matrix.size() != N)
  {
    // let know that the test failed in the very beginning
    EXPECT_EQ(matrix.size(), N);
  }
  else
  {
    double mat_value;
    for (int i = 0; i < N; ++i)
      for (int j = 0; j < N; ++j)
      {
        in >> mat_value;
        if (fabs(mat_value) < math::FLOAT_NUMBERS_EQUALITY_TOLERANCE ||
            absolute)
          EXPECT_NEAR(matrix(i, j), mat_value, tolerance);
        else
          EXPECT_NEAR(fabs((mat_value-matrix(i, j))/mat_value), 0., tolerance);
      }
  }

  in.close();
}

// =============================================================================
//
// Global mass matrix for CG
//
// =============================================================================
void test_mass_matrix_CG(std::shared_ptr<Mesh> mesh,
                         int order, int n_dofs_per_node,
                         const Function &coefficient,
                         const std::string &compare_file,
                         double tolerance)
{
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);

  dof_handler.build_mass_matrix(coefficient, matrix);
  matrix.final_assembly();

  compare_matrices(matrix, compare_file, tolerance);
}

// =============================================================================
//
// Global edge-mass matrix for CG with overlapping
//
// =============================================================================
void test_edge_mass_matrix_CG(std::shared_ptr<Mesh> mesh,
                              int order, int n_dofs_per_node,
                              const Function &coefficient,
                              const std::string &compare_file,
                              double tolerance, bool show_mat = false)
{
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.boundary_edges_dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);

  dof_handler.build_boundary_edges_mass_matrix(coefficient, matrix);
  matrix.final_assembly();

  if (show_mat)
    matrix.matlab_view(std::cout);

  compare_matrices(matrix, compare_file, tolerance);
}



// =============================================================================
//
// Global stiffness matrix for CG
//
// =============================================================================
void test_stif_matrix_CG(std::shared_ptr<Mesh> mesh,
                         int order, int n_dofs_per_node,
                         const Function &coefficient,
                         const std::string &compare_file,
                         double tolerance)
{
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);

  dof_handler.build_stiffness_matrix(coefficient, matrix);
  matrix.final_assembly();

  compare_matrices(matrix, compare_file, tolerance);
}

// =============================================================================
//
// Global mass matrix for DG
//
// =============================================================================
void test_mass_matrix_DG(std::shared_ptr<Mesh> mesh,
                         int order, int n_dofs_per_node,
                         const Function &coefficient,
                         const std::string &compare_file,
                         double tolerance)
{
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  DGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);

  dof_handler.build_mass_matrix(coefficient, matrix);
  matrix.final_assembly();

  compare_matrices(matrix, compare_file, tolerance);
}

// =============================================================================
//
// Global mass matrix for GMsFEM
//
// =============================================================================
void test_mass_matrix_GMsFEM(const CoarseFiniteElementMesh &cfe_mesh,
                             const std::string &compare_file,
                             double tolerance)
{
//  compare_matrices(matrix, compare_file, tolerance);
}

// =============================================================================
//
// Global bare stiffness matrix for DG (with no contribution from edges)
//
// =============================================================================
void test_bare_stif_matrix_DG(std::shared_ptr<Mesh> mesh,
                              int order, int n_dofs_per_node,
                              const Function &coefficient,
                              const std::string &compare_file,
                              double tolerance)
{
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  DGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections); // for bare stiffness matrix
                                                  // the pattern is the same as
                                                  // as for the mass matrix

  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);

  //dof_handler.build_bare_stif_matrix(coefficient, matrix);
  dof_handler.build_stiffness_matrix(coefficient, matrix);
  matrix.final_assembly();

  compare_matrices(matrix, compare_file, tolerance);
}

// =============================================================================
//
// Global stiffness matrix (DG-matrix) for DG with contribution from only
// interior edges (no contribution from boundary edges)
//
// =============================================================================
void test_dg_matrix_wo_bound_DG(std::shared_ptr<Mesh> mesh,
                                int order, int n_dofs_per_node, double gamma,
                                const Function &coefficient,
                                const std::string &compare_file,
                                double tolerance)
{
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  DGDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();
  std::vector<std::set<int> > connections;
  dof_handler.dg_dofs_connections(connections); // there is no contribution from
                                                // boundary edges, but the pattern
                                                // is the same as for the real
                                                // DG matrix

  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);

  //dof_handler.build_stif_matrix_wo_bound(coefficient, matrix);
  dof_handler.build_stiffness_matrix(coefficient, matrix);
  dof_handler.add_interior_edges_matrices(coefficient, matrix);
  matrix.final_assembly();

  //compare_matrices(matrix, compare_file, tolerance);
  // comparison with the matrix computed by Kai Gao - the matrix is in the file
  std::ifstream in(compare_file.c_str());
  require(in, "File " + compare_file + " can't be opened");

  int N;
  in >> N;
  if (matrix.size() != N)
  {
    // let know that the test failed in the very beginning
    EXPECT_EQ(matrix.size(), N);
  }
  else
  {
    double mat_value;
    for (int i = 0; i < N; ++i)
      for (int j = 0; j < N; ++j)
      {
        in >> mat_value;
        //if (fabs(mat_value) < math::FLOAT_NUMBERS_EQUALITY_TOLERANCE)
          EXPECT_NEAR(matrix(i, j), mat_value, tolerance);
//        else
//          EXPECT_NEAR(fabs((mat_value-matrix(i, j))/mat_value), 0., tolerance);
      }
  }

  in.close();
}

// =============================================================================
//
// Global DG-matrix with contribution from only interior edges
//
// =============================================================================
void test_dg_matrix_edge_DG(std::shared_ptr<Mesh> mesh,
                            int order, int n_dofs_per_node, double gamma,
                            const Function &coefficient,
                            const std::string &compare_file,
                            double tolerance,
                            bool show_mat = false,
                            bool compare = true)
{
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  DGDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();
  std::vector<std::set<int> > connections;
  dof_handler.dg_dofs_connections(connections); // there is no contribution from
                                                // boundary edges, but the pattern
                                                // is the same as for the real
                                                // DG matrix

  CSRPattern pattern(connections);
  PetscMatrix matrix(pattern);

  //dof_handler.build_stif_matrix_edge(coefficient, matrix);
  dof_handler.add_interior_edges_matrices(coefficient, matrix);
  matrix.final_assembly();

  if (show_mat)
  {
    matrix.matlab_view(std::cout);
    matrix.spy(std::cout);
  }

  if (compare)
    compare_matrices(matrix, compare_file, tolerance);
}


#endif // FEMPLUS_TEST_TEST_GLOBAL_MATRICES_HPP
