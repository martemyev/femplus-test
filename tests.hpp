#ifndef TEST_FEMPLUS_TESTS_HPP
#define TEST_FEMPLUS_TESTS_HPP

//#define ALL

#ifndef ALL

///* miscelanea */
//#include "auxiliary_functions.hpp"
////#include "grid_function.hpp"

///* mesh elements */
//#include "rectangle.hpp"
//#include "polygon.hpp"

///* linear algebra */
//#include "vector.hpp"
//#include "csr_pattern.hpp"
//#include "dense_pattern.hpp"
//#include "sym_csr_pattern.hpp"
//#include "jadamilu.hpp"
//#include "petsc_matrix.hpp"
//#include "matrix.hpp"

///* dof handlers */
//#include "cg_dof_handler.hpp"
//#include "cg_interior_dof_handler.hpp"
//#include "dg_dof_handler.hpp"

//#include "cg_interior_dof_handler.hpp"
//#include "cg_overlap_dof_handler.hpp"
//#include "gms_dof_handler.hpp"

///* meshes */
//#include "rectangular_mesh.hpp"
#include "triangular_mesh.hpp"
//#include "rectangular_dg_fe_mesh.hpp"
//#include "triangular_dg_fe_mesh.hpp"
//#include "mixed_poly_rect_mesh.hpp"
//#include "quadrilateral_mesh_gmsh.hpp"

///* methods */
//#include "elliptic2D.hpp"
//#include "dg_elliptic2D_rectangles.hpp"
//#include "dg_elliptic2D_triangles.hpp"

//#include "dg_wave_acoustic_2D.hpp"

//#include "gmsfem_elliptic2D.hpp"
//#include "gmsfem_elliptic_salt_dome.hpp"
//#include "gmsfem_elliptic_elastic_2D.hpp"
//#include "gmsfem_build_rhs.hpp"
//#include "gmsfem_build_dg_matrix.hpp"

//#include "gmsfem_wave_acoustic_2D.hpp"

//#include "gmsfem_wave_elastic_2D.hpp"

//#include "fem_wave_acoustic_2D.hpp"

//#include "test_source_approximation.hpp"

///* coarse elements and all related */
//#include "overlap_coarse_element.hpp"
//#include "overlap_coarse_finite_element.hpp"
//#include "overlap_coarse_fe_mesh.hpp"
//#include "coarse_rectangle.hpp"
//#include "coarse_rectangular_mesh.hpp"
//#include "coarse_mixed_poly_rect_mesh.hpp"
//#include "coarse_finite_element.hpp"
//#include "coarse_finite_element_mesh.hpp"

#else // ALL tests
  #include "auxiliary_functions.hpp"
  #include "grid_function.hpp"
  #include "rectangle.hpp"
  #include "polygon.hpp"
  #include "vector.hpp"
  #include "csr_pattern.hpp"
  #include "sym_csr_pattern.hpp"
  #include "dense_pattern.hpp"
  #include "sym_csr_pattern.hpp"
  #include "jadamilu.hpp"
  #include "petsc_matrix.hpp"
  #include "matrix.hpp"
  #include "cg_dof_handler.hpp"
  #include "cg_interior_dof_handler.hpp"
  #include "dg_dof_handler.hpp"
  #include "cg_interior_dof_handler.hpp"
  #include "cg_overlap_dof_handler.hpp"
  #include "gms_dof_handler.hpp"
  #include "rectangular_mesh.hpp"
  #include "triangular_mesh.hpp"
  #include "rectangular_dg_fe_mesh.hpp"
  #include "triangular_dg_fe_mesh.hpp"
  #include "mixed_poly_rect_mesh.hpp"
  #include "quadrilateral_mesh_gmsh.hpp"
  #include "elliptic2D.hpp"
  #include "dg_elliptic2D_triangles.hpp"
  #include "dg_elliptic2D_rectangles.hpp"
  #include "gmsfem_elliptic2D.hpp"
  #include "gmsfem_elliptic_salt_dome.hpp"
  #include "gmsfem_elliptic_elastic_2D.hpp"
  #include "gmsfem_build_rhs.hpp"
  #include "gmsfem_build_dg_matrix.hpp"
  #include "gmsfem_wave_acoustic_2D.hpp"
  #include "gmsfem_wave_elastic_2D.hpp"
  #include "fem_wave_acoustic_2D.hpp"
  #include "coarse_rectangle.hpp"
  #include "coarse_rectangular_mesh.hpp"
  #include "coarse_mixed_poly_rect_mesh.hpp"
  #include "coarse_finite_element.hpp"
  #include "coarse_finite_element_mesh.hpp"
  #include "overlap_coarse_element.hpp"
  #include "overlap_coarse_finite_element.hpp"
  #include "overlap_coarse_fe_mesh.hpp"
#endif

#endif // TEST_FEMPLUS_TESTS_HPP



