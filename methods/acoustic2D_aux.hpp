#ifndef FEMPLUS_TEST_ACOUSTIC2D_AUX_HPP
#define FEMPLUS_TEST_ACOUSTIC2D_AUX_HPP

#include "config.hpp"

#include "femplus/function.hpp"
#include "femplus/constant_function.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/dof_handler.hpp"
#include "femplus/cg_dof_handler.hpp"
#include "femplus/dg_dof_handler.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/vector.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/math_functions.hpp"
#include "femplus/solver.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/rectangle.hpp"
#include "femplus/mesh_element.hpp"
#include "femplus/coarse_finite_element_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/gms_dof_handler.hpp"
#include "femplus/dense_pattern.hpp"
#include "femplus/gaussian_scalar.hpp"

#if defined(USE_BOOST)
  #include "boost/format.hpp"
#endif

#include <fstream>

//#define SHOW_MATRIX
//#define TIME

using namespace femplus;

void print_info_about_wavelenghts(double vmax, double vmin,
                                  double source_frequency,
                                  double hx, double hy,
                                  double tend, double dt)
{
  const double max_wavelength = vmax / source_frequency;
  const double min_wavelength = vmin / source_frequency;
  const double max_h = std::max(hx, hy);
  const double min_h = std::min(hx, hy);

  std::cout << "max wavelength = " << max_wavelength << " m" << std::endl;
  std::cout << "min wavelength = " << min_wavelength << " m" << std::endl;
  std::cout << "max h          = " << max_h << " m" << std::endl;
  std::cout << "min h          = " << min_h << " m" << std::endl;
  std::cout << "min wavelenghts per domain = " << hx / max_wavelength << std::endl;
  std::cout << "max wavelenghts per domain = " << hx / min_wavelength << std::endl;

  std::cout << "after " << tend << " s the wave'll pass "
            << tend * vmax << " m" << std::endl;

  // stability
  const double tmp = 1./(vmax*sqrt(1./(hx*hx)+1./(hy*hy)));
  std::cout << "dt (" << dt << ") should be less than " << tmp << ", and it is"
            << (dt < tmp ? "." : " NOT.") << std::endl;
  std::cout << "ratio should be around 8 or more. it is " << tmp/dt << std::endl;
  if (tmp/dt < 8) {
    std::cout << " suggested dt = " << tmp/8 << " number of time steps = "
              << tend*8/tmp << std::endl;
  }

  // dispersion
  std::cout << "min cells per wavelength = " << min_wavelength / max_h << std::endl;
  std::cout << "max cells per wavelength = " << max_wavelength / min_h << std::endl;
}

//==============================================================================
//
// Create a geo file demonstrating a source support area (a circle) with a
// mesh as a background
//
//==============================================================================
void write_geo_support(const std::string &filemesh,
                       const Point &source_center,
                       double source_support)
{
  require(file_extension(filemesh) == ".msh", "There is no .msh file provided");
  const std::string geofile = file_path(filemesh) + "/" +
                              file_stem(filemesh) + ".geo";
  std::ofstream out(geofile.c_str());
  require(out, "File " + geofile + " can't be opened");
  out << "Merge \"" << filemesh << "\";\n";
  out << "cl = 1; // doesn't really matter\n";
  const double xc = source_center.x();
  const double yc = source_center.y();
  const double rad = 0.5 * source_support;
  out << "Point(1) = { " << xc << ", " << yc << ", 0, cl };\n";
  out << "Point(2) = { " << xc-rad << ", " << yc << ", 0, cl };\n";
  out << "Point(3) = { " << xc << ", " << yc-rad << ", 0, cl };\n";
  out << "Point(4) = { " << xc+rad << ", " << yc << ", 0, cl };\n";
  out << "Point(5) = { " << xc << ", " << yc+rad << ", 0, cl };\n";
  out << "Circle(1) = { 2, 1, 3 };\n";
  out << "Circle(2) = { 3, 1, 4 };\n";
  out << "Circle(3) = { 4, 1, 5 };\n";
  out << "Circle(4) = { 5, 1, 2 };\n";
  out.close();
}

//==============================================================================
//
// Write a geo file with points, connected by circular arcs between each other
//
//==============================================================================
void write_circle(const std::string &fname,
                  const Point &center,
                  const std::vector<Point> &points)
{
  std::ofstream out(fname.c_str());
  require(out, "File " + fname + " can't be opened");

  const int np = points.size();
  out << "Point(" << np+1 << ") = { " << center.x() << ", " << center.y() << ", 0, 1 };\n";
  for (int i = 0; i < np; ++i)
    out << "Point(" << i+1 << ") = { " << points[i].x() << ", " << points[i].y() << ", 0, 1 };\n";

  for (int i = 0; i < np; ++i)
    out << "Circle(" << i+1 << ") = { " << i+1 << ", " << np+1 << ", " << (i+1)%np+1 << " };\n";

  out.close();
}

////==============================================================================
////
//// Create a vector of points located on a circlular arc from beg_angle to
//// end_angle (it's a whole circle by default)
////
////==============================================================================
//void points_along_circle(const Point &center, double radius,
//                         double angle_discretization,
//                         std::vector<Point> &points,
//                         double beg_angle = 0.,
//                         double end_angle = 360.)
//{
//  points.clear();

//  const std::string fname = TESTOUT_DIR + "testdel.geo";
//  std::ofstream out(fname.c_str());
//  require(out, "File '" + fname + "'can't be opened");

//  int i = 1;
//  double angle = beg_angle;
//  for (; angle < end_angle; angle += angle_discretization)
//  {
//    const double rad_angle = angle * math::PI / 180.;
//    const double x = center.x() + radius * cos(rad_angle);
//    const double y = center.y() + radius * sin(rad_angle);
//    points.push_back(Point(x, y));
//    out << "Point(" << i++ << ") = {" << x << "," << y << ",0,1};\n";
//  }
//  out.close();
//}

////==============================================================================
////
//// Create a vector of points located on a straight line
////
////==============================================================================
//void points_along_line(const Point &begin,
//                       const Point &end,
//                       double n_points,
//                       std::vector<Point> &points)
//{
//  const double x0 = begin.x();
//  const double x1 = end.x();
//  const double y0 = begin.y();
//  const double y1 = end.y();
//  points.resize(n_points);

//  const double dx = (x1 - x0) / (n_points-1);
//  const double dy = (y1 - y0) / (n_points-1);

//  for (int i = 0; i < n_points; ++i)
//  {
//    const double x = (i == n_points-1 ? x1 : x0 + i*dx);
//    const double y = (i == n_points-1 ? y1 : y0 + i*dy);
//    points[i] = Point(x, y);
//  }
//}

//==============================================================================
//
// Find x-coordinates of max and min wavefield along a line going horizontally
// from center to end_point
//
//==============================================================================
void sup_wavefield(const DoFHandler &dof_handler,
                   const Point &center,
                   const Point &end_point,
                   const Vector &solution,
                   double &x_of_min,
                   double &x_of_max)
{
  std::vector<Point> points;
  const double hx = 1.; // resolution in meters

  const Point beg = center;
  const Point end = Point(end_point.x(), center.y());
  const int n_points = int((end.x() - beg.x()) / hx) + 1;

  points_along_line(beg, end, n_points, points);

  const Vector sol_at_line = dof_handler.compute_solution_at_points(points, solution);
  double max_value = sol_at_line(0); x_of_max = center.x();
  double min_value = sol_at_line(0); x_of_min = center.x();
  for (int i = 1; i < sol_at_line.size(); ++i) {
    if (sol_at_line(i) > max_value) {
      max_value = sol_at_line(i);
      x_of_max = center.x() + i*hx;
    }
    if (sol_at_line(i) < min_value) {
      min_value = sol_at_line(i);
      x_of_min = center.x() + i*hx;
    }
  }
}

//==============================================================================
//
// FEM
//
//==============================================================================
void fem_acoustic_offline(FEMeshPtr fe_mesh,
                          const Function &coef_mass,
                          const Function &coef_stif,
                          const Function *rhs_space,
                          std::shared_ptr<CGDoFHandler> &dof_handler,
                          PetscMatrixPtr &mass,
                          PetscMatrixPtr &stif,
                          Vector &rhs_vector,
                          Vector *source_vector = NULL,
                          const CGDoFHandler *another_dof_handler = NULL)
{
  dof_handler = std::shared_ptr<CGDoFHandler>(new CGDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();

  std::vector<std::set<int> > connections;
  dof_handler->dofs_connections(connections);
  CSRPattern pattern(connections);

  mass = PetscMatrixPtr(new PetscMatrix(pattern));
  dof_handler->build_mass_matrix(coef_mass, *mass.get());
  mass->final_assembly();

  stif = PetscMatrixPtr(new PetscMatrix(pattern));
  dof_handler->build_stiffness_matrix(coef_stif, *stif.get());
  stif->final_assembly();

  const GaussianScalar *gauss_rhs = dynamic_cast<const GaussianScalar*>(rhs_space);
  if (gauss_rhs != NULL)
  {
    dof_handler->build_rhs(*gauss_rhs, rhs_vector);

  } // for case of Gaussian scalar RHS function
  else
  {
    // values of the source function at the current dofs
    Vector source_values;
    // if the given source vector is empty, we fill it with the values of the
    // source function at the current dofs
    if (source_vector != NULL)
    {
      if (source_vector->empty()) {
        dof_handler->init_vector(*rhs_space, source_values);
        *source_vector = source_values;
      }
      else { // if the given source vector is not empty, it means that the values of
             // the source function have been already computed for 'another_dof_handler',
             // and the current source values will be an approximation of the given
             // source values for the current dofs
        require(another_dof_handler != NULL, "The source vector is not empty, but "
                "'another_dof_handler' is NULL");
        require(another_dof_handler->n_dofs() == source_vector->size(), "The "
                "'another_dof_handler' and 'source_vector' are not in consistency");
        source_values = another_dof_handler->compute_solution_at_points(dof_handler->dofs(),
                                                                        *source_vector);
      }
    }
    else { // if source_vector == NULL
      dof_handler->init_vector(*rhs_space, source_values);
    }

    PetscMatrixPtr L2_mat(new PetscMatrix(pattern));
    dof_handler->build_L2_matrix(*L2_mat.get());
    L2_mat->final_assembly();

    //dof_handler->build_vector(L2_mat, rhs_space, rhs_vector);
    rhs_vector = Mv(L2_mat, source_values); // it's the same as the previous line,
                                            // but with respect to the newest changes
  } // for general case
}



//==============================================================================
//
// DG
//
//==============================================================================
void dg_acoustic_offline(FEMeshPtr fe_mesh,
                         double gamma,
                         const Function &coef_mass,
                         const Function &coef_stif,
                         const Function *rhs_space,
                         std::shared_ptr<DGDoFHandler> &dof_handler,
                         PetscMatrixPtr &mass,
                         PetscMatrixPtr &stif,
                         Vector &rhs_vector,
                         Vector *source_vector = NULL,
                         const DGDoFHandler *another_dof_handler = NULL)
{
  dof_handler = std::shared_ptr<DGDoFHandler>(new DGDoFHandler(fe_mesh, gamma));
  dof_handler->distribute_dofs();
  dof_handler->numerate_edges();
  dof_handler->distribute_boundary_dofs();

  std::vector<std::set<int> > connections;
  dof_handler->mass_dofs_connections(connections);
  CSRPattern mass_pattern(connections);
  mass = PetscMatrixPtr(new PetscMatrix(mass_pattern));
  dof_handler->build_mass_matrix(coef_mass, *mass.get());
  mass->final_assembly();

  connections.clear();
  dof_handler->dg_dofs_connections(connections);
  CSRPattern stif_pattern(connections);
  stif = PetscMatrixPtr(new PetscMatrix(stif_pattern));
  dof_handler->build_stiffness_matrix(coef_stif, *stif.get());
  stif->final_assembly();

  const GaussianScalar *gauss_rhs = dynamic_cast<const GaussianScalar*>(rhs_space);
  if (gauss_rhs != NULL)
  {
    dof_handler->build_rhs(*gauss_rhs, rhs_vector);

  } // for case of Gaussian scalar RHS function
  else
  {
    // values of the source function at the current dofs
    Vector source_values;
    // if the given source vector is empty, we fill it with the values of the
    // source function at the current dofs
    if (source_vector != NULL)
    {
      if (source_vector->empty()) {
        dof_handler->init_vector(*rhs_space, source_values);
        *source_vector = source_values;
      }
      else { // if the given source vector is not empty, it means that the values of
             // the source function have been already computed for 'another_dof_handler',
             // and the current source values will be an approximation of the given
             // source values for the current dofs
        require(another_dof_handler != NULL, "The source vector is not empty, but "
                "'another_dof_handler' is NULL");
        require(another_dof_handler->n_dofs() == source_vector->size(), "The "
                "'another_dof_handler' and 'source_vector' are not in consistency");
        source_values = another_dof_handler->compute_solution_at_points(dof_handler->dofs(),
                                                                        *source_vector);
      }
    }
    else { // if source_vector == NULL
      dof_handler->init_vector(*rhs_space, source_values);
    }

    PetscMatrixPtr L2_mat(new PetscMatrix(mass_pattern));
    dof_handler->build_L2_matrix(*L2_mat.get());
    L2_mat->final_assembly();

    //dof_handler->build_vector(L2_mat, rhs_space, rhs_vector);
    rhs_vector = Mv(L2_mat, source_values); // it's the same as the previous line,
                                            // but with respect to the newest changes
  } // for general case
}



//==============================================================================
//
// GMsFEM
//
//==============================================================================
void gmsfem_acoustic(CoarseMeshPtr &cmesh,
                     FunctionPtr coef_mass,
                     FunctionPtr coef_stif,
                     int fe_order_coarse,
                     int fe_order_fine,
                     int n_boundary_bf,
                     int n_interior_bf,
                     Eigensolver bound_eigensolver,
                     Eigensolver inter_eigensolver,
                     bool show_time_eigensolver,
                     double gamma,
                     const Function *rhs_space,
                     const Function &rhs_time,
                     int n_time_steps,
                     double dt,
                     const std::string &fname_diff,
                     int time_step_snapshot,
                     int time_step_seis,
                     MeshPtr ref_mesh = NULL,
                     double *offline_time = NULL,
                     double *online_time = NULL,
                     const std::string *seismofile = NULL,
                     const std::vector<Point> *receivers = NULL,
                     std::vector<Vector> *values_receivers = NULL,
                     int n_boundary_bf_source = 0,
                     int n_interior_bf_source = 0,
                     double *glob_rel_error = NULL)
{
  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool boundary_edge_mass_matrix = true;
#if defined(TIME)
  double t0 = get_wall_time(), t1;
#endif

  double t0 = get_wall_time();

  // 1. compute multiscale basis functions and distribute dofs
  CoarseFEMeshPtr fe_mesh(new CoarseFiniteElementMesh(cmesh,
                                                      fe_order_coarse,
                                                      fe_order_fine));

  const GaussianScalar *rhs_gaussian = dynamic_cast<const GaussianScalar*>(rhs_space);
  if (rhs_gaussian != NULL)
  {
    // for the special case of a Gaussian distribution in space, we select
    // different number of basis functions in different coarse elements

    const Point source_center = rhs_gaussian->center();
    const bool throw_exc = true; // throw an exception is element is not found
    const int source_cell_num = fe_mesh->find_element(source_center, throw_exc);
//    std::cout << "Gaussian RHS source is in " << source_cell_num << " coarse cell\n";

    require(n_boundary_bf_source != 0 && n_interior_bf_source != 0,
            "The number of boundary and interior bf in a source cell is 0");

    int nb, ni;
    for (int el = 0; el < fe_mesh->n_elements(); ++el)
    {
      if (el == source_cell_num) {
        nb = n_boundary_bf_source;
        ni = n_interior_bf_source;
      }
      else {
        nb = n_boundary_bf;
        ni = n_interior_bf;
      }
      fe_mesh->element(el)->compute_basis(coef_mass,
                                          coef_stif,
                                          nb,
                                          ni,
                                          bound_eigensolver,
                                          inter_eigensolver,
                                          boundary_regularization,
                                          interior_regularization,
                                          boundary_take_first_eigen,
                                          interior_take_first_eigen,
                                          boundary_edge_mass_matrix,
                                          show_time_eigensolver);
    }
  }
  else
  {
    // standard approach
    fe_mesh->compute_basis(coef_mass,
                           coef_stif,
                           n_boundary_bf,
                           n_interior_bf,
                           bound_eigensolver,
                           inter_eigensolver,
                           boundary_regularization,
                           interior_regularization,
                           boundary_take_first_eigen,
                           interior_take_first_eigen,
                           boundary_edge_mass_matrix,
                           show_time_eigensolver);
  }

  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();

  // 2. build inverse mass matrix
  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  CSRPattern mass_pattern(connections);
  PetscMatrixPtr invM(new PetscMatrix(mass_pattern));
  dof_handler.build_inv_mass_matrix(*invM.get());
  invM->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- INVERSE MASS MATRIX ----- \n";
  std::ofstream invmout("inv_mass_matrix_.dat");
  invM->matlab_view(mass_pattern, invmout);
  invmout.close();
#endif
#if defined(TIME)
  t1 = get_wall_time();
  std::cout << "build inverse mass matrix, time = " << t1-t0 << std::endl;
  t0 = t1;
#endif

  // 3. build global DG matrix
  connections.clear();
  dof_handler.dg_dofs_connections(connections);
  CSRPattern dg_pattern(connections);
  PetscMatrixPtr S(new PetscMatrix(dg_pattern));
  dof_handler.build_stiffness_matrix(*coef_stif.get(), *S.get());
  dof_handler.add_boundary_edges_matrices(*coef_stif.get(), *S.get());
  dof_handler.add_interior_edges_matrices(*coef_stif.get(), *S.get());
  S->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- DG MATRIX ----- \n";
  std::ofstream dgout("dg_matrix__.dat");
  S->matlab_view(dg_pattern, dgout);
  dgout.close();
#endif
#if defined(TIME)
  t1 = get_wall_time();
  std::cout << "build global DG matrix, time = " << t1-t0 << std::endl;
  t0 = t1;
#endif

  // 4. inv_mass_mat * dg_mat
  const int N = dof_handler.n_dofs();
  DensePattern dense(N, N);
  PetscMatrixPtr invMS(new PetscMatrix(dense));
  invMS->AB(invM, S);
  invMS->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- INVERSE MASS * DG MATRIX ----- \n";
  std::ofstream invdgout("inv_mass_dg_matrix__.dat");
  invMS->full_view(dense, invdgout);
  invdgout.close();
#endif
#if defined(TIME)
  t1 = get_wall_time();
  std::cout << "inv_mass_mat * dg_mat, time = " << t1-t0 << std::endl;
  t0 = t1;
#endif

  // 5. Global RHS
  Vector rhs_vector;
  if (rhs_gaussian != NULL)
    dof_handler.build_rhs(*rhs_gaussian, rhs_vector);
  else
    dof_handler.build_vector(*rhs_space, rhs_vector);
  double t1 = get_wall_time();

  if (offline_time != NULL) *offline_time = t1 - t0;

  // 5. FEM acoustic offline stage
  std::shared_ptr<CGDoFHandler> ref_dof_handler;
  PetscMatrixPtr ref_mass, ref_stif;
  Vector ref_rhs_vector;
  std::unique_ptr<Solver> ref_solver;
  if (ref_mesh != NULL)
  {
    FEMeshPtr ref_fe_mesh(new LagrangeMesh(ref_mesh, fe_order_fine));
    fem_acoustic_offline(ref_fe_mesh, *coef_mass.get(), *coef_stif.get(),
                         rhs_space, ref_dof_handler, ref_mass, ref_stif,
                         ref_rhs_vector);
    PetscMatrix ref_system_mat(*ref_mass.get());
    ref_dof_handler->dirichlet(ref_system_mat); //, ConstantFunction(0), &rhs_vector);
    ref_solver = std::unique_ptr<Solver>(new Solver(ref_system_mat));
  }

  // 5. online stage
  Vector solution(N);   // numerical solution on the current (n-th) time step
  Vector solution_1(N); // numerical solution on the (n-1)-th time step
  Vector solution_2(N); // numerical solution on the (n-2)-th time step
  Vector ref_solution;
  Vector ref_solution_1;
  Vector ref_solution_2;
  if (ref_mesh != NULL) {
    const int refN = ref_dof_handler->n_dofs();
    ref_solution.resize(refN);
    ref_solution_1.resize(refN);
    ref_solution_2.resize(refN);
  }

  std::ofstream seismogram_out;
  if (seismofile != NULL) {
    seismogram_out.open(seismofile->c_str());
    require(seismogram_out, "File '" + *seismofile + "' can't be opened");
    require(!receivers->empty(), "The vector of receivers is empty");
    require(values_receivers != NULL, "The vector for values on receivers is not given");
    seismogram_out.setf(std::ios::scientific);
    seismogram_out.precision(8);
  }


  if (online_time != NULL) *online_time = 0;

  for (int time_step = 2; time_step <= n_time_steps; ++time_step)
  {
    t0 = get_wall_time();

    const double time = time_step * dt; // current time
    rhs_time.set_time(time - dt);
    const double r = rhs_time.value(Point()); // point doesn't matter

    // ---------------- GMsFEM ---------------
    const Vector tmp_0 = Mv(invMS, solution_1);
    const Vector tmp_1 = Mv(invM, r*rhs_vector);
    solution = 2.*solution_1 - solution_2 - dt*dt*(tmp_0 - tmp_1);
    solution_2 = solution_1; // reassign the solutions on the previuos time steps
    solution_1 = solution;
    t1 = get_wall_time();
    if (online_time != NULL) *online_time += t1 - t0;

    // ---------------- FEM ---------------
    if (ref_mesh != NULL)
    {
      const Vector tmp1 = Mv(ref_mass, 2.*ref_solution_1 - ref_solution_2); // M (2 U-1 - U_2)
      const Vector tmp2 = Mv(ref_stif, ref_solution_1); // S U_1
      Vector tmp3 = tmp1 - dt*dt*(tmp2 - r*ref_rhs_vector);
      // Dirichlet
      for (int bd = 0; bd < ref_dof_handler->n_boundary_dofs(); ++bd) {
        const int bdof = ref_dof_handler->boundary_dof_number(bd);
        tmp3(bdof) = 0;
      }
      ref_solver->solve(tmp3, ref_solution);
      ref_solution_2 = ref_solution_1; // reassign the solutions on the previuos time steps
      ref_solution_1 = ref_solution;
    }

    // -------------------- Seismogram -----------------
    if (time_step % time_step_seis == 0)
    {
      if (seismofile != NULL)
      {
        const Vector sol = dof_handler.compute_solution_at_points(*receivers,
                                                                  solution);
        values_receivers->push_back(sol);
        for (int i = 0; i < sol.size(); ++i)
          seismogram_out << sol(i) << "\t";
        seismogram_out << "\n";
      }
    }
    // -------------------- Comparison -----------------
    if (time_step % time_step_snapshot == 0)
    {
      if (fname_diff != "")
      {
        double rel_error = -1.;
        if (ref_mesh != NULL)
        {
          Vector num_sol_at_ref_dofs; // GMsFEM solution at reference dofs
          // recompute GMsFEM solution at reference dofs points
          rel_error = dof_handler.compute_error(ref_dof_handler->dofs(),
                                                ref_solution,
                                                solution,
                                                num_sol_at_ref_dofs);

          const std::string fname = TESTOUT_DIR + "compare_with_ref_" + fname_diff +
                                    "_t" + d2s<int>(time_step) + ".vtu";
          std::vector<Vector> output_sol(3);
          output_sol[0] = ref_solution;
          output_sol[1] = num_sol_at_ref_dofs;
          output_sol[2] = ref_solution - num_sol_at_ref_dofs;
          std::vector<std::string> sol_names(3);
          sol_names[0] = "1_reference_solution";
          sol_names[1] = "2_gmsfem_solution_at_reference_solution_dofs";
          sol_names[2] = "3_difference_between_them";
          ref_dof_handler->write_solution_vtu(fname, output_sol, sol_names);

//          // comparison in a subdomain
//          const double subx0 = 300;
//          const double subx1 = 700;
//          const double suby0 = 600;
//          const double suby1 = 1000;
//          const int subnx = 50;
//          const int subny = 50;
//          MeshPtr sub_mesh(new RectangularMesh(subx0, subx1, suby0, suby1, subnx, subny));
//          sub_mesh->build();
//          FEMeshPtr sub_fe_mesh(new LagrangeMesh(sub_mesh, 1));
//          CGDoFHandler sub_dh(sub_fe_mesh);
//          sub_dh.distribute_dofs();
//          const Vector solGMs_sub = dof_handler.compute_solution_at_points(sub_dh.dofs(),
//                                                                           solution);
//          const Vector solRef_sub = ref_dof_handler->compute_solution_at_points(sub_dh.dofs(),
//                                                                                ref_solution);
//          std::cout << "rel err: T vs R in subdomain = " << math::rel_error(solRef_sub, solGMs_sub) << std::endl;
//          std::vector<Vector> sol_sub(3);
//          sol_sub[0] = solRef_sub;
//          sol_sub[1] = solGMs_sub;
//          sol_sub[2] = solRef_sub - solGMs_sub;
//          std::vector<std::string> sub_names(3);
//          sub_names[0] = "reference solution";
//          sub_names[1] = "GMsFEM solution";
//          sub_names[2] = "difference";
//          sub_dh.write_solution(TESTOUT_DIR + "flat_subdomain_" + fname_diff + ".vtu", sol_sub, sub_names);

//          // comparison along lines
//          const double x_ver   = 500.; // vertical line
//          const double y_ver_0 = 10.;
//          const double y_ver_1 = 990.;
//          const double x_hor_0 = 10.;  // horizontal line
//          const double x_hor_1 = 990.;
//          const double y_hor   = 500.;
//          const double dx = 5.;
//          const double dy = 5.;
//          std::vector<Vector> solutions_along_lines(2);
//          std::vector<std::string> names(2);

//          std::vector<Point> points;
//          Vector sol_GMs_along_ver = dof_handler.compute_solution_along_line(x_ver, x_ver, y_ver_0, y_ver_1,
//                                                                             dx, dy, solution, points);
//          Vector sol_Ref_along_ver = ref_dof_handler->compute_solution_along_line(x_ver, x_ver, y_ver_0, y_ver_1,
//                                                                                  dx, dy, ref_solution, points);
//          solutions_along_lines[0] = sol_Ref_along_ver; names[0] = "reference";
//          solutions_along_lines[1] = sol_GMs_along_ver; names[1] = "GMsFEM";
//          std::string csv_fname = TESTOUT_DIR + "compare_along_ver_" + fname_diff + ".csv";
//          print_csv(csv_fname, points, solutions_along_lines, names);

//          Vector sol_GMs_along_hor = dof_handler.compute_solution_along_line(x_hor_0, x_hor_1, y_hor, y_hor,
//                                                                             dx, dy, solution, points);
//          Vector sol_Ref_along_hor = ref_dof_handler->compute_solution_along_line(x_hor_0, x_hor_1, y_hor, y_hor,
//                                                                                  dx, dy, ref_solution, points);
//          solutions_along_lines[0] = sol_Ref_along_hor; names[0] = "reference";
//          solutions_along_lines[1] = sol_GMs_along_hor; names[1] = "GMsFEM";
//          csv_fname = TESTOUT_DIR + "compare_along_hor_" + fname_diff + ".csv";
//          print_csv(csv_fname, points, solutions_along_lines, names);

//          std::cout << "time step " << time_step
//                    << " norm(solution) = " << math::L2_norm(solution)
//                    << " norm(ref_solution) = " << math::L2_norm(ref_solution)
//                    << " rel_error = " << (rel_error > 0 ? d2s<double>(rel_error, 1, 12) : "---") << std::endl;

          if (glob_rel_error != NULL) *glob_rel_error = rel_error;

        } // if reference mesh is provided, we compare the solution

        else // if there is no comparison, we just output the snapshot
        {
          const std::string fname = TESTOUT_DIR + fname_diff +
                                    "_t" + d2s<int>(time_step) + ".vtu";
          dof_handler.write_coarsescale_solution_vtu(fname, solution,
                                                     "t="+d2s<double>(time));
        }

      } // if fname_diff is not "", a snapshot is outputted

    }
  } // time loop

  if (seismofile != NULL)
    seismogram_out.close();
}

//==============================================================================
//
// GMsFEM
//
//==============================================================================
Vector gmsfem_acoustic_2(CoarseMeshPtr &cmesh,
                         FunctionPtr coef_mass,
                         FunctionPtr coef_stif,
                         int fe_order_coarse,
                         int fe_order_fine,
                         int n_boundary_bf,
                         int n_interior_bf,
                         Eigensolver bound_eigensolver,
                         Eigensolver inter_eigensolver,
                         bool show_time_eigensolver,
                         double gamma,
                         const Function &rhs_space,
                         const Function &rhs_time,
                         int n_time_steps,
                         double dt,
                         double &offline_time,
                         double &online_time,
                         std::shared_ptr<GMsDoFHandler> &dof_handler)
{
  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool boundary_edge_mass_matrix = true;
#if defined(TIME)
  double t0 = get_wall_time(), t1;
#endif

  double t0 = get_wall_time();

  // 1. compute multiscale basis functions and distribute dofs
  CoarseFEMeshPtr fe_mesh(new CoarseFiniteElementMesh(cmesh,
                                                      fe_order_coarse,
                                                      fe_order_fine));
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         n_boundary_bf,
                         n_interior_bf,
                         bound_eigensolver,
                         inter_eigensolver,
                         boundary_regularization,
                         interior_regularization,
                         boundary_take_first_eigen,
                         interior_take_first_eigen,
                         boundary_edge_mass_matrix,
                         show_time_eigensolver);

  dof_handler = std::shared_ptr<GMsDoFHandler>(new GMsDoFHandler(fe_mesh, gamma));
  dof_handler->distribute_dofs();

  // 2. build inverse mass matrix
  std::vector<std::set<int> > connections;
  dof_handler->mass_dofs_connections(connections);
  CSRPattern mass_pattern(connections);
  PetscMatrixPtr invM(new PetscMatrix(mass_pattern));
  dof_handler->build_inv_mass_matrix(*invM.get());
  invM->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- INVERSE MASS MATRIX ----- \n";
  std::ofstream invmout("inv_mass_matrix_.dat");
  invM->matlab_view(mass_pattern, invmout);
  invmout.close();
#endif
#if defined(TIME)
  t1 = get_wall_time();
  std::cout << "build inverse mass matrix, time = " << t1-t0 << std::endl;
  t0 = t1;
#endif

  // 3. build global DG matrix
  connections.clear();
  dof_handler->dg_dofs_connections(connections);
  CSRPattern dg_pattern(connections);
  PetscMatrixPtr S(new PetscMatrix(dg_pattern));
  dof_handler->build_stiffness_matrix(*coef_stif.get(), *S.get());
  dof_handler->add_boundary_edges_matrices(*coef_stif.get(), *S.get());
  dof_handler->add_interior_edges_matrices(*coef_stif.get(), *S.get());
  S->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- DG MATRIX ----- \n";
  std::ofstream dgout("dg_matrix__.dat");
  S->matlab_view(dg_pattern, dgout);
  dgout.close();
#endif
#if defined(TIME)
  t1 = get_wall_time();
  std::cout << "build global DG matrix, time = " << t1-t0 << std::endl;
  t0 = t1;
#endif

  // 4. inv_mass_mat * dg_mat
  const int N = dof_handler->n_dofs();
  DensePattern dense(N, N);
  PetscMatrixPtr invMS(new PetscMatrix(dense));
  invMS->AB(invM, S);
  invMS->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- INVERSE MASS * DG MATRIX ----- \n";
  std::ofstream invdgout("inv_mass_dg_matrix__.dat");
  invMS->full_view(dense, invdgout);
  invdgout.close();
#endif
#if defined(TIME)
  t1 = get_wall_time();
  std::cout << "inv_mass_mat * dg_mat, time = " << t1-t0 << std::endl;
  t0 = t1;
#endif

  // 5. Global RHS
  Vector rhs_vector;
  dof_handler->build_vector(rhs_space, rhs_vector);
  double t1 = get_wall_time();

  offline_time = t1 - t0;

  // 5. online stage
  Vector solution(N);   // numerical solution on the current (n-th) time step
  Vector solution_1(N); // numerical solution on the (n-1)-th time step
  Vector solution_2(N); // numerical solution on the (n-2)-th time step

  online_time = 0;
  for (int time_step = 2; time_step <= n_time_steps; ++time_step)
  {
    t0 = get_wall_time();

    const double time = time_step * dt; // current time
    rhs_time.set_time(time - dt);
    const double r = rhs_time.value(Point()); // point doesn't matter

    const Vector tmp_0 = Mv(invMS, solution_1);
    const Vector tmp_1 = Mv(invM, r*rhs_vector);
    solution = 2.*solution_1 - solution_2 - dt*dt*(tmp_0 - tmp_1);
    solution_2 = solution_1; // reassign the solutions on the previuos time steps
    solution_1 = solution;
    t1 = get_wall_time();
    online_time += t1 - t0;
  } // time loop

  return solution;
}

#endif // ACOUSTIC2D_AUX_HPP
