#ifndef TEST_FEMPLUS_GMSFEM_BUILD_RHS_HPP
#define TEST_FEMPLUS_GMSFEM_BUILD_RHS_HPP

#include "config.hpp"
#include "analytic_functions.hpp"
#include "femplus/coarse_mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/dg_finite_element_mesh.hpp"
#include "femplus/coarse_finite_element_mesh.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/vector.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/constant_function.hpp"
#include "femplus/solver.hpp"
#include "femplus/math_functions.hpp"
#include "femplus/gms_dof_handler.hpp"
#include "femplus/mixed_poly_rect_mesh.hpp"
#include "femplus/coarse_mixed_poly_rect_mesh.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/coarse_finite_element.hpp"
#include <gtest/gtest.h>
#if defined(USE_BOOST)
  #include "boost/format.hpp"
#endif

using namespace femplus;

//#define SHOW_MAT



// =============================================================================
TEST(GMsFEMBuildRHS, rectangles)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int Nx = 2;
  const int Ny = 2;
  const int nx = 3;
  const int ny = 3;

  RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                FineMeshType::Rectangular));

  cmesh->build_coarse_mesh();
  cmesh->build_fine_mesh();

  const int coarse_order = 1;
  const int fine_order = 1;
  CoarseFEMeshPtr fe_mesh(new CoarseFiniteElementMesh(cmesh,
                                                      coarse_order,
                                                      fine_order));

  const int n_boundary_bf = 1;
  const int n_interior_bf = 1;
  FunctionPtr coef_mass(new ConstantFunction(1));
  FunctionPtr coef_stif(new ConstantFunction(1));

  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool boundary_edge_mass_matrix = true;
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         n_boundary_bf,
                         n_interior_bf,
                         LAPACK,
                         LAPACK,
                         boundary_regularization,
                         interior_regularization,
                         boundary_take_first_eigen,
                         interior_take_first_eigen,
                         boundary_edge_mass_matrix);

//  fe_mesh->write_interior_basis("RECT_COARSE_TRI_FINE_INTERIOR.vtu");
//  fe_mesh->write_boundary_basis("RECT_COARSE_TRI_FINE_BOUNDARY.vtu");

  const int gamma = 1;
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();

  mesh->numerate_edges();

  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  PatternPtr mass_pattern(new CSRPattern(connections));

  connections.clear();
  dof_handler.dg_dofs_connections(connections);
  PatternPtr dg_pattern(new CSRPattern(connections));

  Vector system_rhs; // right hand side vector

  PetscMatrix system_mat(*dg_pattern.get()); // global DG matrix
  PetscMatrix mass_mat(*mass_pattern.get()); // global mass matrix for RHS

  dof_handler.build_stiffness_matrix(*coef_stif.get(), system_mat);
  system_mat.final_assembly();
  dof_handler.build_mass_matrix(*coef_mass.get(), mass_mat);
  mass_mat.final_assembly();

  ConstantFunction rhs_function(1);
  //RHSFunction1 rhs_function;
  dof_handler.build_vector(rhs_function, system_rhs);

  const std::string filename = "rhs_N_" + d2s<int>(Nx) +
                               "_n_" + d2s<int>(nx) + ".vtp";
  dof_handler.write_solution_vtp("./",   // path to 'results' directory
                                 filename,
                                 system_rhs,
                                 "system_rhs");


  // allocate the memory
  Vector rhs_vector(dof_handler.n_dofs());

  // for each coarse element
  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    // current coarse finite element
    const CoarseFiniteElement *cf_element = fe_mesh->element(el);

    Vector global_finescale_rhs;
    cf_element->global_vector(rhs_function,
                              global_finescale_rhs);

    Vector local_coarsescale_rhs = Mv(cf_element->R(), global_finescale_rhs);

#if defined(SHOW_MAT)
    std::cout << "\n\nglobal finescale rhs for " << el << " element\n";
    std::cout << global_finescale_rhs << "\n";

    std::cout << "\nR matrix\n";
    cf_element->R()->view();

    std::cout << "\nlocal coarsescale rhs for " << el << " element\n";
    std::cout << local_coarsescale_rhs << "\n";
#endif

    for (int di = 0; di < cf_element->n_dofs(); ++di)
    {
      const int dof_i = cf_element->dof_number(di);
      rhs_vector(dof_i) += local_coarsescale_rhs(di);
    }
  } // loop over coarse finite elements

#if defined(SHOW_MAT)
    std::cout << "\n\nglobal coarse rhs\n";
    std::cout << rhs_vector << "\n";
#endif

}


#endif // TEST_FEMPLUS_GMSFEM_BUILD_RHS_HPP
