#ifndef FEMPLUS_TEST_GMSFEM_WAVE_ACOUSTIC_2D_HPP
#define FEMPLUS_TEST_GMSFEM_WAVE_ACOUSTIC_2D_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/rectangular_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/coarse_finite_element_mesh.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/gms_dof_handler.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/dense_pattern.hpp"
#include "femplus/point.hpp"
#include "femplus/mixed_poly_rect_mesh.hpp"
#include "femplus/coarse_mixed_poly_rect_mesh.hpp"

#include "source_functions.hpp"
#include "fem_acoustic_aux.hpp"

using namespace femplus;


//==============================================================================
//
// Fully working examples of solving wave acoustic equation in 2D with GMsFEM
//
//==============================================================================

//==============================================================================
//
// Homogeneous medium
//
//==============================================================================
#if 1
TEST(GMsFEM_wave_acoustic_orig_domain, rectangular_domain_homogeneous)
{
  const double x0 = 0.;
  const double x1 = 1000.;
  const double y0 = 0.;
  const double y1 = 1000.;
  const int    Nx = 11;
  const int    Ny = 11;
  const int    nx = 19;
  const int    ny = 19;
  const int fe_order_coarse = 1;
  const int fe_order_fine   = 1;

  const double rho = 1e+3;
  const double vp  = 3e+3;
  const double kappa = rho * vp * vp;

  Eigensolver bound_eigensolver = KRYLOV;
  Eigensolver inter_eigensolver = KRYLOV;
  const bool show_time_eig      = false;

  const double gamma = 100.;

  const double tend = 0.15;
  const int      nt = 1000;
  const double   dt = tend / nt;

//  const double hx = (x1-x0)/(Nx*nx);
//  const double hy = (y1-y0)/(Ny*ny);
  const double xc = 0.5*(x1-x0);
  const double yc = 0.5*(y1-y0);
  const double source_support   = 1.; // meters
  const double source_frequency = 30.; // Hz
  const Point  source_center    = Point(xc, yc);

  const int time_step_snapshot = nt; // every *-th time step is outputted for 2D snapshots
  const int time_step_seis = 1; // every *-th time step is outputted for seismogram

  const double angle_dis = 10.; // in degrees
  const double seis_radius = 150.; // radius of circular line around a source
                                   // center, where receivers are located

  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new ConstantFunction(kappa));
  GaussianScalar rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  double offtime, ontime;
  std::string seismofile, fname, filemesh;
  std::vector<Point> receivers;
  std::vector<Vector> values_receivers; // 1 dim - time, 2 dim - receiver points
  std::vector<Vector> right_values; // 1 dim - recevier points, 2 dim - time
  std::vector<std::string> nams;
  std::vector<Point> time;

  std::shared_ptr<RectangularMesh> coarse_base(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  std::shared_ptr<CoarseMesh> coarse_mesh(new CoarseRectangularMesh(coarse_base, nx, ny));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();

  filemesh = TESTOUT_DIR + "gms_r_ss_" + d2s<int>(source_support) + "_N_"
             + d2s<int>(Nx) + "_n_" + d2s<int>(nx) + "_support.msh";
  coarse_base->write_msh(filemesh);
  write_geo_support(filemesh, source_center, source_support);

  filemesh = TESTOUT_DIR + "gms_r_ss_" + d2s<int>(source_support) + "_N_"
             + d2s<int>(Nx) + "_n_" + d2s<int>(nx) + "_fine.msh";
  coarse_mesh->write_msh(filemesh);
  write_geo_support(filemesh, source_center, source_support);

  const int Nnx = 199; //Nx*nx;
  const int Nny = 199; //Ny*ny;
  MeshPtr f_r_mesh(new RectangularMesh(x0, x1, y0, y1, Nnx, Nny));
  f_r_mesh->build();

  filemesh = TESTOUT_DIR + "fem_r_ss_" + d2s<int>(source_support) + "_N_"
             + d2s<int>(Nnx) + "_support.msh";
  f_r_mesh->write_msh(filemesh);
  write_geo_support(filemesh, source_center, source_support);

  circular_points(source_center, seis_radius, angle_dis, receivers);
  write_circle(TESTOUT_DIR + "circ_receivers.geo", source_center, receivers);

//  const Point end_point = f_r_mesh->max_coord();

  // ----------------- loop over number of basis functions ---------------------
  const int nb_per[] = { 20 }; //, 40 };
  const int ni_per[] = { 5 }; //, 10 }; //, 11, 13, 15 };
  double glob_time = 0;
  for (unsigned nb = 0; nb < sizeof(nb_per)/sizeof(int); ++nb)
  {
    const int n_boundary_bf = int((2*(nx+1) + 2*(ny-1)) * nb_per[nb] * 0.01);
    int n_boundary_bf_source = n_boundary_bf;
    for (unsigned ni = 0; ni < sizeof(ni_per)/sizeof(int); ++ni)
    {
      double loc_time = get_wall_time();

      const int n_interior_bf = int((nx-1)*(ny-1) * ni_per[ni] * 0.01);
      int n_interior_bf_source = n_interior_bf;

      double rel_error_single, rel_error_double;

      std::string fname_base = "gms_R1_ss_" + d2s<int>(source_support) + "_N_"
                               + d2s<int>(Nx) + "_n_" + d2s<int>(nx) +
                               "_nb_" + d2s<int>(n_boundary_bf) +
                               "_ni_" + d2s<int>(n_interior_bf) + "_nosource";
      seismofile = TESTOUT_DIR + fname_base;
      gmsfem_acoustic(coarse_mesh, coef_mass, coef_stif, fe_order_coarse, fe_order_fine,
                      n_boundary_bf, n_interior_bf, bound_eigensolver, inter_eigensolver,
                      show_time_eig, gamma, &rhs_space, rhs_time, nt, dt, fname_base,
                      time_step_snapshot, time_step_seis, f_r_mesh, &offtime, &ontime,
                      &seismofile, &receivers, &values_receivers,
                      n_boundary_bf_source, n_interior_bf_source, &rel_error_single);

//      right_values.resize(receivers.size());
//      nams.resize(receivers.size());
//      for (int i = 0; i < (int)receivers.size(); ++i) {
//        nams[i] = "receiver " + d2s<int>(i*angle_dis);
//        std::vector<double> val(values_receivers.size()); // values for ONE receiver over time
//        for (int j = 0; j < (int)values_receivers.size(); ++j)
//          val[j] = values_receivers[j](i);
//        right_values[i] = Vector(val);
//      }
//      time.resize(values_receivers.size());
//      for (int i = 0; i < (int)values_receivers.size(); ++i)
//        time[i] = Point((i+1)*dt, 0, 0);
//      fname = TESTOUT_DIR + fname_base + "_values_receivers.csv";
//      print_csv(fname, time, right_values, nams);

      n_boundary_bf_source *= 2;
      n_interior_bf_source *= 2;
      fname_base = "gms_R1_ss_" + d2s<int>(source_support) + "_N_"
                   + d2s<int>(Nx) + "_n_" + d2s<int>(nx) +
                   "_nb_" + d2s<int>(n_boundary_bf) +
                   "_ni_" + d2s<int>(n_interior_bf) +
                   "_nbs_" + d2s<int>(n_boundary_bf_source) +
                   "_nis_" + d2s<int>(n_interior_bf_source);
      seismofile = TESTOUT_DIR + fname_base;
      gmsfem_acoustic(coarse_mesh, coef_mass, coef_stif, fe_order_coarse, fe_order_fine,
                      n_boundary_bf, n_interior_bf, bound_eigensolver, inter_eigensolver,
                      show_time_eig, gamma, &rhs_space, rhs_time, nt, dt, fname_base,
                      time_step_snapshot, time_step_seis, f_r_mesh, &offtime, &ontime,
                      &seismofile, &receivers, &values_receivers,
                      n_boundary_bf_source, n_interior_bf_source, &rel_error_double);

      double elap_time = get_wall_time() - loc_time;
      glob_time += elap_time;

      //std::cout.setf(std::ios::floatfield);
      std::cout.precision(4);
      std::cout << nb_per[nb] << " & " << n_boundary_bf << " & "
                << ni_per[ni] << " & " << n_interior_bf << " & "
                << 100*rel_error_single << " & " << 100*rel_error_double
                << " \\\\\hline" << std::endl;

    } // interior basis functions
  } // boundary basis functions
}
#endif


//==============================================================================
//
// Salt dome flat
//
//==============================================================================
#if 0
TEST(GMsFEM_wave_acoustic, salt_dome_flat)
{
  const std::string file_layers = TESTFILES_DIR + "/salt_dome_properties.dat";
  const std::string F_R_name  = TESTFILES_DIR + "/salt_dome_flat_F_R.msh";
  const std::string F_T_name  = TESTFILES_DIR + "/salt_dome_flat_F_UT.msh";
  const std::string C_RR_name = "salt_dome_flat_C_RR.msh";

  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int    Nx = 10;
  const int    Ny = 10;
  const int    nx = 15;
  const int    ny = 15;
  const int fe_order_coarse = 1;
  const int fe_order_fine   = 1;

  const int n_boundary_bf = 25;
  const int n_interior_bf = 15;
  Eigensolver bound_eigensolver = KRYLOV;
  Eigensolver inter_eigensolver = KRYLOV;
  const bool show_time_eig      = false;

  const double gamma = 100.;

  const double tend = 0.3;
  const int      nt = 2500;
  const double   dt = tend / nt;
  const int time_step_output = nt; //300; // every *-th time step is outputted

  const double hx = (x1-x0)/(Nx*nx);
  const double hy = (y1-y0)/(Ny*ny);
  const double xc = 500;
  const double yc = 950;
  const double source_frequency = 20.;
  const double source_support   = 20.; // in meters
  const Point  source_center    = Point(xc, yc);

  FunctionPtr coef_mass(new CoefRhoLayers(file_layers));
  FunctionPtr coef_stif(new CoefKappaLayers(file_layers));
  SourceGaussAcousticRectangle rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  // ------------------------------ F-R --------------------------------
  const int Nnx = Nx*nx;
  const int Nny = Ny*ny;
  MeshPtr f_r_mesh(new RectangularMesh(x0, x1, y0, y1, Nnx, Nny));
  f_r_mesh->read(F_R_name);
  FEMeshPtr fe_rmesh(new LagrangeMesh(f_r_mesh, fe_order_fine));
  std::shared_ptr<CGDoFHandler> dof_handler_R;
  Vector solution_R = fem_acoustic(fe_rmesh, *coef_mass.get(), *coef_stif.get(),
                                   rhs_space, rhs_time,
                                   nt, dt, time_step_output, "",
                                   dof_handler_R);

  // ------------------------------ F-T --------------------------------
  MeshPtr f_t_mesh(new TriangularMesh(F_T_name));
  FEMeshPtr fe_tmesh(new LagrangeMesh(f_t_mesh, fe_order_fine));
  std::shared_ptr<CGDoFHandler> dof_handler_T;
  Vector solution_T = fem_acoustic(fe_tmesh, *coef_mass.get(), *coef_stif.get(),
                                   rhs_space, rhs_time,
                                   nt, dt, time_step_output, "",
                                   dof_handler_T);

  // --------------------------------- C-RR --------------------------------
  double t0 = get_wall_time();
  std::shared_ptr<RectangularMesh> coarse_base(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  std::shared_ptr<CoarseMesh> coarse_mesh(new CoarseRectangularMesh(coarse_base, nx, ny));
  std::shared_ptr<GMsDoFHandler> dof_handler_C;

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_mesh->set_material_ids(f_r_mesh.get());
  coarse_base->numerate_edges();
  double t1 = get_wall_time();
  double offline_time = t1 - t0;

  coarse_mesh->write_msh(C_RR_name);

  double off_time, online_time;
  const Vector solution_C =
  gmsfem_acoustic_2(coarse_mesh, coef_mass, coef_stif, fe_order_coarse, fe_order_fine,
                    n_boundary_bf, n_interior_bf, bound_eigensolver, inter_eigensolver,
                    show_time_eig, gamma, rhs_space, rhs_time, nt, dt, off_time, online_time,
                    dof_handler_C);

  std::cout << " offline time = " << offline_time + off_time
            << " online time = " << online_time << std::endl;

  // ------------------------- comparison --------------------------------
  Vector sol_C_at_T_dofs; // GMsFEM solution at reference dofs
  // recompute GMsFEM solution at reference dofs points
  double rel_error = dof_handler_C->compute_error(dof_handler_T->dofs(),
                                                  solution_T,
                                                  solution_C,
                                                  sol_C_at_T_dofs);

  std::cout << "rel error: F-T vs C-RR = " << rel_error << std::endl;

//  Vector sol_R_at_T_dofs; // F-R solution at reference dofs
//  // recompute GMsFEM solution at reference dofs points
//  rel_error = dof_handler_R->compute_error(dof_handler_T->dofs(),
//                                           solution_T,
//                                           solution_R,
//                                           sol_R_at_T_dofs);

//  std::cout << "rel error: F-T vs F-R = " << rel_error << std::endl;

  Vector sol_C_at_R_dofs; // F-R solution at reference dofs
  // recompute GMsFEM solution at reference dofs points
  rel_error = dof_handler_C->compute_error(dof_handler_R->dofs(),
                                           solution_R,
                                           solution_C,
                                           sol_C_at_R_dofs);

  std::cout << "rel error: F-R vs C-RR = " << rel_error << std::endl;

  std::string fname = "salt_dome_flat_F_T_C_RR.vtu";
  std::vector<Vector> output_sol(3);
  output_sol[0] = solution_T;
  output_sol[1] = sol_C_at_T_dofs;
  output_sol[2] = solution_T - sol_C_at_T_dofs;
  std::vector<std::string> sol_names(3);
  sol_names[0] = "1_reference_solution_F_T";
  sol_names[1] = "2_gmsfem_solution_at_reference_solution_dofs";
  sol_names[2] = "3_difference_between_them";
  dof_handler_T->write_solution_vtu(fname, output_sol, sol_names);

  fname = "salt_dome_flat_F_R_C_RR.vtu";
  output_sol[0] = solution_R;
  output_sol[1] = sol_C_at_R_dofs;
  output_sol[2] = solution_R - sol_C_at_R_dofs;
  sol_names[0] = "1_reference_solution_F_R";
  sol_names[1] = "2_gmsfem_solution_at_reference_solution_dofs";
  sol_names[2] = "3_difference_between_them";
  dof_handler_R->write_solution_vtu(fname, output_sol, sol_names);

  // comparison in a subdomain
  const double subx0 = 300;
  const double subx1 = 700;
  const double suby0 = 600;
  const double suby1 = 1000;
  const int subnx = 50;
  const int subny = 50;
  MeshPtr sub_mesh(new RectangularMesh(subx0, subx1, suby0, suby1, subnx, subny));
  sub_mesh->build();
  FEMeshPtr sub_fe_mesh(new LagrangeMesh(sub_mesh, 1));
  CGDoFHandler sub_dh(sub_fe_mesh);
  sub_dh.distribute_dofs();

  const Vector solC_sub = dof_handler_C->compute_solution_at_points(sub_dh.dofs(),
                                                                    solution_C);
  const Vector solR_sub = dof_handler_R->compute_solution_at_points(sub_dh.dofs(),
                                                                    solution_R);
  const Vector solT_sub = dof_handler_T->compute_solution_at_points(sub_dh.dofs(),
                                                                    solution_T);
  std::cout << "rel err: T vs R in subdomain = " << math::rel_error(solT_sub, solR_sub) << std::endl;
  std::cout << "rel err: T vs C in subdomain = " << math::rel_error(solT_sub, solC_sub) << std::endl;
  std::cout << "rel err: R vs C in subdomain = " << math::rel_error(solR_sub, solC_sub) << std::endl;

  fname = "salt_dome_flat_F_T_C_RR_sub.vtu";
  output_sol[0] = solT_sub;
  output_sol[1] = solC_sub;
  output_sol[2] = solT_sub - solC_sub;
  sol_names[0] = "reference solution";
  sol_names[1] = "GMsFEM solution";
  sol_names[2] = "difference";
  sub_dh.write_solution(fname, output_sol, sol_names);

  fname = "salt_dome_flat_F_R_C_RR_sub.vtu";
  output_sol[0] = solR_sub;
  output_sol[1] = solC_sub;
  output_sol[2] = solR_sub - solC_sub;
  sol_names[0] = "reference solution";
  sol_names[1] = "GMsFEM solution";
  sol_names[2] = "difference";
  sub_dh.write_solution(fname, output_sol, sol_names);

  // comparison along lines
  const double x_ver   = 500.; // vertical line
  const double y_ver_0 = 10.;
  const double y_ver_1 = 990.;
  const double x_hor_0 = 10.;  // horizontal line
  const double x_hor_1 = 990.;
  const double y_hor   = 500.;
  const double dx = 5.;
  const double dy = 5.;

  std::vector<Point> points;
  Vector sol_C_along_ver = dof_handler_C->compute_solution_along_line(x_ver, x_ver, y_ver_0, y_ver_1,
                                                                      dx, dy, solution_C, points);
  Vector sol_R_along_ver = dof_handler_R->compute_solution_along_line(x_ver, x_ver, y_ver_0, y_ver_1,
                                                                      dx, dy, solution_R, points);
  Vector sol_T_along_ver = dof_handler_T->compute_solution_along_line(x_ver, x_ver, y_ver_0, y_ver_1,
                                                                      dx, dy, solution_T, points);
  output_sol[0] = sol_T_along_ver; sol_names[0] = "F-T";
  output_sol[1] = sol_R_along_ver; sol_names[1] = "F-R";
  output_sol[2] = sol_C_along_ver; sol_names[2] = "C-RR";
  std::string csv_fname = "compare_along_ver.csv";
  print_csv(csv_fname, points, output_sol, sol_names);

  Vector sol_C_along_hor = dof_handler_C->compute_solution_along_line(x_hor_0, x_hor_1, y_hor, y_hor,
                                                                      dx, dy, solution_C, points);
  Vector sol_R_along_hor = dof_handler_R->compute_solution_along_line(x_hor_0, x_hor_1, y_hor, y_hor,
                                                                      dx, dy, solution_R, points);
  Vector sol_T_along_hor = dof_handler_T->compute_solution_along_line(x_hor_0, x_hor_1, y_hor, y_hor,
                                                                      dx, dy, solution_T, points);
  output_sol[0] = sol_T_along_hor; sol_names[0] = "F-T";
  output_sol[1] = sol_R_along_hor; sol_names[1] = "F-R";
  output_sol[2] = sol_C_along_hor; sol_names[2] = "C-RR";
  csv_fname = "compare_along_hor.csv";
  print_csv(csv_fname, points, output_sol, sol_names);
}
#endif

//==============================================================================
//
// Salt dome topo
//
//==============================================================================
#if 0
TEST(GMsFEM_wave_acoustic, salt_dome_topo)
{
  // ------------------------ Get top surface topography -------------------
  const std::string fname = TESTFILES_DIR + "/salt_dome_for_topo_points.msh";
  TriangularMesh tmesh(fname); // read the mesh
  std::vector<int> top_points;
  std::vector<Point> top_surface;
  for (int i = 0; i < tmesh.n_boundary_elements(); ++i) {
    const MeshElement *line = tmesh.boundary_element(i);
    if (line->material_id() == Mesh::TOP_LINE) {
      for (int v = 0; v < line->n_vertices(); ++v) {
        const int vert = line->vertex_number(v);
        if (std::find(top_points.begin(), top_points.end(), vert) == top_points.end()) {
          top_points.push_back(vert);
          const Point to_add = Point(tmesh.vertex(vert).x(),
                                     tmesh.vertex(vert).y() + 0.15); // add 0.15 m
          top_surface.push_back(to_add);
        }
      }
    }
  }
  std::sort(top_surface.begin(), top_surface.end(), Point::compare_by_x);

  // ----------------------- Build coarse mixed poly-rect mesh --------------
  const double zero_surface = 933.33333333333333333333;
  const double depth = 933.33333333333333333333;
  const int Nx = 10;
  const int Ny = 11;
  const int nx = 15;
  const int ny = 14;
  const double percent_accept = 1.;
  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, zero_surface, depth,
                                                      Nx, Ny, percent_accept));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny,
                                                 FineMeshType::Rectangular,
                                                 FineMeshType::Triangular));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();
  mpr_mesh->numerate_edges();

  // ---------------------- Read F-R mesh to assign properties --------------
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int Nnx = 150;//Nx*nx;
  const int Nny = 150;//Ny*ny;
  const std::string rect_file = TESTFILES_DIR + "/salt_dome_flat_F_R.msh";
  MeshPtr f_r_mesh(new RectangularMesh(x0, x1, y0, y1, Nnx, Nny));
  f_r_mesh->read(rect_file);

  // ---------------------- Assign properties to C-RR mesh ----------------
  const int top_layer_id = 1;
  CoarseMixedPolyRectMesh *cmesh = dynamic_cast<CoarseMixedPolyRectMesh*>(mesh.get());
  cmesh->set_material_rect(f_r_mesh.get());
  cmesh->set_material_poly(top_layer_id);
  mpr_mesh->write_geo("salt_dome_topo_C1.geo");
  mesh->write_msh("salt_dome_topo_C1.msh");



  // --------------------- GMsFEM solution and comparison ---------------
  const std::string file_layers = TESTFILES_DIR + "/salt_dome_properties.dat";
  const std::string F_UT_name  = TESTFILES_DIR + "/salt_dome_topo_F_UT.msh";

  const int fe_order_coarse = 1;
  const int fe_order_fine   = 1;

  const int n_boundary_bf = 25;
  const int n_interior_bf = 15;
  Eigensolver bound_eigensolver = KRYLOV;
  Eigensolver inter_eigensolver = KRYLOV;
  const bool show_time_eig      = false;

  const double gamma = 100.;

  const double tend = 0.3;
  const int      nt = 2500;
  const double   dt = tend / nt;
  const int time_step_output = nt; // every *-th time step is outputted

//  const double hx = (x1-x0)/(Nnx);
//  const double hy = (y1-y0)/(Nny);
  const double xc = 500.;
  const double yc = 950.;
  const double source_frequency = 20.;
  const double source_support   = 20.; // in meters
  const Point  source_center    = Point(xc, yc);

  FunctionPtr coef_mass(new CoefRhoLayers(file_layers));
  FunctionPtr coef_stif(new CoefKappaLayers(file_layers));
  SourceGaussAcousticRectangle rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  // ------------------------------ F-UT --------------------------------
  MeshPtr f_ut_mesh(new TriangularMesh(F_UT_name));

  // ------------------------- comparison --------------------------------
  double offline_time, online_time;
  std::string fname_base = "gms_salt_dome_topo_f_ut_N_" + d2s<int>(Nx) + "_n_" +
                           d2s<int>(nx) + "_nb_" + d2s<int>(n_boundary_bf) +
                           "_ni_" + d2s<int>(n_interior_bf);
  gmsfem_acoustic(mesh, coef_mass, coef_stif, fe_order_coarse, fe_order_fine,
                  n_boundary_bf, n_interior_bf, bound_eigensolver, inter_eigensolver,
                  show_time_eig, gamma, rhs_space, rhs_time, nt, dt, fname_base,
                  time_step_output, f_ut_mesh, offline_time, online_time);

  std::cout << " offline time = " << offline_time
            << " online time = " << online_time << std::endl;
}
#endif


#endif // FEMPLUS_TEST_GMSFEM_WAVE_ACOUSTIC_2D_HPP
