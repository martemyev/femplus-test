#ifndef FEMPLUS_TEST_FEM_ACOUSTIC_AUX_HPP
#define FEMPLUS_TEST_FEM_ACOUSTIC_AUX_HPP

#include "femplus/finite_element_mesh.hpp"
#include "femplus/dof_handler.hpp"
#include "femplus/cg_dof_handler.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/constant_function.hpp"
#include "femplus/solver.hpp"

#include "acoustic2D_aux.hpp"

#include <fstream>

using namespace femplus;

//==============================================================================
Vector fem_acoustic(FEMeshPtr fe_mesh,
                    const Function &coef_mass,
                    const Function &coef_stif,
                    const Function &rhs_space,
                    Function &rhs_time,
                    int nt, double dt, int step_snapshot, int step_seismo,
                    const std::string &fname_base,
                    std::shared_ptr<CGDoFHandler> &dof_handler,
                    Vector *source_vector = NULL,
                    const CGDoFHandler *another_dof_handler = NULL,
                    const std::string *seismofile = NULL,
                    const std::vector<Point> *receivers = NULL,
                    std::vector<Vector> *values_receivers = NULL)
{
  PetscMatrixPtr mass;
  PetscMatrixPtr stif;
  Vector rhs_vector;
  fem_acoustic_offline(fe_mesh, coef_mass, coef_stif, &rhs_space,
                       dof_handler, mass, stif, rhs_vector, source_vector,
                       another_dof_handler);

  PetscMatrix system_mat(*mass.get());
  dof_handler->dirichlet(system_mat); //, ConstantFunction(0), &rhs_vector);
  Solver solver(system_mat);

  const int N = dof_handler->n_dofs();
  Vector solution(N), solution_1(N), solution_2(N);

  std::ofstream out;
  if (seismofile != NULL) {
    out.open(seismofile->c_str());
    require(out, "File '" + *seismofile + "' can't be opened");
    require(!receivers->empty(), "The vector of receivers is empty");
    require(values_receivers != NULL, "The vector for values on receivers is not given");
    out.setf(std::ios::scientific);
    out.precision(8);
  }


  for (int time_step = 2; time_step <= nt; ++time_step)
  {
    const double time = time_step * dt; // current time

    rhs_time.set_time(time - dt);
    const double r = rhs_time.value(Point()); // point doesn't matter - only time

    const Vector tmp1 = Mv(mass, 2*solution_1 - solution_2); // M (2 U-1 - U_2)
    const Vector tmp2 = Mv(stif, solution_1); // S U_1
    Vector tmp3 = tmp1 - dt*dt*(tmp2 - r*rhs_vector);

    // Dirichlet
    for (int bd = 0; bd < dof_handler->n_boundary_dofs(); ++bd) {
      const int bdof = dof_handler->boundary_dof_number(bd);
      tmp3(bdof) = 0;
    }

    solver.solve(tmp3, solution);

    // reassign the solutions on the previuos time steps
    solution_2 = solution_1;
    solution_1 = solution;

    if (time_step % step_snapshot == 0) {
      if (fname_base != "") {
        const std::string fname = TESTOUT_DIR + fname_base + "_t" + d2s<int>(time_step) + ".vtu";
        std::vector<Vector> sols(1, solution);
        std::vector<std::string> nams(1, "pressure, Pa");
        dof_handler->write_solution_vtu(fname, sols, nams);
      }
    }
    if (time_step % step_seismo == 0) {
      if (seismofile != NULL) {
        const Vector sol = dof_handler->compute_solution_at_points(*receivers, solution);
        values_receivers->push_back(sol);
        for (int i = 0; i < sol.size(); ++i)
          out << sol(i) << "\t";
        out << "\n";
      }
    }
  } // time loop

  if (seismofile != NULL)
    out.close();

  return solution;
}

#endif // FEMPLUS_TEST_FEM_ACOUSTIC_AUX_HPP
