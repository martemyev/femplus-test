function [U, UU] = fd_wave(n, T, K, a, rho, F_xy, F_t)

% solution of a wave equation with finite difference method.
% it is written by Tat Leung


start_fd = tic;

h = 1. / n;
dt = T / K;

assert(size(a, 1) == size(a, 2), 'a is not square');
assert(size(rho, 1) == size(rho, 2), 'rho is not square');
assert(size(a, 1) == size(rho, 1), 'a and rho are not consistent');
assert(size(a, 1) == n, strcat('a and size of mesh are not consistent: size(a, 1) = ', num2str(size(a, 1)), ' n = ', num2str(n)));


ax = conv2(a,[1,1]/2,'valid');
ay = conv2(a,[1;1]/2,'valid');


f = zeros((n+1)^2,1);


[nx,ny] = meshgrid(2:2:2*(n-1),2:2:2*(n-1));
nx = reshape(nx,(n-1)^2,1);
ny = reshape(ny,(n-1)^2,1);

x = (nx)*h/2;
y = (ny)*h/2;

v = 1:(n+1)^2;
v = reshape(v,n+1,n+1);
vv = [(1:n+1),(2*(n+1):n+1:(n+1)*n),(n+2:n+1:(n+1)*(n-1)+1),(n*(n+1)+1:(n+1)^2)];
v0 = reshape(v(2:n,2:n),(n-1)^2,1)';

clear v
v1 = [v0,v0,v0,v0,v0,vv];
v2 = [(v0-1),(v0-(n+1)),v0,(v0+1),(v0+(n+1)),vv];
v3 = [reshape(  -ax( (1:n-1) , (1:n-1) ) , (n-1)^2 , 1 ) ;...
    reshape(  -ay( (1:n-1) ,  (1:n-1) ) , (n-1)^2 , 1 ) ;...
    reshape( ((ax( (1:n-1) , (1:n-1) ) + ax( (2:n) , (1:n-1) )) + ay( (1:n-1) , (1:n-1) ) + ay( (1:n-1) , (2:n) )) , (n-1)^2, 1 ); ...
    reshape(  -ax( (2:n) , (1:n-1) ) , (n-1)^2 , 1 ) ;...
    reshape(  -ay( (1:n-1) ,  (2:n) ) , (n-1)^2 , 1 ) ;ones((n+1)^2-(n-1)^2,1)]'/h^2;
% v3 = [( - reshape( a(2:2:2*(n-1),((2:2:2*(n-1))-1))', (n-1)^2, 1 )*n^2)', (-b1(x-h,y)*n/2 - reshape( a(((2:2:2*(n-1))-1),((2:2:2*(n-1))))', (n-1)^2, 1 )*(n)^2)', ...
%     ((reshape( a(((2:2:2*(n-1))-1),((2:2:2*(n-1))))', (n-1)^2, 1 )+reshape( a(((2:2:2*(n-1))+1),((2:2:2*(n-1))))', (n-1)^2, 1 )+reshape( a(((2:2:2*(n-1))),((2:2:2*(n-1))-1))', (n-1)^2, 1 )+reshape( a(((2:2:2*(n-1))),((2:2:2*(n-1))+1))', (n-1)^2, 1 ))*(n)^2)', ...
%     (b2(x,y+h)*n/2 - reshape( a(((2:2:2*(n-1))),((2:2:2*(n-1))+1))'*(n)^2,(n-1)^2,1 ))', (b1(x+h,y)*n/2 - reshape( a(((2:2:2*(n-1))+1),((2:2:2*(n-1))))'*(n)^2,(n-1)^2,1))', ones(1,(n+1)^2-(n-1)^2)];

A = sparse(v1,v2,v3,(n+1)^2,(n+1)^2);
f(v0) = F_xy(x,y);

u0 = zeros((n+1)^2,1);
u1 = zeros((n+1)^2,1);

rho = conv2(rho,[1,1;1,1]/4,'valid');

M = sparse( [v0,vv] , [v0,vv] , [reshape(rho,(n-1)^2,1)',ones(1,size(vv,2))] ,(n+1)^2,(n+1)^2);
clear vv

A = M\A;
f = M\f;

fprintf('fd stage before time loop took %f\n', toc(start_fd));

start_timeloop = tic;

for k = 1:K
    U = 2*u1 - u0 - dt^2*( (A*u1-f*F_t(dt*(k-1))) );
    u2 = u1;
    u1 = U;
    u0 = u2;
end

fprintf('fd time loop took %f\n', toc(start_timeloop));

fprintf('fd code took totally %f\n', toc(start_fd));

UU = reshape(U,n+1,n+1);

end % function
