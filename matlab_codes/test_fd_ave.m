function ret = test_fd_ave(param, a, back_to_fine)

% to solve the underlying problem with finite difference method,
% but for averaged coefficients.
% 'param' contains necessary parameters,
% 'coef_a' contains coefficient distribution for a fine scale,
% 'back_to_fine' = 1 means, that after averaging inside nxn blocks
% we solve the porblem on a (N*n x N*n) grid (i.e. fine one),
% 'back_to_fine' = 0 means, that we solve the problems with FDM on
% a coarse grid NxN

N = param.N;
n = param.n;

averaged_a = zeros(N, N);

for i = 1 : N
    ii_beg = (i - 1) * n;
    for j = 1 : N
        jj_beg = (j - 1) * n;
        for ii = 1 : n
            for jj = 1 : n
                averaged_a(i, j) = averaged_a(i, j) + 1 / a(ii_beg + ii, jj_beg + jj);
            end
        end
        assert(abs(averaged_a(i, j)) > 1e-16, 'too small averaged a');
        averaged_a(i, j) = (n*n) / averaged_a(i, j);
        
        if back_to_fine
            for ii = 1 : n
                for jj = 1 : n
                    a(ii_beg + ii, jj_beg + jj) = averaged_a(i, j);
                end
            end
        end
    end
end

[~, filestem, ~] = fileparts(param.COEFFILE);
if back_to_fine
    param.n_fd = N*n;
    param.COEFFILE = strcat(filestem, '_avefine_N', num2str(N), '_n', num2str(n));
    test_fd_wave(param, a);
else
    param.n_fd = N;
    param.COEFFILE = strcat(filestem, '_avecoarse_N', num2str(N), '_n', num2str(n));
    test_fd_wave(param, averaged_a);
end

ret = 0;
end % function
