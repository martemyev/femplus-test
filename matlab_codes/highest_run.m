% this is a script for launching a sequence of programs with
% different coefficient distributions defined in files


%clear; run('/u/artemyev/projects/fem2d_acoustic/coef/co_3_bin_08_250.dat');
%clear; run('/u/artemyev/projects/fem2d_acoustic/coef/co_3_ave_08_250.dat');
%clear; run('/u/artemyev/projects/fem2d_acoustic/coef/co_3_bin_24_250.dat');

%clear; run('/u/artemyev/projects/fem2d_acoustic/coef/co_slop_bin_08_250.dat');
%clear; run('/u/artemyev/projects/fem2d_acoustic/coef/co_slop_ave_08_250.dat');
clear; run('/u/artemyev/projects/fem2d_acoustic/coef/co_slop_bin_24_250.dat');

%clear; run('../vonkarman.dat');
