function [sol_vector, sol1] = main_problem(N,n,E_per,eig_num,gamma,dt,T,Af,F,a,rho,check_solution,RES_DIR)

% this is a core of a GMsFEM implementation.
% it is written by Tat Leung.


%% initialize

siz = N*n;

H = 1/N;
h= H/n;


K = T/dt;




%% main
tic


mass = conv2(ones(n,n),[1, 1; 1, 1]/4);
mass_line = conv(ones(n,1),[1, 1]/2);

element_num = eig_num +4*n*9;

%tic
%[Global_A,WW,MM,num_boundary,num_basis,l]= local_problem_2(N,N,h,a,rho,eig_num,gamma,E_per);
%toc
tic
[Global_A,WW,MM,num_boundary,num_basis,l]= local_problem(N,N,h,a,rho,eig_num,gamma,E_per);
toc

W = sparse( size(Global_A,1) , num_basis(N,N) +(num_boundary(N,N)+eig_num) );
for ii = 1:N
    for jj = 1:N
        
        W( ((ii-1)*N + (jj-1))*(n+1)^2 + (1:(n+1)^2) , num_basis(ii,jj) + (1:(num_boundary(ii,jj)+eig_num)) ) = reshape(WW(:,:,1:(num_boundary(ii,jj)+eig_num),ii,jj),(n+1)^2,(num_boundary(ii,jj)+eig_num));
        
    end
end

%fprintf('size(Global_A, 1) = %d size(Global_A, 2) = %d\n', size(Global_A, 1), size(Global_A, 2));

A = W'*Global_A*W;
A = (A'+A)/2;


f0 = zeros( N^2*(n+1)^2 ,1);

[X,Y] = meshgrid(0:1/(N*n):1);
F1 = F(X,Y)';



for i = 1:N
    for j = 1:N 
            f0( ((j-1)*N + (i-1))*(n+1)^2 + (1:(n+1)^2) ) = reshape(mass.*F1(1+(j-1)*n:j*n+1,1+(i-1)*n:i*n+1)',(n+1)^2,1)*h^2;

    end
end

f = W'*f0;

  u1 = 0*f;
  u0 = u1;
  U = u0;
  


 u1 = 0*f;
  u0 = u1;
  U = u0;
  
  %fprintf('size(U, 1) = %d size(U, 2) = %d size(W, 1) = %d size(W, 2) = %d\n', size(U, 1), size(U, 2), size(W, 1), size(W, 2));

    AA = MM\A;
    ff = MM\f;
    toc
    tic
    
sol = zeros(n+1,n+1,N,N);
sol1 = zeros(siz+1,siz+1);


  for k = 1:K

    U = 2*u1 - u0 - dt^2*(AA*u1-Af((k-1)*dt)*ff);
    u2 = u1;
    u1 = U;
    u0 = u2;

%    if mod(k, check_solution) == 0     
%         fname = strcat(RES_DIR, '/res', '-', num2str(k), '.vts');        
%         print_vts(fname, N, n, X*(1e+3), Y*(1e+3), sol1*(1e-6), a*(1e+6));
%    end

  end
  
  
for i = 1:N
    for j = 1:N
        for kk = 1 : (num_boundary(i,j)+eig_num)
            sol(:,:,i,j) = sol(:,:,i,j) + U( num_basis(i,j) + kk)*WW(:,:,kk,i,j);
        end
        sol1( (j-1)*n+1:j*n+1 , (i-1)*n+1:i*n+1 ) = sol1( (j-1)*n+1:j*n+1 , (i-1)*n+1:i*n+1 ) + mass.*sol(:,:,i,j);
    end
end

sol_vector = reshape(sol1, (N*n+1)*(N*n+1), 1);


  toc


   






    

