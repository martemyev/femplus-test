function sol_vector = test(param, coef_a)

% This is a function implementing
% GMsFEM solution of a wave equation.
% all necessary parameters are sent through 'param' structure.
% there is also 'coef_a' parameter, which
% was designed to provide the coefficient distribution
% read in a higher level function (for example, vonKarman distribution)


start = tic();

if nargin < 2
    coef_a = 0;
end

%% grid_size
N = param.N; %25;
n = param.n; %10;
h = 1/N/n;
T = param.T; %0.4;
K = param.K; %4000;
dt = T/K; %h/1e+1/10;
Nx = N;
Ny = N;
siz1 = Nx*n;
siz2 = Ny*n;

%% num_basis
E_per = param.N_BOUNDARY_BF; %15; %0.05;
eig_num = param.N_INTERIOR_BF; %15;

%% penality_term
gamma = param.GAMMA; %2; %1/2;

coeffile = param.COEFFILE; %'/u/artemyev/projects/fem2d_acoustic/coef/co_3_bin_24_250.dat';

%% source
F0 = param.SOURCE_FREQUENCY; %20;
hH = param.SUPPORT_COEF * h;
xcen = param.SOURCE_CENTER_X; %0.5;
ycen = param.SOURCE_CENTER_Y; %0.1;
Af = @(t) (1 - 2*pi^2*F0^2*(t-2/F0)^2)*exp(-pi^2*F0^2*(t-2/F0)^2);
%F = @(x,y) 1./(hH)^2*exp(-((x-.5).^2+(y-.5).^2)/(hH)^2).*heaviside(3*h - abs(x-.5)).*heaviside(3*h - abs(y-.5));
F = @(x,y)1./(hH)^2*exp(-((x-xcen).^2+(y-ycen).^2)/(hH)^2);

% f(t,x) = Af(t)F(x,y)

%% medium
%fid1 = fopen('velocity.sq');
%aa=fread(fid1,[751 2301],'single');

rho = ones(N*n);
if coef_a == 0
    aa = ones(N*n);
    if strcmp(coeffile, '')
        a = aa;
    else
        aa = load_coef_file(coeffile, aa);
        a = aa*(1e-6);
    end
else
    assert(size(coef_a, 1) == N*n && size(coef_a, 2) == N*n, 'asdasd');
    a = coef_a;
end

%a = a(1:end-1,1:1:end-1);
%a = 0*a + 1;
%[X,Y] = meshgrid(1/size(a,2)/2:1/size(a,2):1,1/size(a,1)/2:1/size(a,1):1);
%[X1,Y1] = meshgrid(1/n/Nx/2:1/n/Nx:1,1/n/Ny/2:1/n/Ny:1);
%a = interp2(X,Y,a,X1,Y1,'spline');
%rho = 1 +0*X1*Y1;
%a = rho;

%create a directory for the results
[~,coefstem,~] = fileparts(coeffile);
FNAME = strcat(pwd, '/res/', coefstem, '_new/gms_b', num2str(param.N_BOUNDARY_BF), '_i', num2str(param.N_INTERIOR_BF), '_', coefstem, '.vts');

% check if necessary directory exists
% ex_code = exist(RES_DIR, 'dir');
% if (ex_code == 0) % directory doesn't exists
%     s = mkdir(RES_DIR); % create the directory
%     assert(s, strcat('Directory ', RES_DIR, ' cannot be created'));
%     ex_code = exist(RES_DIR, 'dir'); % check again
% end
% assert(ex_code == 7, strcat('Directory ', RES_DIR, ' was not created properly'));

fprintf('setups are done\n');

%% solver
[sol_vector, sol] = main_problem(N,n,E_per,eig_num,gamma,dt,T,Af,F,a,rho);%,check_solution,RES_DIR);
fprintf('total time = %f sec\n', toc(start));

assert(size(sol, 1) == size(sol, 2) && size(sol, 1) == N*n+1, 'size of solution is strange');

[X,Y] = meshgrid(0:h:1);
%fname = strcat(RES_DIR, '/fd_solution.vts');
%fprintf('writing in %s\n', fname);
sol_name = strcat('Ms_N', num2str(N), '_n', num2str(n), '_b', num2str(param.N_BOUNDARY_BF), '_i', num2str(param.N_INTERIOR_BF));
print_vts(FNAME, N, n, X*(1e+3), Y*(1e+3), sol*(1e-6), a*(1e+6), sol_name);

end % function
