function ret = print_vts(fname, N, n, X, Y, U, coef_beta, Uname)

% print an xml file for further visualization
% in Paraview


assert(size(X, 1) == size(Y, 1) && size(X, 2) == size(Y, 2), 'sizes of X and Y are inconsistent');
assert(size(X, 1) == size(U, 1) && size(X, 2) == size(U, 2), 'sizes of X and U are inconsistent');
assert(size(X, 1) == N*n+1 && size(X, 2) == N*n+1, 'size of X is inconsistent with fine mesh');
assert(size(coef_beta, 1) == N*n && size(coef_beta, 2) == N*n, 'size of coef_beta is inconsistent with fine mesh');

if (nargin < 8)
    Uname = '';
end 
    
fid = fopen(fname, 'w');

assert(fid ~= -1, strcat('File ', fname, ' cannot be opened'));

fprintf(fid, '<?xml version="1.0"?>\n');
fprintf(fid, '<VTKFile type="StructuredGrid" version="0.1" byte_order="LittleEndian">\n');
fprintf(fid, '  <StructuredGrid WholeExtent="1 %d 1 %d 1 1">\n', N*n+1, N*n+1);
fprintf(fid, '    <Piece Extent="1 %d 1 %d 1 1">\n', N*n+1, N*n+1);
fprintf(fid, '      <PointData Scalars="scalars">\n');
if strcmp(Uname, '')
    fprintf(fid, '        <DataArray type="Float64" Name="U_multiscale_%d_%d" format="ascii">\n', N, n);
else
    fprintf(fid, '        <DataArray type="Float64" Name="%s" format="ascii">\n', Uname);
end
for i = 1 : N*n+1
    for j = 1 : N*n+1
        fprintf(fid, '%2.14e ', U(i, j));
    end
end
fprintf(fid, '\n');
fprintf(fid, '        </DataArray>\n');
fprintf(fid, '      </PointData>\n');
fprintf(fid, '      <CellData Scalars="scalars">\n');
fprintf(fid, '        <DataArray type="Float64" Name="velocity, m/s" format="ascii">\n');
for i = 1 : N*n
    for j = 1 : N*n
        fprintf(fid, '%2.14e ', sqrt(coef_beta(i, j)));
    end
end
fprintf(fid, '\n');
fprintf(fid, '        </DataArray>\n');
fprintf(fid, '      </CellData>\n');
fprintf(fid, '      <Points>\n');
fprintf(fid, '        <DataArray type="Float64" NumberOfComponents="3" format="ascii">\n');
for i = 1 : N*n+1
    for j = 1 : N*n+1
        fprintf(fid, '%f %f 0.0\n', X(i, j), Y(i, j));
    end
end
fprintf(fid, '\n');
fprintf(fid, '        </DataArray>\n');
fprintf(fid, '      </Points>\n');
fprintf(fid, '    </Piece>\n');
fprintf(fid, '  </StructuredGrid>\n');
fprintf(fid, '</VTKFile>\n');

fclose (fid);
ret = 0;
end % function
