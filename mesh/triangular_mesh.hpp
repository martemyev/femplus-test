#ifndef TEST_FEMPLUS_TRIANGULAR_MESH_HPP
#define TEST_FEMPLUS_TRIANGULAR_MESH_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/mesh.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/triangle.hpp"
#include "femplus/mesh_element.hpp"
#include "femplus/line.hpp"
#include "femplus/point.hpp"
#include "femplus/math_functions.hpp"

#include <algorithm>

#include "compare_functions.hpp"

using namespace femplus;

// =============================================================================
TEST(TriangularMesh, ctor_0)
{
  std::vector<Point> points;
  points.push_back(Point(-1, -1));
  points.push_back(Point(0, -1));
  points.push_back(Point(-1, 1));
  const Point beg(-1, -1);
  const Point end(0, 1);

  std::vector<int> vertices;
  vertices.push_back(1);
  vertices.push_back(2);
  vertices.push_back(0);

  const double x_min = std::min_element(points.begin(), points.end(), Point::compare_by_x)->x();
  const double x_max = std::max_element(points.begin(), points.end(), Point::compare_by_x)->x();

  const int n_elements = 10;
  const double cl = (x_max - x_min) / n_elements;

  TriangularMesh mesh(points, vertices, cl);
  for (int i = 0; i < Point::n_coord; ++i)
  {
    EXPECT_FLOAT_EQ(beg.coord(i), mesh.min_coord().coord(i));
    EXPECT_FLOAT_EQ(end.coord(i), mesh.max_coord().coord(i));
  }
}

// =============================================================================
TEST(TriangularMesh, ctor_1)
{
  TriangularMesh mesh(Point(0, 0),
                      Point(1, 0),
                      10,
                      10,
                      0);

  EXPECT_EQ(mesh.n_vertices(), 10);
  EXPECT_EQ(mesh.n_elements(), 10);
  EXPECT_EQ(mesh.n_boundary_elements(), 0);
}

// =============================================================================
TEST(TriangularMesh, ctor_2)
{
  const Point beg(1, 1);
  const Point end(10, 12);
  TriangularMesh mesh(beg,
                      end,
                      10,
                      10,
                      0);

  EXPECT_EQ(mesh.n_vertices(), 10);
  EXPECT_EQ(mesh.n_elements(), 10);
  EXPECT_EQ(mesh.n_boundary_elements(), 0);

  for (int i = 0; i < Point::n_coord; ++i)
  {
    EXPECT_FLOAT_EQ(beg.coord(i), mesh.min_coord().coord(i));
    EXPECT_FLOAT_EQ(end.coord(i), mesh.max_coord().coord(i));
  }
}

// =============================================================================
TEST(TriangularMesh, ctor_3)
{
  const Point beg(1, 1);
  const Point end(10, 12);
  Mesh *mesh = new TriangularMesh(beg,
                                  end,
                                  10,
                                  10,
                                  0);

  EXPECT_EQ(mesh->n_vertices(), 10);
  EXPECT_EQ(mesh->n_elements(), 10);
  EXPECT_EQ(mesh->n_boundary_elements(), 0);

  for (int i = 0; i < Point::n_coord; ++i)
  {
    EXPECT_FLOAT_EQ(beg.coord(i), mesh->min_coord().coord(i));
    EXPECT_FLOAT_EQ(end.coord(i), mesh->max_coord().coord(i));
  }

  delete mesh;
}

// =============================================================================
TEST(TriangularMesh, ctor_4)
{
  const Point beg(1, 1);
  const Point end(10, 12);
  const Mesh &mesh = TriangularMesh(beg,
                                    end,
                                    10,
                                    10,
                                    0);

  EXPECT_EQ(mesh.n_vertices(), 10);
  EXPECT_EQ(mesh.n_elements(), 10);
  EXPECT_EQ(mesh.n_boundary_elements(), 0);

  for (int i = 0; i < Point::n_coord; ++i)
  {
    EXPECT_FLOAT_EQ(beg.coord(i), mesh.min_coord().coord(i));
    EXPECT_FLOAT_EQ(end.coord(i), mesh.max_coord().coord(i));
  }
}

// =============================================================================
TEST(TriangularMesh, ctor_5)
{
  const Point beg(1, 1);
  const Point end(10, 12);
  TriangularMesh tri_mesh(beg,
                          end,
                          10,
                          10,
                          0);
  Mesh &mesh = tri_mesh;

  EXPECT_EQ(mesh.n_vertices(), 10);
  EXPECT_EQ(mesh.n_elements(), 10);
  EXPECT_EQ(mesh.n_boundary_elements(), 0);

  for (int i = 0; i < Point::n_coord; ++i)
  {
    EXPECT_FLOAT_EQ(beg.coord(i), mesh.min_coord().coord(i));
    EXPECT_FLOAT_EQ(end.coord(i), mesh.max_coord().coord(i));
  }
}

// =============================================================================
TEST(TriangularMesh, ctor_0_write_msh)
{
  std::cout << "This test doesn't check anything usefull" << std::endl;

  std::vector<Point> points;
  points.push_back(Point(-1, -1));
  points.push_back(Point(0, -1));
  points.push_back(Point(-1, 1));

  std::vector<int> vertices;
  vertices.push_back(1);
  vertices.push_back(2);
  vertices.push_back(0);

  const double x_min = std::min_element(points.begin(), points.end(), Point::compare_by_x)->x();
  const double x_max = std::max_element(points.begin(), points.end(), Point::compare_by_x)->x();

  const int n_elements = 10;
  const double cl = (x_max - x_min) / n_elements;

  TriangularMesh mesh(points, vertices, cl);
  mesh.set_mesh_generator("gmsh"); // in case when Gmsh lib is not used, the
                                   // Gmsh bin file is used
  mesh.build();

  const std::string mshfile = TESTOUT_DIR + "tri_mesh_build0.msh";
  mesh.write_msh(mshfile);

//  const std::string command = "gmsh " + mshfile;
//  system(command.c_str()); // show the mesh
}

// =============================================================================
TEST(TriangularMesh, build_mesh_triangle)
{
  std::cout << "This test doesn't check anything usefull" << std::endl;

  std::vector<int> vertices;
  vertices.push_back(1);
  vertices.push_back(2);
  vertices.push_back(0);
  Triangle tri(vertices);

  std::vector<Point> points;
  points.push_back(Point(-1, -1));
  points.push_back(Point(0, -1));
  points.push_back(Point(-1, 1));

  const double x_min = std::min_element(points.begin(), points.end(), Point::compare_by_x)->x();
  const double x_max = std::max_element(points.begin(), points.end(), Point::compare_by_x)->x();

  const int n_elements = 10;
  const double cl = (x_max - x_min) / n_elements;

  TriangularMesh mesh(points, &tri, cl);
  mesh.set_mesh_generator("gmsh"); // in case when Gmsh lib is not used, the
                                   // Gmsh bin file is used
  mesh.build();

  const std::string mshfile = TESTOUT_DIR + "tri_mesh_build1.msh";
  mesh.write_msh(mshfile);

//  const std::string command = "gmsh " + mshfile;
//  system(command.c_str()); // show the mesh
}

// =============================================================================
TEST(TriangularMesh, read_msh_0)
{
  Point points[] = { Point(0, 0, 0),
                     Point(1, 0, 0),
                     Point(0, 1, 0)
                   };
  const int n_points = sizeof(points) / sizeof(Point);
  std::vector<Point> vec_points(&points[0],
                                &points[0] + n_points);

  MeshElementPtr tri(new Triangle(1, 2, 3));
  std::vector<MeshElementPtr> vec_triangles;
  vec_triangles.push_back(tri);

  // ----------------------------- ASCII -----------------------------
  TriangularMesh mesh(TESTFILES_DIR + "/test_mesh_0.msh");
  compare_points(mesh, vec_points);
  compare_elements(mesh, vec_triangles, vec_points);

  // ----------------------------- Binary ----------------------------
  TriangularMesh bin_mesh(TESTFILES_DIR + "/test_mesh_bin_0.msh");
  compare_points(bin_mesh, vec_points);
  compare_elements(bin_mesh, vec_triangles, vec_points);
}

// =============================================================================
TEST(TriangularMesh, read_msh_2)
{
  Point points[] = { Point(0, 0, 0),
                     Point(1, 0, 0),
                     Point(1, 1, 0),
                     Point(0, 1, 0),
                     Point(0.499999999998694, 0, 0),
                     Point(1, 0.499999999998694, 0),
                     Point(0.5000000000020591, 1, 0),
                     Point(0, 0.5000000000020591, 0),
                     Point(0.5, 0.5, 0),
                     Point(0.2500000000010295, 0.7500000000010295, 0),
                     Point(0.7500000000010295, 0.7499999999993469, 0),
                     Point(0.7499999999993471, 0.249999999999347, 0),
                     Point(0.2499999999994391, 0.2500000000006863, 0)
                   };
  const int n_points = sizeof(points) / sizeof(Point);
  std::vector<Point> vec_points(&points[0],
                                &points[0] + n_points);

  Triangle triangles[] = { Triangle(7, 4, 10),
                           Triangle(1, 13, 8),
                           Triangle(6, 3, 11),
                           Triangle(2, 12, 5),
                           Triangle(7, 10, 9),
                           Triangle(8, 9, 10),
                           Triangle(7, 9, 11),
                           Triangle(8, 13, 9),
                           Triangle(11, 9, 12),
                           Triangle(5, 9, 13),
                           Triangle(6, 11, 12),
                           Triangle(5, 12, 9),
                           Triangle(1, 5, 13),
                           Triangle(6, 12, 2),
                           Triangle(4, 8, 10),
                           Triangle(7, 11, 3)
                         };
  const int n_triangles = sizeof(triangles) / sizeof(Triangle);
  std::vector<MeshElementPtr> vec_triangles;
  for (int i = 0; i < n_triangles; ++i)
  {
    MeshElementPtr tri(new Triangle(triangles[i]));
    vec_triangles.push_back(tri);
  }

  // ----------------------------- ASCII -----------------------------
  TriangularMesh mesh(TESTFILES_DIR + "/test_mesh_2.msh");
  compare_points(mesh, vec_points);
  compare_elements(mesh, vec_triangles, vec_points);

  // ----------------------------- Binary ----------------------------
  TriangularMesh bin_mesh(TESTFILES_DIR + "/test_mesh_bin_2.msh");
  compare_points(bin_mesh, vec_points);
  compare_elements(bin_mesh, vec_triangles, vec_points);
}

// =============================================================================
TEST(TriangularMesh, numerate_edges_0)
{
  TriangularMesh mesh(TESTFILES_DIR + "/test_mesh_0.msh");
  EXPECT_ANY_THROW(mesh.numerate_edges()); // since the edges should be already numerated

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(0, 2));
  edges.push_back(new Line(1, 2));

  compare_edges(mesh, edges);

  std::vector<int> boundary_edges;
  mesh.boundary_edges_numbers(boundary_edges);
  const int expected_b_edges[] = { 0, 1, 2 };
  EXPECT_EQ(boundary_edges.size(), 3);
  for (unsigned i = 0; i < boundary_edges.size(); ++i)
    EXPECT_EQ(boundary_edges[i], expected_b_edges[i]);
  std::vector<int>::const_iterator b_edge = boundary_edges.begin();
  std::vector<int>::const_iterator end = boundary_edges.end();
  for (; b_edge != end; ++b_edge) {
    const std::vector<int> &cell_num = mesh.cells_of_edge(*b_edge);
    EXPECT_EQ(cell_num.size(), 1);
    EXPECT_EQ(cell_num[0], 0);
  }

  std::vector<int> interior_edges;
  mesh.interior_edges_numbers(interior_edges);
  EXPECT_EQ(interior_edges.size(), 0); // there are no interior edges

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];
}

// =============================================================================
TEST(TriangularMesh, numerate_edges_1)
{
  TriangularMesh mesh(TESTFILES_DIR + "/test_mesh_1.msh");
  EXPECT_ANY_THROW(mesh.numerate_edges()); // since the edges should be already numerated

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(1, 2));
  edges.push_back(new Line(0, 3));
  edges.push_back(new Line(2, 3));
  edges.push_back(new Line(0, 4));
  edges.push_back(new Line(1, 4));
  edges.push_back(new Line(2, 4));
  edges.push_back(new Line(3, 4));

  compare_edges(mesh, edges);

  std::vector<int> boundary_edges;
  mesh.boundary_edges_numbers(boundary_edges);
  const int expected_b_edges[] = { 0, 1, 2, 3 };
  EXPECT_EQ(boundary_edges.size(), 4);
  for (unsigned i = 0; i < boundary_edges.size(); ++i)
    EXPECT_EQ(boundary_edges[i], expected_b_edges[i]);
  const int expected_b_cells[] = { 0, 2, 1, 3 };
  std::vector<int>::const_iterator b_edge = boundary_edges.begin();
  std::vector<int>::const_iterator end = boundary_edges.end();
  for (int i = 0; b_edge != end; ++b_edge, ++i) {
    const std::vector<int> &cell_num = mesh.cells_of_edge(*b_edge);
    EXPECT_EQ(cell_num.size(), 1);
    EXPECT_EQ(cell_num[0], expected_b_cells[i]);
  }

  std::vector<int> interior_edges;
  mesh.interior_edges_numbers(interior_edges);
  const int expected_i_edges[] = { 4, 5, 6, 7 };
  EXPECT_EQ(interior_edges.size(), 4);
  for (unsigned i = 0; i < interior_edges.size(); ++i)
    EXPECT_EQ(interior_edges[i], expected_i_edges[i]);
  const int expected_i_cells[][2] = { { 0, 1 }, { 0, 2 }, { 2, 3 }, { 1, 3 } };
  std::vector<int>::const_iterator i_edge = interior_edges.begin();
  end = interior_edges.end();
  for (int i = 0; i_edge != end; ++i_edge, ++i) {
    const std::vector<int> &cell_num = mesh.cells_of_edge(*i_edge);
    EXPECT_EQ(cell_num.size(), 2);
    for (int j = 0; j < 2; ++j)
      EXPECT_EQ(cell_num[j], expected_i_cells[i][j]);
  }

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];
}

// =============================================================================
TEST(TriangularMesh, init_boundary_elements)
{
  std::cout << "This test doesn't check anything usefull" << std::endl;
  const std::string fname = TESTFILES_DIR + "/salt_dome_flat_F_UT.msh";
  TriangularMesh mesh(fname); // init_boundary_elements is included
  mesh.write(fname);
}

// =============================================================================
TEST(TriangularMesh, is_conforming_small_0)
{
  std::vector<Point> vertices;
  vertices.push_back(Point(0, 0));
  vertices.push_back(Point(1, 0));
  vertices.push_back(Point(2, -1));
  vertices.push_back(Point(3, 1));
  vertices.push_back(Point(0, 2));

  const double x_min = std::min_element(vertices.begin(), vertices.end(), Point::compare_by_x)->x();
  const double x_max = std::max_element(vertices.begin(), vertices.end(), Point::compare_by_x)->x();
  const double y_min = std::min_element(vertices.begin(), vertices.end(), Point::compare_by_y)->y();
  const double y_max = std::max_element(vertices.begin(), vertices.end(), Point::compare_by_y)->y();

  const Point min_coord(x_min, y_min);
  const Point max_coord(x_max, y_max);

  std::vector<const MeshElement*> cells;
  cells.push_back(new Triangle(0, 1, 4));
  cells.push_back(new Triangle(2, 3, 4));

  const int n_boundary_elements = 0;
  TriangularMesh mesh(min_coord,
                      max_coord,
                      vertices.size(),
                      cells.size(),
                      n_boundary_elements);

  mesh.set_vertices(vertices);
  mesh.set_elements(cells);

  mesh.numerate_edges();

  const bool conforming = mesh.is_conforming();
  EXPECT_FALSE(conforming); // this mesh in unconforming
}

// =============================================================================
TEST(TriangularMesh, is_conforming_small_1)
{
  std::vector<Point> vertices;
  vertices.push_back(Point(0, 0));
  vertices.push_back(Point(1, 0));
  vertices.push_back(Point(3, 1));
  vertices.push_back(Point(0, 2));

  const double x_min = std::min_element(vertices.begin(), vertices.end(), Point::compare_by_x)->x();
  const double x_max = std::max_element(vertices.begin(), vertices.end(), Point::compare_by_x)->x();
  const double y_min = std::min_element(vertices.begin(), vertices.end(), Point::compare_by_y)->y();
  const double y_max = std::max_element(vertices.begin(), vertices.end(), Point::compare_by_y)->y();

  const Point min_coord(x_min, y_min);
  const Point max_coord(x_max, y_max);

  std::vector<const MeshElement*> cells;
  cells.push_back(new Triangle(0, 1, 3));
  cells.push_back(new Triangle(1, 2, 3));

  const int n_boundary_elements = 0;
  TriangularMesh mesh(min_coord,
                      max_coord,
                      vertices.size(),
                      cells.size(),
                      n_boundary_elements);

  mesh.set_vertices(vertices);
  mesh.set_elements(cells);

  mesh.numerate_edges();

  const bool conforming = mesh.is_conforming();
  EXPECT_TRUE(conforming); // this mesh in conforming
}

// =============================================================================
TEST(TriangularMesh, compare_two_temp_meshes)
{
  TriangularMesh mesh0("./test_mesh_0.msh");
  TriangularMesh mesh1("./test_mesh_1.msh");

  // compare the vertices
  EXPECT_EQ(mesh0.n_vertices(), mesh1.n_vertices());
  for (int i = 0; i < mesh0.n_vertices(); ++i)
  {
    EXPECT_DOUBLE_EQ(math::norm(mesh0.vertex(i) - mesh1.vertex(i)), 0);
  }

  // compare the elements
  EXPECT_EQ(mesh0.n_elements(), mesh1.n_elements());
  for (int i = 0; i < mesh0.n_elements(); ++i)
  {
    EXPECT_EQ(mesh0.element(i)->n_vertices(), mesh1.element(i)->n_vertices());
    for (int j = 0; j < mesh0.element(i)->n_vertices(); ++j)
      EXPECT_EQ(mesh0.element(i)->vertex_number(j), mesh1.element(i)->vertex_number(j));
  }
}


#endif // TEST_FEMPLUS_TRIANGULAR_MESH_HPP
