#ifndef TEST_FEMPLUS_RECTANGULAR_MESH_HPP
#define TEST_FEMPLUS_RECTANGULAR_MESH_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/material_function.hpp"
#include "femplus/mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/line.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/auxiliary_functions.hpp"

#include "compare_functions.hpp"

#include <fstream>

using namespace femplus;

// =======================================================
TEST(RectangularMesh, build1)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 1, 1));
  mesh->build();

  EXPECT_EQ(mesh->n_vertices(), 4);
  EXPECT_EQ(mesh->n_elements(), 1);
  EXPECT_EQ(mesh->n_boundary_elements(), 4);
}

// =======================================================
TEST(RectangularMesh, build2)
{
  MeshPtr mesh(new RectangularMesh(-1, 0, -1, 0, 1, 1));
  mesh->build();

  EXPECT_EQ(mesh->n_vertices(), 4);
  EXPECT_EQ(mesh->n_elements(), 1);
  EXPECT_EQ(mesh->n_boundary_elements(), 4);
}

// =======================================================
TEST(RectangularMesh, build3)
{
  MeshPtr mesh(new RectangularMesh(-1000, 0, -2000, 0, 1, 1));
  mesh->build();

  EXPECT_EQ(mesh->n_vertices(), 4);
  EXPECT_EQ(mesh->n_elements(), 1);
  EXPECT_EQ(mesh->n_boundary_elements(), 4);
}

// =======================================================
TEST(RectangularMesh, build4)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 2));
  mesh->build();

  EXPECT_EQ(mesh->n_vertices(), 9);
  EXPECT_EQ(mesh->n_elements(), 4);
  EXPECT_EQ(mesh->n_boundary_elements(), 8);
}

// =======================================================
TEST(RectangularMesh, build5)
{
  MeshPtr mesh(new RectangularMesh(0, 500, 0, 127, 25, 34));
  mesh->build();

  mesh->write_msh("rect_mesh_to_see.msh");
}

// =======================================================
TEST(RectangularMesh, write_msh)
{
  MeshPtr mesh(new RectangularMesh(0, 500, 0, 127, 25, 34));
  mesh->build();

  mesh->write_msh("rect_mesh_to_see.msh");
}

// =======================================================
TEST(RectangularMesh, write_geo)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 2));
  mesh->build();

  mesh->write_geo("rect_geo_to_see.geo", 1./2./5.);
}

// =======================================================
TEST(RectangularMesh, numerate_edges_1)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 1, 1));
  mesh->build();
  mesh->numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(0, 2));
  edges.push_back(new Line(1, 3));
  edges.push_back(new Line(2, 3));

  compare_edges(*mesh.get(), edges);

  std::vector<int> boundary_edges;
  mesh->boundary_edges_numbers(boundary_edges);
  const int expected_b_edges[] = { 0, 1, 2, 3 };
  EXPECT_EQ(boundary_edges.size(), 4);
  for (unsigned i = 0; i < boundary_edges.size(); ++i)
    EXPECT_EQ(boundary_edges[i], expected_b_edges[i]);
  std::vector<int>::const_iterator b_edge = boundary_edges.begin();
  std::vector<int>::const_iterator end = boundary_edges.end();
  for (; b_edge != end; ++b_edge) {
    const std::vector<int> &cell_num = mesh->cells_of_edge(*b_edge);
    EXPECT_EQ(cell_num.size(), 1);
    EXPECT_EQ(cell_num[0], 0);
  }

  std::vector<int> interior_edges;
  mesh->interior_edges_numbers(interior_edges);
  EXPECT_EQ(interior_edges.size(), 0); // there are no interior edges

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];
}

// =======================================================
TEST(RectangularMesh, numerate_edges_2)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 1));
  mesh->build();
  mesh->numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(1, 2));
  edges.push_back(new Line(0, 3));
  edges.push_back(new Line(1, 4));
  edges.push_back(new Line(3, 4));
  edges.push_back(new Line(2, 5));
  edges.push_back(new Line(4, 5));

  compare_edges(*mesh.get(), edges);

  std::vector<int> boundary_edges;
  mesh->boundary_edges_numbers(boundary_edges);
  const int expected_b_edges[] = { 0, 1, 2, 4, 5, 6 };
  EXPECT_EQ(boundary_edges.size(), 6);
  for (unsigned i = 0; i < boundary_edges.size(); ++i)
    EXPECT_EQ(boundary_edges[i], expected_b_edges[i]);
  const int expected_b_cells[] = { 0, 1, 0, 0, 1, 1 };
  std::vector<int>::const_iterator b_edge = boundary_edges.begin();
  std::vector<int>::const_iterator end = boundary_edges.end();
  for (int i = 0; b_edge != end; ++b_edge, ++i) {
    const std::vector<int> &cell_num = mesh->cells_of_edge(*b_edge);
    EXPECT_EQ(cell_num.size(), 1);
    EXPECT_EQ(cell_num[0], expected_b_cells[i]);
  }

  std::vector<int> interior_edges;
  mesh->interior_edges_numbers(interior_edges);
  const int expected_i_edges[] = { 3 };
  EXPECT_EQ(interior_edges.size(), 1);
  for (unsigned i = 0; i < interior_edges.size(); ++i)
    EXPECT_EQ(interior_edges[i], expected_i_edges[i]);
  const int expected_i_cells[][2] = { { 0, 1 } };
  std::vector<int>::const_iterator i_edge = interior_edges.begin();
  end = interior_edges.end();
  for (int i = 0; i_edge != end; ++i_edge, ++i) {
    const std::vector<int> &cell_num = mesh->cells_of_edge(*i_edge);
    EXPECT_EQ(cell_num.size(), 2);
    for (int j = 0; j < 2; ++j)
      EXPECT_EQ(cell_num[j], expected_i_cells[i][j]);
  }

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];
}

// =======================================================
TEST(RectangularMesh, numerate_edges_3)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 2));
  mesh->build();
  mesh->numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(1, 2));
  edges.push_back(new Line(0, 3));
  edges.push_back(new Line(1, 4));
  edges.push_back(new Line(3, 4));
  edges.push_back(new Line(2, 5));
  edges.push_back(new Line(4, 5));
  edges.push_back(new Line(3, 6));
  edges.push_back(new Line(4, 7));
  edges.push_back(new Line(6, 7));
  edges.push_back(new Line(5, 8));
  edges.push_back(new Line(7, 8));

  compare_edges(*mesh.get(), edges);

  std::vector<int> boundary_edges;
  mesh->boundary_edges_numbers(boundary_edges);
  const int expected_b_edges[] = { 0, 1, 2, 5, 7, 9, 10, 11 };
  EXPECT_EQ(boundary_edges.size(), 8);
  for (unsigned i = 0; i < boundary_edges.size(); ++i)
    EXPECT_EQ(boundary_edges[i], expected_b_edges[i]);
  const int expected_b_cells[] = { 0, 1, 0, 1, 2, 2, 3, 3 };
  std::vector<int>::const_iterator b_edge = boundary_edges.begin();
  std::vector<int>::const_iterator end = boundary_edges.end();
  for (int i = 0; b_edge != end; ++b_edge, ++i) {
    const std::vector<int> &cell_num = mesh->cells_of_edge(*b_edge);
    EXPECT_EQ(cell_num.size(), 1);
    EXPECT_EQ(cell_num[0], expected_b_cells[i]);
  }

  std::vector<int> interior_edges;
  mesh->interior_edges_numbers(interior_edges);
  const int expected_i_edges[] = { 3, 4, 6, 8 };
  EXPECT_EQ(interior_edges.size(), 4);
  for (unsigned i = 0; i < interior_edges.size(); ++i)
    EXPECT_EQ(interior_edges[i], expected_i_edges[i]);
  const int expected_i_cells[][2] = { { 0, 1 }, { 0, 2 }, { 1, 3 }, { 2, 3 } };
  std::vector<int>::const_iterator i_edge = interior_edges.begin();
  end = interior_edges.end();
  for (int i = 0; i_edge != end; ++i_edge, ++i) {
    const std::vector<int> &cell_num = mesh->cells_of_edge(*i_edge);
    EXPECT_EQ(cell_num.size(), 2);
    for (int j = 0; j < 2; ++j)
      EXPECT_EQ(cell_num[j], expected_i_cells[i][j]);
  }

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];
}

// =============================================================================
TEST(RectangularMesh, clone)
{
  Mesh *m1 = new RectangularMesh(0, 1, 0, 1, 2, 2);
  m1->build();
  m1->numerate_edges();

  Mesh *m2 = m1->clone();

  delete m1;

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(1, 2));
  edges.push_back(new Line(0, 3));
  edges.push_back(new Line(1, 4));
  edges.push_back(new Line(3, 4));
  edges.push_back(new Line(2, 5));
  edges.push_back(new Line(4, 5));
  edges.push_back(new Line(3, 6));
  edges.push_back(new Line(4, 7));
  edges.push_back(new Line(6, 7));
  edges.push_back(new Line(5, 8));
  edges.push_back(new Line(7, 8));

  compare_edges(*m2, edges);

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];
}

#if 1 // uncomment only when the femplus library is built in release mode
// =============================================================================
#if 0
TEST(RectangularMesh, build_from_another_mesh_10)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int nx    = 10;
  const int ny    = 10;

  std::string fname = TESTFILES_DIR + "/salt_dome_flat_0.msh";
  TriangularMesh t0(fname);
  RectangularMesh r0(x0, x1, y0, y1, nx, ny);
  r0.build(t0);
  std::string out_fname = "salt_dome_flat_rect_10_0.msh";
  r0.write_msh(out_fname);
  std::shared_ptr<Mesh> tri0 = r0.build_triangular_mesh();
  out_fname = "salt_dome_flat_tria_10_0.msh";
  tri0->write_msh(out_fname);

  fname = TESTFILES_DIR + "/salt_dome_flat_1.msh";
  TriangularMesh t1(fname);
  RectangularMesh r1(x0, x1, y0, y1, nx, ny);
  r1.build(t1);
  out_fname = "salt_dome_flat_rect_10_1.msh";
  r1.write_msh(out_fname);
  std::shared_ptr<Mesh> tri1 = r1.build_triangular_mesh();
  out_fname = "salt_dome_flat_tria_10_1.msh";
  tri1->write_msh(out_fname);
}
#endif
// =============================================================================
#if 0
TEST(RectangularMesh, build_from_another_mesh_100)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;

  std::string fname = TESTFILES_DIR + "/salt_dome_flat_F_UT.msh";
  TriangularMesh t0(fname);

  std::cout << "F_UT: n_cells = " << t0.n_elements()
            << " n_dofs = " << t0.n_vertices() << std::endl;

//  const int n_cells = t0.n_elements();
//  const int nx = int(int(sqrt(n_cells)) / 10) * 10;
//  const int ny = nx;
  const int nx = 300;
  const int ny = 300;

  std::cout << "F_R: nx = " << nx << " hx = " << (x1-x0)/nx << " n_cells = "
            << nx*ny << " n_dofs = " << (nx+1)*(ny+1) << std::endl;

  RectangularMesh r0(x0, x1, y0, y1, nx, ny);
  r0.build(t0);
  std::string out_fname = "salt_dome_flat_F_R_" + d2s<int>(nx) + ".msh";
  r0.write_msh(out_fname);
}
#endif
//==============================================================================
#if 0
TEST(RectangularMesh, build_from_another_mesh_with_another_way_0)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int nx    = 10;
  const int ny    = 10;

  std::string fname = TESTFILES_DIR + "/salt_dome_flat_0.msh";
  TriangularMesh t0(fname);

  RectangularMesh r0(x0, x1, y0, y1, nx, ny);
  r0.build(t0);

  RectangularMesh r1(x0, x1, y0, y1, nx, ny);
  r1.build_fast(t0);

  std::string out_fname_0 = "salt_dome_flat_rect_10_0.msh";
  std::string out_fname_1 = "salt_dome_flat_rect_10_1.msh";
  r0.write_msh(out_fname_0);
  r1.write_msh(out_fname_1);

  EXPECT_EQ(r0.n_elements(), r1.n_elements());
  EXPECT_EQ(r0.n_elements_x(), r1.n_elements_x());
  EXPECT_EQ(r0.n_elements_y(), r1.n_elements_y());
  EXPECT_EQ(r0.n_boundary_elements(), r1.n_boundary_elements());
  EXPECT_EQ(r0.n_vertices(), r1.n_vertices());
  for (int i = 0; i < r0.n_elements(); ++i)
    EXPECT_EQ(r0.element(i)->material_id(), r1.element(i)->material_id());
  for (int i = 0; i < r0.n_boundary_elements(); ++i)
    EXPECT_EQ(r0.boundary_element(i)->material_id(),
              r1.boundary_element(i)->material_id());
}
#endif
//==============================================================================
#if 1 // it's a very time consuming test, and it doesn't give any insights
class CoefALayers: public femplus::MaterialFunction
{
public:
  CoefALayers(const std::string &filename)
  {
    std::ifstream in(filename.c_str());
    require(in, "File " + filename + " can't be opened");
    int layer;
    double rho, vp;
    std::string tmp;
    while (in >> layer)
    {
      in >> rho >> vp; getline(in, tmp);
      _values.insert(std::pair<int, Vector>(layer, Vector(1, vp*vp))); // a = vp^2
    }
    in.close();
  }
};
class VpLayers: public femplus::MaterialFunction
{
public:
  VpLayers(const std::string &filename)
  {
    std::ifstream in(filename.c_str());
    require(in, "File " + filename + " can't be opened");
    int layer;
    double rho, vp;
    std::string tmp;
    while (in >> layer)
    {
      in >> rho >> vp; getline(in, tmp);
      _values.insert(std::pair<int, Vector>(layer, Vector(1, vp)));
    }
    in.close();
  }
};
TEST(RectangularMesh, build_from_another_mesh_with_another_way_1)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const double hx = 4;
  const double hy = 2;
  const int nx    = (x1 - x0) / hx;
  const int ny    = (y1 - y0) / hy;

  //std::string fname = "salt_dome_flat_F_UT_superbig.msh";//TESTFILES_DIR + "/salt_dome_flat_1.msh";
  std::string fname = "/u/artemyev/salt_dome_flat_h1.msh"; //TESTFILES_DIR + "karst.msh"; //"salt_dome_flat_1.msh";
  TriangularMesh t0(fname);

  double time;

//  time = get_wall_time();
//  RectangularMesh r0(x0, x1, y0, y1, nx, ny);
//  r0.build(t0);
//  std::cout << "build() time = " << get_wall_time() - time << std::endl;
//  std::string out_fname_0 = "salt_dome_flat_rect_100_0.msh";
//  r0.write_msh(out_fname_0);

  time = get_wall_time();
  RectangularMesh r1(x0, x1, y0, y1, nx, ny);
  const bool show_info = true;
  r1.build_fast(t0, show_info);
  std::cout << "build_fast() time = " << get_wall_time() - time << std::endl;
  std::string out_fname_1 = "salt_dome_flat_hx4_hy2_R.msh"; //TESTOUT_DIR + "karst_gridR.msh";
  r1.write_msh(out_fname_1);


  const std::string earth_properties = TESTFILES_DIR + "salt_dome_properties.dat";
  VpLayers coef(earth_properties);


  const std::string coef_a_grid = "salt_dome_hx4_hy2.bin";
  std::ofstream out(coef_a_grid.c_str(), std::ios::binary);
  require(out, "File " + coef_a_grid + " can't be opened");

//  out.setf(std::ios::scientific);
//  out.precision(16);

//  out << r1.n_elements() << "\n";
//  const double rho = 1.;
  for (int el = 0; el < r1.n_elements(); ++el)
  {
    coef.set_attributes(*r1.element(el));
    float c = coef.value(Point());

    out.write(reinterpret_cast<char*>(&c), sizeof(c));
//    out << rho << " " << c << "\n";
  }
  out.close();

//  EXPECT_EQ(r0.n_elements(), r1.n_elements());
//  EXPECT_EQ(r0.n_elements_x(), r1.n_elements_x());
//  EXPECT_EQ(r0.n_elements_y(), r1.n_elements_y());
//  EXPECT_EQ(r0.n_boundary_elements(), r1.n_boundary_elements());
//  EXPECT_EQ(r0.n_vertices(), r1.n_vertices());
//  for (int i = 0; i < r0.n_elements(); ++i)
//    EXPECT_EQ(r0.element(i)->material_id(), r1.element(i)->material_id());
//  for (int i = 0; i < r0.n_boundary_elements(); ++i)
//    EXPECT_EQ(r0.boundary_element(i)->material_id(),
//              r1.boundary_element(i)->material_id());
}
#endif

#endif // uncomment only when the femplus library is built in release mode



#endif // TEST_FEMPLUS_RECTANGULAR_MESH_HPP
