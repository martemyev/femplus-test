#ifndef TEST_FEMPLUS_PETSC_MATRIX_HPP
#define TEST_FEMPLUS_PETSC_MATRIX_HPP

#include "config.hpp"
#include <cstdio>
#include <cmath>
#include <gtest/gtest.h>
#include <fstream>

#include "femplus/petsc_matrix.hpp"
#include "femplus/dense_pattern.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/cg_dof_handler.hpp"
#include "femplus/cg_interior_dof_handler.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/coarse_finite_element.hpp"
#include "femplus/eigensolver.hpp"

#include "slepceps.h"

//#define SHOW_EIGS_

using namespace femplus;

// =============================================================================
TEST(PetscMatrix, RARt)
{
  DensePattern patternA(3, 3);
  DensePattern patternR(5, 3);
  DensePattern patternC(5, 5);

  PetscMatrixPtr A(new PetscMatrix(patternA));
  PetscMatrixPtr R(new PetscMatrix(patternR));
  PetscMatrixPtr C(new PetscMatrix(patternC));

  A->add_value(0, 0, 1);
  A->add_value(0, 1, 2);
  A->add_value(0, 2, 3);
  A->add_value(1, 0, 4);
  A->add_value(1, 1, 5);
  A->add_value(1, 2, 6);
  A->add_value(2, 0, 7);
  A->add_value(2, 1, 8);
  A->add_value(2, 2, 9);
  A->final_assembly();

  R->add_value(0, 0, 5);
  R->add_value(0, 1, 1);
  R->add_value(0, 2, 5);
  R->add_value(1, 0, 45);
  R->add_value(1, 1, 7);
  R->add_value(1, 2, 1234);
  R->add_value(2, 0, 12);
  R->add_value(2, 1, 567);
  R->add_value(2, 2, 78);
  R->add_value(3, 0, 234);
  R->add_value(3, 1, 6);
  R->add_value(3, 2, 9);
  R->add_value(4, 0, -4);
  R->add_value(4, 1, 2);
  R->add_value(4, 2, 467);
  R->final_assembly();

  const double checkC[][5] = { {605, 83809, 36861, 11220, 30756},
                               {109967, 14385196, 6652905, 2199903, 5254311},
                               {38313, 5260311, 2331693, 719442, 1929042},
                               {6270, 1029081, 390924, 85905, 382329},
                               {41118, 5359953, 2486556, 826137, 1957185} };

  C->RARt(A, R);

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
      EXPECT_DOUBLE_EQ((*C)(i, j), checkC[i][j]);
}

// =============================================================================
TEST(PetscMatrix, ABCt)
{
  DensePattern patternA(2, 3);
  DensePattern patternB(3, 4);
  DensePattern patternC(1, 4);
  DensePattern patternD(2, 1);

  PetscMatrixPtr A(new PetscMatrix(patternA));
  PetscMatrixPtr B(new PetscMatrix(patternB));
  PetscMatrixPtr C(new PetscMatrix(patternC));
  PetscMatrixPtr D(new PetscMatrix(patternD));

  A->add_value(0, 0, 1);
  A->add_value(0, 1, 2);
  A->add_value(0, 2, 3);
  A->add_value(1, 0, 5);
  A->add_value(1, 1, 1);
  A->add_value(1, 2, -7);
  A->final_assembly();

  B->add_value(0, 0, 6);
  B->add_value(0, 1, 7);
  B->add_value(0, 2, 88);
  B->add_value(0, 3, 9);
  B->add_value(1, 0, 1);
  B->add_value(1, 1, 4);
  B->add_value(1, 2, 6);
  B->add_value(1, 3, 7);
  B->add_value(2, 0, 5);
  B->add_value(2, 1, 2);
  B->add_value(2, 2, 5);
  B->add_value(2, 3, 7);
  B->final_assembly();

  C->add_value(0, 0, 6);
  C->add_value(0, 1, 25);
  C->add_value(0, 2, 456);
  C->add_value(0, 3, 2);
  C->final_assembly();

  D->ABCt(A, B, C);

  const double checkD[][1] = { { 53191 },
                               { 188023 } };

  for (int i = 0; i < 2; ++i)
    for (int j = 0; j < 1; ++j)
      EXPECT_DOUBLE_EQ((*D)(i, j), checkD[i][j]);
}

// =============================================================================
TEST(PetscMatrix, save_and_load)
{
  const int n = 2;
  const int m = 3;
  DensePattern patternA(n, m);
  PetscMatrixPtr A(new PetscMatrix(patternA));
  A->add_value(0, 0, 1);
  A->add_value(0, 1, 2);
  A->add_value(0, 2, 3);
  A->add_value(1, 0, 5);
  A->add_value(1, 1, 1);
  A->add_value(1, 2, -7);
  A->final_assembly();

  const double checkA[][m] = { { 1, 2, 3 },
                               { 5, 1, -7 } };
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      EXPECT_DOUBLE_EQ((*A)(i, j), checkA[i][j]);

  const std::string fname = "matA.dat";
  A->save(fname);

  PetscMatrix B(patternA);
  B.load(fname);
  B.final_assembly();
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      EXPECT_DOUBLE_EQ(B(i, j), checkA[i][j]);
}
// =============================================================================
TEST(PetscMatrix, sym_csr_arrays)
{
  const int N = 5;
  const double mat[][N] = { { 4, 5, -1, 7, 8 },
                            { 5, 1, 3, 6, 0 },
                            { -1, 3, 4, 5, 1 },
                            { 7, 6, 5, 8, 5 },
                            { 8, 0, 1, 5, 2 } };

  DensePattern patternA(N, N);
  PetscMatrix A(patternA);
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      A.add_value(i, j, mat[i][j]);
  A.final_assembly();

  std::vector<int> IA, JA;
  std::vector<double> VA;

  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  A.sym_csr_arrays(patternA, IA, JA, VA, 1);

  const int ia[] = { 1, 2, 4, 7, 11, 16 };
  const int ja[] = { 1, 1, 2, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4, 5 };
  const double va[] = { 4, 5, 1, -1, 3, 4, 7, 6, 5, 8, 8, 0, 1, 5, 2 };

  EXPECT_EQ(IA.size(), 6);
  for (int i = 0; i < 6; ++i)
    EXPECT_EQ(IA[i], ia[i]);

  EXPECT_EQ(JA.size(), 15);
  for (int i = 0; i < 15; ++i)
    EXPECT_EQ(JA[i], ja[i]);

  EXPECT_EQ(VA.size(), 15);
  for (int i = 0; i < 15; ++i)
    EXPECT_DOUBLE_EQ(VA[i], va[i]);
}

// =============================================================================
//
// Testing eigensolvers
//
// =============================================================================
TEST(PetscMatrix, eigensolver_0)
{
  const int N = 5;

  const double mat[][N] = { { 1.1483, 1.0014, 0.9944, 1.6389, 1.3167 },
                            { 1.0014, 2.0192, 1.6129, 1.9326, 2.0472 },
                            { 0.9944, 1.6129, 2.7910, 2.6227, 2.8290 },
                            { 1.6389, 1.9326, 2.6227, 3.4866, 3.2840 },
                            { 1.3167, 2.0472, 2.8290, 3.2840, 3.3526 } };

  DensePattern patternA(N, N);
  PetscMatrixPtr A(new PetscMatrix(patternA));
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      A->add_value(i, j, mat[i][j]);
  A->final_assembly();

  // eigenvalues computed by Matlab are considered as reference values
  const double eigs_matlab[] = { 0.0004, 0.2340, 0.6986, 0.8894, 10.9753 };

  // compute eigenvalues with Lapack through SLEPc
  std::vector<double> eigenvalues_real(N);
  std::vector<double> eigenvalues_imag(N);
  std::vector<Vector> eigenvector_real(N);
  std::vector<Vector> eigenvector_imag(N);

  double t0 = get_wall_time();
  const int nconv = eigensolver_SLEPc_LAPACK_HEP(*A.get(),
                                            eigenvalues_real, eigenvalues_imag,
                                            eigenvector_real, eigenvector_imag);
  double t1 = get_wall_time();
  const double eigs_lapack[] = { 0.00033935202488577753, 0.23399405899323705,
                                 0.69854452537793188, 0.88947442696699308,
                                 10.975347636636952 };

  EXPECT_EQ(nconv, N);

#if defined(SHOW_EIGS_)
  std::cout << "\nLAPACK (SLEPc)\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  double tol = 1e-12;
  for (int i = 0; i < N; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_matlab[i], eigenvalues_real[i],
                                    fabs((eigs_matlab[i] - eigenvalues_real[i]) / eigs_matlab[i]));
#endif
    EXPECT_NEAR(eigenvalues_real[i], eigs_lapack[i], tol);
    EXPECT_DOUBLE_EQ(eigenvalues_imag[i], 0.);
  }

  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<int> IA, JA;
  std::vector<double> VA, EIGS;
  std::vector<Vector> EIGV;
  A->sym_csr_arrays(patternA, IA, JA, VA, 1);

  // compute eigenvalues with JADAMILU
  t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&IA[0], &JA[0], &VA[0], N, N, EIGS, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS.size(), N);
#if defined(SHOW_EIGS_)
  std::cout << "\nJADAMILU\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif

  tol = 8e-5;
  for (int i = 0; i < N; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_matlab[i], EIGS[i],
                                    fabs((eigs_matlab[i] - EIGS[i]) / eigs_matlab[i]));
#endif
    EXPECT_NEAR(EIGS[i], eigs_matlab[i], tol);
  }
}
// -----------------------------------------------------------------------------
TEST(PetscMatrix, eigensolver_1)
{
  const double x0 = 5;
  const double x1 = 10;
  const double y0 = -8;
  const double y1 = 17;
  const int    nx = 3;
  const int    ny = 4;
  const int    N = (nx+1)*(ny+1);
  const int  inN = (nx-1)*(ny-1);
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);
  CSRPattern pattern(connections);
  PetscMatrix mass(pattern);
  dof_handler.build_mass_matrix(ConstantFunction(1.), mass);
  mass.final_assembly();

  CGInteriorDoFHandler in_dof_handler(fe_mesh);
  in_dof_handler.distribute_dofs();
  connections.clear();
  in_dof_handler.dofs_connections(connections);
  CSRPattern in_pattern(connections);
  PetscMatrix in_mass(in_pattern);
  PetscMatrix in_stif(in_pattern);
  in_dof_handler.build_mass_matrix(ConstantFunction(1.), in_mass);
  in_dof_handler.build_stiffness_matrix(ConstantFunction(1.), in_stif);
  in_mass.final_assembly();
  in_stif.final_assembly();

#if 0 // save matrices and compute Matlab version OR compare the results
  // represent mass and stiffness matrices in matlab view to compare eigenvalues
  // computed in Matlab with others
  std::ofstream out("mass_eigen1.dat");
  require(out, "File mass_eigen1.dat can't be opened");
  mass.matlab_view(pattern, out);
  out.close();

  out.open("in_mass_eigen1.dat");
  require(out, "File in_mass_eigen1.dat can't be opened");
  in_mass.matlab_view(pattern, out);
  out.close();

  out.open("in_stif_eigen1.dat");
  require(out, "File in_stif_eigen1.dat can't be opened");
  in_stif.matlab_view(pattern, out);
  out.close();
#else
  // eigenvalues computed by Matlab
  const double eigs_M[] = { 0.5719,0.6324,0.7053,0.7790,1.1942,1.4692,1.5016,
                            1.6564,1.7606,2.1638,2.2034,2.2200,2.4292,2.7270,
                            3.1386,4.6040,4.6216,5.8247,6.7770,8.5400 };
  const double eigs_inM[] = { 3.2478,3.4700,3.6565,5.6035,5.7900,6.0122 };
  const double eigs_inS[] = { 2.4310,2.9500,3.3771,7.3429,7.7700,8.2890 };
  const double eigs_GEP[] = { 0.4043,0.5095,0.6027,2.0081,2.2392,2.5521 };

  // compute eigenvalues with Lapack through SLEPc
  // mass matrix
  std::vector<double> eigenvalues_real_M(N);
  std::vector<double> eigenvalues_imag_M(N);
  std::vector<Vector> eigenvector_real_M(N);
  std::vector<Vector> eigenvector_imag_M(N);
  double t0 = get_wall_time();
  int nconv = eigensolver_SLEPc_LAPACK_HEP(mass,
                                        eigenvalues_real_M, eigenvalues_imag_M,
                                        eigenvector_real_M, eigenvector_imag_M);
  double t1 = get_wall_time();
  EXPECT_EQ(nconv, N);
#if defined(SHOW_EIGS_)
  std::cout << "\nLAPACK (SLEPc)\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  double tol = 6e-3;
  for (int i = 0; i < N; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_M[i], eigenvalues_real_M[i],
                                    fabs((eigs_M[i] - eigenvalues_real_M[i]) / eigs_M[i]));
#endif
    EXPECT_NEAR(fabs((eigs_M[i] - eigenvalues_real_M[i]) / eigs_M[i]), 0., tol);
    EXPECT_DOUBLE_EQ(eigenvalues_imag_M[i], 0.);
  }

  // interior mass matrix
  std::vector<double> eigenvalues_real_inM(inN);
  std::vector<double> eigenvalues_imag_inM(inN);
  std::vector<Vector> eigenvector_real_inM(inN);
  std::vector<Vector> eigenvector_imag_inM(inN);
  t0 = get_wall_time();
  nconv = eigensolver_SLEPc_LAPACK_HEP(in_mass,
                                       eigenvalues_real_inM, eigenvalues_imag_inM,
                                       eigenvector_real_inM, eigenvector_imag_inM);
  t1 = get_wall_time();
  EXPECT_EQ(nconv, inN);
#if defined(SHOW_EIGS_)
  std::cout << "inMass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  tol = 3.1e-1; // the  values are not very much equal
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_inM[i], eigenvalues_real_inM[i],
                                    fabs((eigs_inM[i] - eigenvalues_real_inM[i]) / eigs_inM[i]));
#endif
    EXPECT_NEAR(fabs((eigs_inM[i] - eigenvalues_real_inM[i]) / eigs_inM[i]), 0., tol);
    EXPECT_DOUBLE_EQ(eigenvalues_imag_inM[i], 0.);
  }

  // interior stiffness matrix
  std::vector<double> eigenvalues_real_inS(inN);
  std::vector<double> eigenvalues_imag_inS(inN);
  std::vector<Vector> eigenvector_real_inS(inN);
  std::vector<Vector> eigenvector_imag_inS(inN);
  t0 = get_wall_time();
  nconv = eigensolver_SLEPc_LAPACK_HEP(in_stif,
                                       eigenvalues_real_inS, eigenvalues_imag_inS,
                                       eigenvector_real_inS, eigenvector_imag_inS);
  t1 = get_wall_time();
  EXPECT_EQ(nconv, inN);
#if defined(SHOW_EIGS_)
  std::cout << "inStif matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  tol = 2.8e-1; // not very much equal as well
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_inS[i], eigenvalues_real_inS[i],
                                    fabs((eigs_inS[i] - eigenvalues_real_inS[i]) / eigs_inS[i]));
#endif
    EXPECT_NEAR(fabs((eigs_inS[i] - eigenvalues_real_inS[i]) / eigs_inS[i]), 0., tol);
    EXPECT_DOUBLE_EQ(eigenvalues_imag_inS[i], 0.);
  }

  // generalized eigenproblem
  std::vector<double> eigenvalues_real_GEP(inN);
  std::vector<double> eigenvalues_imag_GEP(inN);
  std::vector<Vector> eigenvector_real_GEP(inN);
  std::vector<Vector> eigenvector_imag_GEP(inN);
  t0 = get_wall_time();
  nconv = eigensolver_SLEPc_LAPACK_GHEP(in_stif, in_mass,
                                   eigenvalues_real_GEP, eigenvalues_imag_GEP,
                                   eigenvector_real_GEP, eigenvector_imag_GEP);
  t1 = get_wall_time();
  EXPECT_EQ(nconv, inN);
#if defined(SHOW_EIGS_)
  std::cout << "Generalized eigenproblem\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  tol = 1.1e-1; // big for the first value, less for others
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_GEP[i], eigenvalues_real_GEP[i],
                                    fabs((eigs_GEP[i] - eigenvalues_real_GEP[i]) / eigs_GEP[i]));
#endif
    EXPECT_NEAR(fabs((eigs_GEP[i] - eigenvalues_real_GEP[i]) / eigs_GEP[i]), 0., tol);
    EXPECT_DOUBLE_EQ(eigenvalues_imag_GEP[i], 0.);
  }

  // compute eigenvalues with JADAMILU
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<int> IA, JA;
  std::vector<double> VA, EIGS_M;
  std::vector<Vector> EIGV;
  mass.sym_csr_arrays(pattern, IA, JA, VA, 1);
  t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&IA[0], &JA[0], &VA[0], N, N, EIGS_M, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_M.size(), N);
#if defined(SHOW_EIGS_)
  std::cout << "\nJADAMILU\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  tol = 1e-12;
  for (int i = 0; i < N; ++i)
  {
    EXPECT_NEAR(EIGS_M[i], eigenvalues_real_M[i], tol);
//    printf("%2.6e  %2.6e  %2.6e\n", eigs_M[i], EIGS_M[i],
//                                    fabs((eigs_M[i] - EIGS_M[i]) / eigs_M[i]));
  }

  std::vector<int> inMIA, inMJA;
  std::vector<double> inMVA, EIGS_inM;
  in_mass.sym_csr_arrays(pattern, inMIA, inMJA, inMVA, 1);
  t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&inMIA[0], &inMJA[0], &inMVA[0], inN, inN, EIGS_inM, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_inM.size(), inN);
#if defined(SHOW_EIGS_)
  std::cout << "inMass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
  printf("%12s  %12s  %12s\n", "Matlab", "LAPACK", "JADAMILU");
#endif
  // these values are quite far from the ones computed with Matlab. To see how
  // far they are, uncomment #define SHOW_EIGS_ in the beginning of this file
  const double eigenvalues_real_inM_lapack[] = { 2.2446062826622435, 3.4722222222222214,
                                                 3.7410104711037411, 4.6998381617822007,
                                                 5.7870370370370363, 7.8330636029703324 };
  // these values are much closer to the ones computed with Matlab
  const double EIGS_inM_jada[] = { 3.2496741945878105, 3.4722222222222214,
                                   3.6588795077744702, 5.6003797514847884,
                                   5.7870370370370372, 6.0095850646714508 };
  tol = 1e-14;
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_inM[i], eigenvalues_real_inM[i], EIGS_inM[i]);
#endif
    EXPECT_DOUBLE_EQ(eigenvalues_real_inM[i], eigenvalues_real_inM_lapack[i]);
    EXPECT_DOUBLE_EQ(EIGS_inM[i], EIGS_inM_jada[i]);
  }

  std::vector<int> inSIA, inSJA;
  std::vector<double> inSVA, EIGS_inS;
  in_stif.sym_csr_arrays(pattern, inSIA, inSJA, inSVA, 1);
  t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&inSIA[0], &inSJA[0], &inSVA[0], inN, inN, EIGS_inS, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_inS.size(), inN);
#if defined(SHOW_EIGS_)
  std::cout << "inStif matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
  printf("%12s  %12s  %12s\n", "Matlab", "LAPACK", "JADAMILU");
#endif
  // these values are quite far from the ones computed with Matlab. To see how
  // far they are, uncomment #define SHOW_EIGS_ in the beginning of this file
  const double eigenvalues_real_inS_lapack[] = { 2.3748306484886132, 2.9444444444444446,
                                                 3.5140582404002734, 5.303578045533528,
                                                 7.7666666666666657, 10.22975528779981 };
  // these values are much closer to the ones computed with Matlab
  const double EIGS_inS_jada[] = { 2.425047308611934, 2.9444444444444446,
                                   3.3717847212005898, 7.3393263899105232,
                                   7.7666666666666675, 8.2860638024991786 };
  tol = 1e-14;
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_inS[i], eigenvalues_real_inS[i], EIGS_inS[i]);
#endif
    EXPECT_NEAR(eigenvalues_real_inS[i], eigenvalues_real_inS_lapack[i], tol);
    EXPECT_DOUBLE_EQ(EIGS_inS[i], EIGS_inS_jada[i]);
  }

  std::vector<double> EIGS_GEP;
  t0 = get_wall_time();
  in_stif.sym_csr_arrays(pattern, inSIA, inSJA, inSVA, 1);
  in_mass.sym_csr_arrays(pattern, inMIA, inMJA, inMVA, 1);
  eigensolver_JADAMILU_GHEP(&inSIA[0], &inSJA[0], &inSVA[0],
                            &inMIA[0], &inMJA[0], &inMVA[0],
                            inN, inN, EIGS_GEP, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_GEP.size(), inN);
#if defined(SHOW_EIGS_)
  std::cout << "Generalized eigenproblem\n";
  std::cout << "time = " << t1 - t0 << " s\n";
  printf("%12s  %12s  %12s\n", "Matlab", "LAPACK", "JADAMILU");
#endif
  // these values in contrary to the previous two cases are closer to the values
  // computed with Matlab than the values computed with JADAMILU. To see how
  // closer they are, uncomment the line #define SHOW_EIGS_ in the beginning of
  // this file
  const double eigenvalues_real_GEP_lapack[] = { 4.486186e-01, 5.088000e-01,
                                                 6.348099e-01, 2.176619e+00,
                                                 2.236800e+00, 2.362810e+00 };
  const double EIGS_GEP_jada[] = { 2.418599e-01, 3.089446e-01, 3.790672e-01,
                                   7.739276e-01, 9.794053e-01, 1.309664e+00 };
  tol = 1e-5;
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_GEP[i], eigenvalues_real_GEP[i], EIGS_GEP[i]);
#endif
    EXPECT_NEAR(fabs((eigenvalues_real_GEP[i] - eigenvalues_real_GEP_lapack[i])/
                     eigenvalues_real_GEP_lapack[i]), 0., tol);
    EXPECT_NEAR(fabs((EIGS_GEP[i] - EIGS_GEP_jada[i])/EIGS_GEP_jada[i]), 0., tol);
  }

  // the eigen vectors computed with LAPACK and JADAMILU have nothing in common,
  // therefore it doesn't make sense to compare them. The vectors can be printed
  // however to see their values. To do that, uncomment #define SHOW_EIGS_
#if defined(SHOW_EIGS_)
  printf("\nEigenvectors\n");
  for (int i = 0; i < inN; ++i)
  {
    printf("LAPACK: ");
    for (int j = 0; j < inN; ++j)
      printf("%2.6e  ", eigenvector_real_GEP[i](j));
    printf("\nJADAMI: ");
    for (int j = 0; j < inN; ++j)
      printf("%2.6e  ", EIGV[i](j));
    printf("\n---\n");
  }
#endif

#endif
}
// -----------------------------------------------------------------------------
// This test makes sense only to check, that solvers don't crush, and if the
// SHOW_EIGS_ key is ON to visually compare the eigevalues and eigenvectors.
// Other than that, this test is pointless. It doesn't compare computed values,
// so it can't be considered as a regression test. From the unit testing
// viewpoint this test does the same job as the previous one.
// -----------------------------------------------------------------------------
TEST(PetscMatrix, eigensolver_2)
{
  const double x0 = 5;
  const double x1 = 10;
  const double y0 = -8;
  const double y1 = 17;
  const int    nx = 3;
  const int    ny = 4;
  const int    N = 2*(nx+1)*(ny+1);
  const int  inN = 2*(nx-1)*(ny-1);
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2; // vector FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);
  CSRPattern pattern(connections);
  PetscMatrix mass(pattern);
  dof_handler.build_mass_matrix(ConstantFunction(1.), mass);
  mass.final_assembly();

  CGInteriorDoFHandler in_dof_handler(fe_mesh);
  in_dof_handler.distribute_dofs();
  connections.clear();
  in_dof_handler.dofs_connections(connections);
  CSRPattern in_pattern(connections);
  PetscMatrix in_mass(in_pattern);
  PetscMatrix in_stif(in_pattern);
  in_dof_handler.build_mass_matrix(ConstantFunction(1.), in_mass);
  Vector coef(6);
  coef(0) = 1.; coef(1) = -1.; coef(2) = 0.;
  coef(3) = 1.; coef(4) =  0.; coef(5) = 1.;
  in_dof_handler.build_stiffness_matrix(ConstantFunction(coef), in_stif);
  in_mass.final_assembly();
  in_stif.final_assembly();

#if 0 // save matrices and compute Matlab version OR compare the results
  // represent mass and stiffness matrices in matlab view to compare eigenvalues
  // computed in Matlab with others
  std::ofstream out("mass_eigen2.dat");
  require(out, "File mass_eigen2.dat can't be opened");
  mass.matlab_view(pattern, out);
  out.close();

  out.open("in_mass_eigen2.dat");
  require(out, "File in_mass_eigen2.dat can't be opened");
  in_mass.matlab_view(pattern, out);
  out.close();

  out.open("in_stif_eigen2.dat");
  require(out, "File in_stif_eigen2.dat can't be opened");
  in_stif.matlab_view(pattern, out);
  out.close();
#else
  // eigenvalues computed by Matlab
  const double eigs_M[] = { 0.5719,0.5719,0.6324,0.6324,0.7053,0.7053,0.7790,
                            0.7790,1.1942,1.1942,1.4692,1.4692,1.5016,1.5016,
                            1.6564,1.6564,1.7606,1.7606,2.1638,2.1638,2.2034,
                            2.2034,2.2200,2.2200,2.4292,2.4292,2.7270,2.7270,
                            3.1386,3.1386,4.6040,4.6040,4.6216,4.6216,5.8247,
                            5.8247,6.7770,6.7770,8.5400,8.5400 };
  const double eigs_inM[] = { 3.2478,3.2478,3.4700,3.4700,3.6565,3.6565,5.6035,
                              5.6035,5.7900,5.7900,6.0122,6.0122 };
  const double eigs_inS[] = { 2.4310,2.4310,2.9500,2.9500,3.3771,3.3771,7.3429,
                              7.3429,7.7700,7.7700,8.2890,8.2890 };
  const double eigs_GEP[] = { 0.4043,0.4043,0.5095,0.5095,0.6027,0.6027,2.0081,
                              2.0081,2.2392,2.2392,2.5521,2.5521 };

  // compute eigenvalues with Lapack through SLEPc
  // mass matrix
  std::vector<double> eigenvalues_real_M(N);
  std::vector<double> eigenvalues_imag_M(N);
  std::vector<Vector> eigenvector_real_M(N);
  std::vector<Vector> eigenvector_imag_M(N);
  double t0 = get_wall_time();
  int nconv = eigensolver_SLEPc_LAPACK_HEP(mass,
                                        eigenvalues_real_M, eigenvalues_imag_M,
                                        eigenvector_real_M, eigenvector_imag_M);
  double t1 = get_wall_time();
  EXPECT_EQ(nconv, N);
#if defined(SHOW_EIGS_)
  std::cout << "\nLAPACK (SLEPc)\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  double tol = 5.8e-3;
  for (int i = 0; i < N; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_M[i], eigenvalues_real_M[i],
                                    fabs((eigs_M[i] - eigenvalues_real_M[i]) / eigs_M[i]));
#endif
    EXPECT_NEAR(fabs((eigs_M[i] - eigenvalues_real_M[i]) / eigs_M[i]), 0., tol);
    EXPECT_DOUBLE_EQ(eigenvalues_imag_M[i], 0.);
  }

  // interior mass matrix
  std::vector<double> eigenvalues_real_inM(inN);
  std::vector<double> eigenvalues_imag_inM(inN);
  std::vector<Vector> eigenvector_real_inM(inN);
  std::vector<Vector> eigenvector_imag_inM(inN);
  t0 = get_wall_time();
  nconv = eigensolver_SLEPc_LAPACK_HEP(in_mass,
                                       eigenvalues_real_inM, eigenvalues_imag_inM,
                                       eigenvector_real_inM, eigenvector_imag_inM);
  t1 = get_wall_time();
  EXPECT_EQ(nconv, inN);
#if defined(SHOW_EIGS_)
  std::cout << "inMass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  tol = 3.1e-1;
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_inM[i], eigenvalues_real_inM[i],
                                    fabs((eigs_inM[i] - eigenvalues_real_inM[i]) / eigs_inM[i]));
#endif
    EXPECT_NEAR(fabs((eigs_inM[i] - eigenvalues_real_inM[i]) / eigs_inM[i]), 0., tol);
    EXPECT_DOUBLE_EQ(eigenvalues_imag_inM[i], 0.);
  }

  // interior stiffness matrix
  std::vector<double> eigenvalues_real_inS(inN);
  std::vector<double> eigenvalues_imag_inS(inN);
  std::vector<Vector> eigenvector_real_inS(inN);
  std::vector<Vector> eigenvector_imag_inS(inN);
  t0 = get_wall_time();
  nconv = eigensolver_SLEPc_LAPACK_HEP(in_stif,
                                       eigenvalues_real_inS, eigenvalues_imag_inS,
                                       eigenvector_real_inS, eigenvector_imag_inS);
  t1 = get_wall_time();
  EXPECT_EQ(nconv, inN);
#if defined(SHOW_EIGS_)
  std::cout << "inStif matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  tol = 2.8e-1;
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_inS[i], eigenvalues_real_inS[i],
                                    fabs((eigs_inS[i] - eigenvalues_real_inS[i]) / eigs_inS[i]));
#endif
    EXPECT_NEAR(fabs((eigs_inS[i] - eigenvalues_real_inS[i]) / eigs_inS[i]), 0., tol);
    EXPECT_DOUBLE_EQ(eigenvalues_imag_inS[i], 0.);
  }

  // generalized eigenproblem
  std::vector<double> eigenvalues_real_GEP(inN);
  std::vector<double> eigenvalues_imag_GEP(inN);
  std::vector<Vector> eigenvector_real_GEP(inN);
  std::vector<Vector> eigenvector_imag_GEP(inN);
  t0 = get_wall_time();
  nconv = eigensolver_SLEPc_LAPACK_GHEP(in_stif, in_mass,
                                   eigenvalues_real_GEP, eigenvalues_imag_GEP,
                                   eigenvector_real_GEP, eigenvector_imag_GEP);
  t1 = get_wall_time();
  EXPECT_EQ(nconv, inN);
#if defined(SHOW_EIGS_)
  std::cout << "Generalized eigenproblem\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  tol = 1.1e-1;
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_GEP[i], eigenvalues_real_GEP[i],
                                    fabs((eigs_GEP[i] - eigenvalues_real_GEP[i]) / eigs_GEP[i]));
#endif
    EXPECT_NEAR(fabs((eigs_GEP[i] - eigenvalues_real_GEP[i]) / eigs_GEP[i]), 0., tol);
    EXPECT_DOUBLE_EQ(eigenvalues_imag_GEP[i], 0.);
  }

  // compute eigenvalues with JADAMILU
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<int> IA, JA;
  std::vector<double> VA, EIGS_M;
  std::vector<Vector> EIGV;
  mass.sym_csr_arrays(pattern, IA, JA, VA, 1);
  t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&IA[0], &JA[0], &VA[0], N, N, EIGS_M, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_M.size(), N);
#if defined(SHOW_EIGS_)
  std::cout << "\nJADAMILU\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  tol = 1e-12;
  for (int i = 0; i < N; ++i)
  {
    EXPECT_NEAR(EIGS_M[i], eigenvalues_real_M[i], tol);
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_M[i], EIGS_M[i],
                                    fabs((eigs_M[i] - EIGS_M[i]) / eigs_M[i]));
#endif
  }

  std::vector<int> inMIA, inMJA;
  std::vector<double> inMVA, EIGS_inM;
  in_mass.sym_csr_arrays(pattern, inMIA, inMJA, inMVA, 1);
  t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&inMIA[0], &inMJA[0], &inMVA[0], inN, inN, EIGS_inM, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_inM.size(), inN);
#if defined(SHOW_EIGS_)
  std::cout << "inMass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
  printf("%12s  %12s  %12s\n", "Matlab", "LAPACK", "JADAMILU");
#endif
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_inM[i], eigenvalues_real_inM[i], EIGS_inM[i]);
#endif
  }

  std::vector<int> inSIA, inSJA;
  std::vector<double> inSVA, EIGS_inS;
  in_stif.sym_csr_arrays(pattern, inSIA, inSJA, inSVA, 1);
  t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&inSIA[0], &inSJA[0], &inSVA[0], inN, inN, EIGS_inS, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_inS.size(), inN);
#if defined(SHOW_EIGS_)
  std::cout << "inStif matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
  printf("%12s  %12s  %12s\n", "Matlab", "LAPACK", "JADAMILU");
#endif
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_inS[i], eigenvalues_real_inS[i], EIGS_inS[i]);
#endif
  }

  std::vector<double> EIGS_GEP;
  t0 = get_wall_time();
  in_stif.sym_csr_arrays(pattern, inSIA, inSJA, inSVA, 1);
  in_mass.sym_csr_arrays(pattern, inMIA, inMJA, inMVA, 1);
  eigensolver_JADAMILU_GHEP(&inSIA[0], &inSJA[0], &inSVA[0],
                            &inMIA[0], &inMJA[0], &inMVA[0],
                            inN, inN, EIGS_GEP, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_GEP.size(), inN);
#if defined(SHOW_EIGS_)
  std::cout << "Generalized eigenproblem\n";
  std::cout << "time = " << t1 - t0 << " s\n";
  printf("%12s  %12s  %12s\n", "Matlab", "LAPACK", "JADAMILU");
#endif
  for (int i = 0; i < inN; ++i)
  {
#if defined(SHOW_EIGS_)
    printf("%2.6e  %2.6e  %2.6e\n", eigs_GEP[i], eigenvalues_real_GEP[i], EIGS_GEP[i]);
#endif
  }

  // the eigenvectors are different. They can be seen, not compared
#if defined(SHOW_EIGS_)
  printf("\nEigenvectors\n");
  for (int i = 0; i < inN; ++i)
  {
    printf("LAPACK: ");
    for (int j = 0; j < inN; ++j)
      printf("%2.6e  ", eigenvector_real_GEP[i](j));
    printf("\nJADAMI: ");
    for (int j = 0; j < inN; ++j)
      printf("%2.6e  ", EIGV[i](j));
    printf("\n---\n");
  }
#endif

#endif
}
// -----------------------------------------------------------------------------
TEST(PetscMatrix, eigensolver_time_10_10)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 10;
  const int    ny = 10;
  const int    N = (nx+1)*(ny+1);
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix mass(pattern);
  PetscMatrix stif(pattern);

  dof_handler.build_mass_matrix(ConstantFunction(1.), mass);
  dof_handler.build_stiffness_matrix(ConstantFunction(1.), stif);

  mass.final_assembly();
  stif.final_assembly();

#if 0 // save matrices and compute Matlab version OR compare the results
  // represent mass and stiffness matrices in matlab view to compare eigenvalues
  // computed in Matlab with others
  std::ofstream out("mass_eigen2.dat");
  require(out, "File mass_eigen2.dat can't be opened");
  mass.matlab_view(pattern, out);
  out.close();

  out.open("stif_eigen2.dat");
  require(out, "File stif_eigen2.dat can't be opened");
  stif.matlab_view(pattern, out);
  out.close();
#else

  // compute eigenvalues with Lapack through SLEPc
  // mass matrix
  std::vector<double> eigenvalues_real_M(N);
  std::vector<double> eigenvalues_imag_M(N);
  std::vector<Vector> eigenvector_real_M(N);
  std::vector<Vector> eigenvector_imag_M(N);
  double t0 = get_wall_time();
  int nconv = eigensolver_SLEPc_LAPACK_HEP(mass,
                                        eigenvalues_real_M, eigenvalues_imag_M,
                                        eigenvector_real_M, eigenvector_imag_M);
  double t1 = get_wall_time();
  EXPECT_EQ(nconv, N);

#if defined(SHOW_EIGS_)
  std::cout << "\nLAPACK (SLEPc)\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif

  // stiffness matrix
  std::vector<double> eigenvalues_real_S(N);
  std::vector<double> eigenvalues_imag_S(N);
  std::vector<Vector> eigenvector_real_S(N);
  std::vector<Vector> eigenvector_imag_S(N);
  t0 = get_wall_time();
  nconv = eigensolver_SLEPc_LAPACK_HEP(stif,
                                       eigenvalues_real_S, eigenvalues_imag_S,
                                       eigenvector_real_S, eigenvector_imag_S);
  t1 = get_wall_time();
  EXPECT_EQ(nconv, N);

#if defined(SHOW_EIGS_)
  std::cout << "Stif matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif

  // generalized eigenproblem
  std::vector<double> eigenvalues_real_GEP(N);
  std::vector<double> eigenvalues_imag_GEP(N);
  std::vector<Vector> eigenvector_real_GEP(N);
  std::vector<Vector> eigenvector_imag_GEP(N);
  t0 = get_wall_time();
  nconv = eigensolver_SLEPc_LAPACK_GHEP(stif, mass,
                                   eigenvalues_real_GEP, eigenvalues_imag_GEP,
                                   eigenvector_real_GEP, eigenvector_imag_GEP);
  t1 = get_wall_time();
  EXPECT_EQ(nconv, N);

#if defined(SHOW_EIGS_)
  std::cout << "Generalized eigenproblem\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif

  // compute eigenvalues with JADAMILU
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<int> IA, JA;
  std::vector<double> VA, EIGS_M;
  std::vector<Vector> EIGV;
  const int NEIG = 30;
  mass.sym_csr_arrays(pattern, IA, JA, VA, 1);
  t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&IA[0], &JA[0], &VA[0], N, NEIG, EIGS_M, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_M.size(), NEIG);
#if defined(SHOW_EIGS_)
  std::cout << "\nJADAMILU\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  const double tol = 1e-13;
  for (int i = 0; i < NEIG; ++i)
  {
    EXPECT_NEAR(eigenvalues_real_M[i], EIGS_M[i], tol);
  }

  // compute eigenvalues with Krylov-Shur of SLEPc
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<double> eigenvalues_real_Kr_M(NEIG);
  std::vector<double> eigenvalues_imag_Kr_M(NEIG);
  std::vector<Vector> eigenvectors_real_Kr_M(NEIG);
  std::vector<Vector> eigenvectors_imag_Kr_M(NEIG);
  t0 = get_wall_time();
  eigensolver_SLEPc_Krylov_HEP(mass, NEIG,
                               eigenvalues_real_Kr_M,
                               eigenvalues_imag_Kr_M,
                               eigenvectors_real_Kr_M,
                               eigenvectors_imag_Kr_M);
  t1 = get_wall_time();
  EXPECT_EQ((int)eigenvalues_real_Kr_M.size(), NEIG);
#if defined(SHOW_EIGS_)
  std::cout << "\nKrylov (SLEPc)\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  for (int i = 0; i < NEIG; ++i)
  {
    EXPECT_NEAR(eigenvalues_real_M[i], eigenvalues_real_Kr_M[i], tol);
  }
#endif
}
// -----------------------------------------------------------------------------
TEST(PetscMatrix, eigensolver_time_20_20)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 20;
  const int    ny = 20;
  const int    N = (nx+1)*(ny+1);
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix mass(pattern);
  PetscMatrix stif(pattern);

  dof_handler.build_mass_matrix(ConstantFunction(1.), mass);
  dof_handler.build_stiffness_matrix(ConstantFunction(1.), stif);

  mass.final_assembly();
  stif.final_assembly();

#if 0 // save matrices and compute Matlab version OR compare the results
  // represent mass and stiffness matrices in matlab view to compare eigenvalues
  // computed in Matlab with others
  std::ofstream out("mass_eigen2.dat");
  require(out, "File mass_eigen1.dat can't be opened");
  mass.matlab_view(pattern, out);
  out.close();

  out.open("stif_eigen1.dat");
  require(out, "File stif_eigen2.dat can't be opened");
  stif.matlab_view(pattern, out);
  out.close();
#else

  // compute eigenvalues with Lapack through SLEPc
  // mass matrix
  std::vector<double> eigenvalues_real_M(N);
  std::vector<double> eigenvalues_imag_M(N);
  std::vector<Vector> eigenvector_real_M(N);
  std::vector<Vector> eigenvector_imag_M(N);
  double t0 = get_wall_time();
  int nconv = eigensolver_SLEPc_LAPACK_HEP(mass,
                                        eigenvalues_real_M, eigenvalues_imag_M,
                                        eigenvector_real_M, eigenvector_imag_M);
  double t1 = get_wall_time();
  EXPECT_EQ(nconv, N);
#if defined(SHOW_EIGS_)
  std::cout << "\nLAPACK (SLEPc)\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif

  // stiffness matrix
  std::vector<double> eigenvalues_real_S(N);
  std::vector<double> eigenvalues_imag_S(N);
  std::vector<Vector> eigenvector_real_S(N);
  std::vector<Vector> eigenvector_imag_S(N);
  t0 = get_wall_time();
  nconv = eigensolver_SLEPc_LAPACK_HEP(stif,
                                        eigenvalues_real_S, eigenvalues_imag_S,
                                        eigenvector_real_S, eigenvector_imag_S);
  t1 = get_wall_time();
  EXPECT_EQ(nconv, N);
#if defined(SHOW_EIGS_)
  std::cout << "Stif matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif

  // generalized eigenproblem
  std::vector<double> eigenvalues_real_GEP(N);
  std::vector<double> eigenvalues_imag_GEP(N);
  std::vector<Vector> eigenvector_real_GEP(N);
  std::vector<Vector> eigenvector_imag_GEP(N);
  t0 = get_wall_time();
  nconv = eigensolver_SLEPc_LAPACK_GHEP(stif, mass,
                                   eigenvalues_real_GEP, eigenvalues_imag_GEP,
                                   eigenvector_real_GEP, eigenvector_imag_GEP);
  t1 = get_wall_time();
  EXPECT_EQ(nconv, N);
#if defined(SHOW_EIGS_)
  std::cout << "Generalized eigenproblem\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif

  // compute eigenvalues with JADAMILU
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<int> IA, JA;
  std::vector<double> VA, EIGS_M;
  std::vector<Vector> EIGV;
  const int NEIG = 10;
  mass.sym_csr_arrays(pattern, IA, JA, VA, 1);
  t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&IA[0], &JA[0], &VA[0], N, NEIG, EIGS_M, EIGV);
  t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_M.size(), NEIG);
#if defined(SHOW_EIGS_)
  std::cout << "\nJADAMILU\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  double tol = 2e-12;
  for (int i = 0; i < NEIG; ++i)
  {
    EXPECT_NEAR(eigenvalues_real_M[i], EIGS_M[i], tol);
//    printf("%2.6e  %2.6e  %2.6e\n", eigenvalues_real_M[i], EIGS_M[i],
//                                    fabs((eigenvalues_real_M[i] - EIGS_M[i]) /
//                                    eigenvalues_real_M[i]));
  }

  // compute eigenvalues with Krylov-Shur of SLEPc
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<double> eigenvalues_real_Kr_M(NEIG);
  std::vector<double> eigenvalues_imag_Kr_M(NEIG);
  std::vector<Vector> eigenvectors_real_Kr_M(NEIG);
  std::vector<Vector> eigenvectors_imag_Kr_M(NEIG);
  t0 = get_wall_time();
  eigensolver_SLEPc_Krylov_HEP(mass, NEIG,
                               eigenvalues_real_Kr_M,
                               eigenvalues_imag_Kr_M,
                               eigenvectors_real_Kr_M,
                               eigenvectors_imag_Kr_M);
  t1 = get_wall_time();
  EXPECT_EQ((int)eigenvalues_real_Kr_M.size(), NEIG);
#if defined(SHOW_EIGS_)
  std::cout << "\nKrylov (SLEPc)\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  tol = 1.5e-5;
  for (int i = 0; i < NEIG; ++i)
  {
    EXPECT_NEAR(eigenvalues_real_M[i], eigenvalues_real_Kr_M[i], tol);
  }
#endif
}
// -----------------------------------------------------------------------------
TEST(PetscMatrix, eigensolver_time_40_40)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 40;
  const int    ny = 40;
  const int    N = (nx+1)*(ny+1);
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix mass(pattern);
  PetscMatrix stif(pattern);

  dof_handler.build_mass_matrix(ConstantFunction(1.), mass);
  dof_handler.build_stiffness_matrix(ConstantFunction(1.), stif);

  mass.final_assembly();
  stif.final_assembly();

#if 0 // save matrices and compute Matlab version OR compare the results
  // represent mass and stiffness matrices in matlab view to compare eigenvalues
  // computed in Matlab with others
  std::ofstream out("mass_eigen2.dat");
  require(out, "File mass_eigen1.dat can't be opened");
  mass.matlab_view(pattern, out);
  out.close();

  out.open("stif_eigen1.dat");
  require(out, "File stif_eigen2.dat can't be opened");
  stif.matlab_view(pattern, out);
  out.close();
#else

  // compute eigenvalues with Lapack through SLEPc
  // mass matrix
  std::vector<double> eigenvalues_real_M(N);
  std::vector<double> eigenvalues_imag_M(N);
  std::vector<Vector> eigenvector_real_M(N);
  std::vector<Vector> eigenvector_imag_M(N);
  double t0 = get_wall_time();
  int nconv = eigensolver_SLEPc_LAPACK_HEP(mass,
                                        eigenvalues_real_M, eigenvalues_imag_M,
                                        eigenvector_real_M, eigenvector_imag_M);
  double t1 = get_wall_time();
  EXPECT_EQ(nconv, N);
#if defined(SHOW_EIGS_)
  std::cout << "\nLAPACK (SLEPc)\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif

//  // stiffness matrix
//  std::vector<double> eigenvalues_real_S(N);
//  std::vector<double> eigenvalues_imag_S(N);
//  std::vector<Vector> eigenvector_real_S(N);
//  std::vector<Vector> eigenvector_imag_S(N);
//  t0 = get_wall_time();
//  nconv = eigensolver_SLEPc_LAPACK_HEP(stif,
//                                        eigenvalues_real_S, eigenvalues_imag_S,
//                                        eigenvector_real_S, eigenvector_imag_S);
//  t1 = get_wall_time();
//  EXPECT_EQ(nconv, N);
//  std::cout << "Stif matrix\n";
//  std::cout << "time = " << t1 - t0 << " s\n";

//  // generalized eigenproblem
//  std::vector<double> eigenvalues_real_GEP(N);
//  std::vector<double> eigenvalues_imag_GEP(N);
//  std::vector<Vector> eigenvector_real_GEP(N);
//  std::vector<Vector> eigenvector_imag_GEP(N);
//  t0 = get_wall_time();
//  nconv = eigensolver_SLEPc_LAPACK_GHEP(stif, mass,
//                                   eigenvalues_real_GEP, eigenvalues_imag_GEP,
//                                   eigenvector_real_GEP, eigenvector_imag_GEP);
//  t1 = get_wall_time();
//  EXPECT_EQ(nconv, N);
//  std::cout << "Generalized eigenproblem\n";
//  std::cout << "time = " << t1 - t0 << " s\n";

  // compute eigenvalues with JADAMILU
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<int> IA, JA;
  std::vector<double> VA, EIGS_M;
  std::vector<Vector> EIGV;
  const int NEIG = 30;
  mass.sym_csr_arrays(pattern, IA, JA, VA, 1);
  t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&IA[0], &JA[0], &VA[0], N, NEIG, EIGS_M, EIGV);
  t1 = get_wall_time();
  const int EXPECT_NEIG = 27; // it computes 27 only, so we check it
  EXPECT_EQ((int)EIGS_M.size(), EXPECT_NEIG);
#if defined(SHOW_EIGS_)
  std::cout << "\nJADAMILU\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  const double tol = 1e-10;
  for (int i = 0; i < std::min((int)EIGS_M.size(), NEIG); ++i)
  {
    EXPECT_NEAR(eigenvalues_real_M[i], EIGS_M[i], tol);
//    printf("%2.6e  %2.6e  %2.6e\n", eigenvalues_real_M[i], EIGS_M[i],
//                                    fabs((eigenvalues_real_M[i] - EIGS_M[i]) /
//                                    eigenvalues_real_M[i]));
  }

  // compute eigenvalues with Krylov-Shur of SLEPc
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<double> eigenvalues_real_Kr_M(NEIG);
  std::vector<double> eigenvalues_imag_Kr_M(NEIG);
  std::vector<Vector> eigenvectors_real_Kr_M(NEIG);
  std::vector<Vector> eigenvectors_imag_Kr_M(NEIG);
  t0 = get_wall_time();
  eigensolver_SLEPc_Krylov_HEP(mass, NEIG,
                               eigenvalues_real_Kr_M,
                               eigenvalues_imag_Kr_M,
                               eigenvectors_real_Kr_M,
                               eigenvectors_imag_Kr_M);
  t1 = get_wall_time();
  EXPECT_EQ((int)eigenvalues_real_Kr_M.size(), NEIG);
#if defined(SHOW_EIGS_)
  std::cout << "\nKrylov (SLEPc)\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  for (int i = 0; i < NEIG; ++i)
  {
    EXPECT_NEAR(eigenvalues_real_M[i], eigenvalues_real_Kr_M[i], tol);
//    printf("%2.6e  %2.6e  %2.6e\n", eigenvalues_real_M[i], EIGS_M[i],
//                                    fabs((eigenvalues_real_M[i] - EIGS_M[i]) /
//                                    eigenvalues_real_M[i]));
  }
#endif // save matrices and compute Matlab version OR compare the results
}
// -----------------------------------------------------------------------------
TEST(PetscMatrix, eigensolver_time_50_50)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 50;
  const int    ny = 50;
  const int    N = (nx+1)*(ny+1);
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix mass(pattern);
  dof_handler.build_mass_matrix(ConstantFunction(1.), mass);
  mass.final_assembly();

  // compute eigenvalues with JADAMILU
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<int> IA, JA;
  std::vector<double> VA, EIGS_M;
  std::vector<Vector> EIGV;
  const int NEIG = 20;
  mass.sym_csr_arrays(pattern, IA, JA, VA, 1);
  double t0 = get_wall_time();
  eigensolver_JADAMILU_HEP(&IA[0], &JA[0], &VA[0], N, NEIG, EIGS_M, EIGV);
  double t1 = get_wall_time();
  EXPECT_EQ((int)EIGS_M.size(), NEIG);
#if defined(SHOW_EIGS_)
  std::cout << "\nJADAMILU\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
  for (int i = 0; i < NEIG; ++i)
    printf("%2.6e\n", EIGS_M[i]);
#endif

  // compute eigenvalues with Krylov-Shur of SLEPc
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<double> eigenvalues_real_Kr_M(NEIG);
  std::vector<double> eigenvalues_imag_Kr_M(NEIG);
  std::vector<Vector> eigenvectors_real_Kr_M(NEIG);
  std::vector<Vector> eigenvectors_imag_Kr_M(NEIG);
  t0 = get_wall_time();
  eigensolver_SLEPc_Krylov_HEP(mass, NEIG,
                               eigenvalues_real_Kr_M,
                               eigenvalues_imag_Kr_M,
                               eigenvectors_real_Kr_M,
                               eigenvectors_imag_Kr_M);
  t1 = get_wall_time();
  EXPECT_EQ((int)eigenvalues_real_Kr_M.size(), NEIG);
#if defined(SHOW_EIGS_)
  std::cout << "\nKrylov (SLEPc)\n";
  std::cout << "Mass matrix\n";
  std::cout << "time = " << t1 - t0 << " s\n";
#endif
  const double tol = 5.7e-6;
  for (int i = 0; i < NEIG; ++i)
  {
    EXPECT_NEAR(EIGS_M[i], eigenvalues_real_Kr_M[i], tol);
//    printf("%2.6e  %2.6e  %2.6e\n", eigenvalues_real_M[i], EIGS_M[i],
//                                    fabs((eigenvalues_real_M[i] - EIGS_M[i]) /
//                                    eigenvalues_real_M[i]));
  }
}

// -----------------------------------------------------------------------------
// This test measures time required to get a solution with different methods.
// The computed time is not kept anywhere, so this test can't be considered as
// a regression test. The solutions are not compared to each other, and only
// printed in output. Therefore the test is mostly pointless, and was developped
// at the early stage of incorporation of eigensolvers in code.
// -----------------------------------------------------------------------------
#if defined(SHOW_EIGS_)
TEST(PetscMatrix, eigensolver_jadamilu_matlab_50_50)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 50;
  const int    ny = 50;
  const int    N = (nx-1)*(ny-1);
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGInteriorDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);
  PetscMatrix mass(pattern);
  dof_handler.build_mass_matrix(ConstantFunction(1.), mass);
  mass.final_assembly();
  PetscMatrix stif(pattern);
  dof_handler.build_stiffness_matrix(ConstantFunction(1.), stif);
  stif.final_assembly();

#if 0
  // represent mass and stiffness matrices in matlab view to compare eigenvalues
  // computed in Matlab with others
  std::ofstream out("mass_eigen50.dat");
  require(out, "File mass_eigen50.dat can't be opened");
  mass.matlab_view(pattern, out);
  out.close();

  out.open("stif_eigen50.dat");
  require(out, "File stif_eigen50.dat can't be opened");
  stif.matlab_view(pattern, out);
  out.close();

#else

  const double eigs[] = { 34.7457,64.4031,64.4031,94.0604,113.9619,113.9619,
                          143.6192,143.6192,183.6175,183.6175,193.1777,213.2746,
                          213.2746,262.8330,262.8330,273.6444,273.6444,303.3013,
                          303.3013,332.4879,352.8593,352.8593,384.3971,384.3971,
                          414.0538,414.0538,422.5138,422.5138,463.6114,463.6114
                        };

  // compute eigenvalues with JADAMILU:
  // get all arrays describing the sparse matrix in symmetric CSR format with
  // all numbers starting from 1 (for Fortran)
  std::vector<int> sIA, sJA, mIA, mJA;
  std::vector<double> sVA, mVA, EIGS;
  std::vector<Vector> EIGV;
  const int NEIG = 20;
  mass.sym_csr_arrays(pattern, mIA, mJA, mVA, 1);
  stif.sym_csr_arrays(pattern, sIA, sJA, sVA, 1);
  double jt0 = get_wall_time();
  eigensolver_JADAMILU_GHEP(&sIA[0], &sJA[0], &sVA[0],
                            &mIA[0], &mJA[0], &mVA[0],
                            N, NEIG, EIGS, EIGV);
  double jt1 = get_wall_time();
  EXPECT_EQ((int)EIGS.size(), NEIG);

  // compute eigenvalues with Krylov-Shur of SLEPc
  std::vector<double> eigenvalues_real_Kr_M(NEIG);
  std::vector<double> eigenvalues_imag_Kr_M(NEIG);
  std::vector<Vector> eigenvectors_real_Kr_M(NEIG);
  std::vector<Vector> eigenvectors_imag_Kr_M(NEIG);
  double kt0 = get_wall_time();
  eigensolver_SLEPc_Krylov_GHEP(stif, mass, NEIG,
                                eigenvalues_real_Kr_M,
                                eigenvalues_imag_Kr_M,
                                eigenvectors_real_Kr_M,
                                eigenvectors_imag_Kr_M);
  double kt1 = get_wall_time();
  EXPECT_EQ((int)eigenvalues_real_Kr_M.size(), NEIG);

  std::cout << "Generalized eigenproblem\n";
  std::cout << "JADA time = " << jt1 - jt0 << " s\n";
  std::cout << "KRYL time = " << kt1 - kt0 << " s\n";
  printf("%12s  %12s  %12s\n", "Matlab", "JADAMILU", "Krylov");
  for (int i = 0; i < NEIG; ++i)
    printf("%2.6e  %2.6e  %2.6e\n", eigs[i], EIGS[i], eigenvalues_real_Kr_M[i]);

#endif // output for Matlab or computations here
}
#endif // if defined(SHOW_EIGS_)

//==============================================================================
TEST(PetscMatrix, symmetric)
{
  DensePattern patternA(3, 3);
  PetscMatrixPtr A(new PetscMatrix(patternA));

  A->add_value(0, 0, 1);
  A->add_value(0, 1, 2);
  A->add_value(0, 2, 3);
  A->add_value(1, 0, 4);
  A->add_value(1, 1, 5);
  A->add_value(1, 2, 6);
  A->add_value(2, 0, 7);
  A->add_value(2, 1, 8);
  A->add_value(2, 2, 9);
  A->final_assembly();

  EXPECT_FALSE(A->symmetric());

  A->insert_value(0, 0, 1);
  A->insert_value(0, 1, 2);
  A->insert_value(0, 2, 3);
  A->insert_value(1, 0, 2);
  A->insert_value(1, 1, 3);
  A->insert_value(1, 2, 4);
  A->insert_value(2, 0, 3);
  A->insert_value(2, 1, 4);
  A->insert_value(2, 2, 5);
  A->final_assembly();

  EXPECT_TRUE(A->symmetric());
}

//==============================================================================
TEST(PetscMatrix, symmetrize)
{
  DensePattern patternA(3, 3);
  PetscMatrixPtr A(new PetscMatrix(patternA));

  A->add_value(0, 0, 1);
  A->add_value(0, 1, 2);
  A->add_value(0, 2, 3);
  A->add_value(1, 0, 4);
  A->add_value(1, 1, 5);
  A->add_value(1, 2, 6);
  A->add_value(2, 0, 7);
  A->add_value(2, 1, 8);
  A->add_value(2, 2, 9);
  A->final_assembly();

  EXPECT_FALSE(A->symmetric());

  math::symmetrize(*A.get());
  A->final_assembly();

  EXPECT_TRUE(A->symmetric());
}

//==============================================================================
TEST(PetscMatrix, mat_AXPY)
{
  const int n = 3;
  const int m = 3;
  DensePattern pattern(n, m);
  PetscMatrixPtr A(new PetscMatrix(pattern));
  PetscMatrixPtr B(new PetscMatrix(pattern));

  for (int i = 0, k = 1; i < n; ++i)
    for (int j = 0; j < m; ++j, ++k) {
      A->add_value(i, j, k);
      B->add_value(i, j, -k);
    }

  A->final_assembly();
  B->final_assembly();

  mat_AXPY(*A.get(), 1., *B.get());

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      EXPECT_DOUBLE_EQ((*A)(i, j), 0.);

  mat_AXPY(*A.get(), -1., *B.get());

  for (int i = 0, k = 1; i < n; ++i)
    for (int j = 0; j < m; ++j, ++k)
      EXPECT_DOUBLE_EQ((*A)(i, j), k);
}


#endif // TEST_FEMPLUS_PETSC_MATRIX_HPP
