#ifndef FEMPLUS_TEST_DENSE_PATTERN_HPP
#define FEMPLUS_TEST_DENSE_PATTERN_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/dense_pattern.hpp"

using namespace femplus;

// =============================================================================
TEST(DensePattern, to_sym_csr_0)
{
  DensePattern dense(5, 5);

  const int sr[] = { 0, 0, 1, 3, 6, 10 };
  const int sc[] = { 0, 0, 1, 0, 1, 2, 0, 1, 2, 3 };

  std::vector<int> s_row, s_col;
  dense.to_sym_csr(s_row, s_col);

  EXPECT_EQ(s_row.size(), 6);
  for (int i = 0; i < 6; ++i)
    EXPECT_EQ(s_row[i], sr[i]);

  EXPECT_EQ(s_col.size(), 10);
  for (int i = 0; i < 10; ++i)
    EXPECT_EQ(s_col[i], sc[i]);
}

#endif // FEMPLUS_TEST_DENSE_PATTERN_HPP
