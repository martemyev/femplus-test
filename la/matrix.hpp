#ifndef FEMPLUS_TEST_MATRIX_HPP
#define FEMPLUS_TEST_MATRIX_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/dense_matrix.hpp"
#include "femplus/dense_pattern.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/math_functions.hpp"
#include "femplus/auxiliary_functions.hpp"

#include <iostream>
#include <fstream>

using namespace femplus;


//==============================================================================
//
// First tests check the accuracy of the inversion of dense matrices using PETSc
// and LAPACK comparing the inverted matrices with the results from Octave. The
// matrices are of low order.
//
//==============================================================================
TEST(Matrix, inv_matrix_petsc_N2)
{
  const int N = 2;

  const double matA[][N] = { { 1., 2. },
                             { 3., 4. } };
  const double matB[][N] = { {-2., 1. },
                             {1.5, -0.5}};

  DenseMatrix A(N, N);
  DensePattern pattern(N, N);
  PetscMatrix B(pattern);

  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      A(i, j) = matA[i][j];

  inv_matrix_petsc(A, B); // inversion for PETSc matrices
  B.final_assembly();

  DenseMatrix C = inverse(A); // inversion for dense matrices

  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
    {
      EXPECT_DOUBLE_EQ(B(i, j), matB[i][j]);
      EXPECT_DOUBLE_EQ(C(i, j), matB[i][j]);
    }
}
//==============================================================================
TEST(Matrix, inv_matrix_petsc_N3)
{
  const int N = 3;

  const double matA[][N] = { { 1., 2., 3. },
                             { 3., 5., 3. },
                             { 9., 8., 9. } };
  const double matB[][N] = { { -5.0000e-01, -1.4286e-01,  2.1429e-01 },
                             { -3.5686e-17,  4.2857e-01, -1.4286e-01 },
                             {  5.0000e-01, -2.3810e-01,  2.3810e-02 } };

  DenseMatrix A(N, N);
  DensePattern pattern(N, N);
  PetscMatrix B(pattern);

  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      A(i, j) = matA[i][j];

  inv_matrix_petsc(A, B); // inversion for PETSc matrices
  B.final_assembly();

  DenseMatrix C = inverse(A); // inversion for dense matrices

  const double tol_abs = 1e-16;
  const double tol_rel = 2e-5;
  for (int i = 0; i < N; ++i)
  {
    for (int j = 0; j < N; ++j)
      if (fabs(matB[i][j]) < math::FLOAT_NUMBERS_EQUALITY_TOLERANCE)
      {
        EXPECT_NEAR(fabs(B(i, j) - matB[i][j]), 0., tol_abs);
        EXPECT_NEAR(fabs(C(i, j) - matB[i][j]), 0., tol_abs);
      }
      else
      {
        EXPECT_NEAR(fabs((B(i, j) - matB[i][j]) / matB[i][j]), 0., tol_rel);
        EXPECT_NEAR(fabs((C(i, j) - matB[i][j]) / matB[i][j]), 0., tol_rel);
      }
  }
}
//==============================================================================
TEST(Matrix, inv_matrix_petsc_N5)
{
  const int N = 5;

  const double matA[][N] = { { 0.592995, 0.235927, 0.073594, 0.745547, 0.724077 },
                             { 0.194026, 0.071158, 0.569205, 0.977741, 0.154245 },
                             { 0.679769, 0.254924, 0.019777, 0.315349, 0.981417 },
                             { 0.385968, 0.176723, 0.131318, 0.161059, 0.789273 },
                             { 0.524787, 0.653258, 0.718162, 0.129113, 0.556021 } };
  const double matB[][N] = { { -5.786992,  2.368444,  10.594196, -8.284099, -0.061151 },
                             {  7.450932, -3.654615, -8.884195,   3.731361,  1.695432 },
                             { -4.026802,  2.139541,  3.298292,  -0.962154,  0.194421 },
                             {  2.749960, -0.327586, -2.878527,   1.240000, -0.169629 },
                             {  1.270448, -0.629046, -3.152895,   4.389631, -0.347447 } };

  DenseMatrix A(N, N);
  DensePattern pattern(N, N);
  PetscMatrix B(pattern);

  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      A(i, j) = matA[i][j];

  inv_matrix_petsc(A, B); // inversion for PETSc matrices
  B.final_assembly();

  DenseMatrix C = inverse(A); // inversion for dense matrices

  const double tol_abs = 1e-16;
  const double tol_rel = 8e-5;
  for (int i = 0; i < N; ++i)
  {
    for (int j = 0; j < N; ++j)
      if (fabs(matB[i][j]) < math::FLOAT_NUMBERS_EQUALITY_TOLERANCE)
      {
        EXPECT_NEAR(fabs(B(i, j) - matB[i][j]), 0., tol_abs);
        EXPECT_NEAR(fabs(C(i, j) - matB[i][j]), 0., tol_abs);
      }
      else
      {
        EXPECT_NEAR(fabs((B(i, j) - matB[i][j]) / matB[i][j]), 0., tol_rel);
        EXPECT_NEAR(fabs((C(i, j) - matB[i][j]) / matB[i][j]), 0., tol_rel);
      }
  }
}





//==============================================================================
//
// Second part of tests is designed to adress performace issues. We increase the
// size of dense square matrices for inversion, and compare the time of
// inversion between PETSc and LAPACK (with row and column major arrangment of
// elements)
//
//==============================================================================
TEST(Matrix, performance_LAPACK_PETSc)
{
  // algorithm:
  // 1. for each size: from n1 to n2
  // 2.   create 3 matrices with the same elements
  // 3.   compute inverse matrices using:
  //                                       PETSc for row major
  //                                       LAPACK for row major
  //                                       LAPACK for column major
  // 4.   compare the inverse matrices to make sure they are the same
  // 5.   compare the time required for inversion by these 3 methods

  const int n1 = 10;
  const int n2 = 400;
  const int dn = 10; // step in increment between n1 and n2

  double total_time_petsc = 0;
  double total_time_row_lapack = 0;
  double total_time_col_lapack = 0;

  for (int n = n1; n <= n2; n += dn)
  {
    DenseMatrix A_petsc(n, n);
    DenseMatrix A_row_lapack(n, n, 0, DenseMatrix::ROW_MAJOR);
    DenseMatrix A_col_lapack(n, n, 0, DenseMatrix::COL_MAJOR);

    // initialize the matrices with the same values
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        const double value = rand() % 100;
        A_petsc(i, j) = value;
        A_row_lapack(i, j) = value;
        A_col_lapack(i, j) = value;
      }

    // inversion with PETSc
    double twall_petsc_0 = get_wall_time();
    double tcpu_petsc_0  = get_cpu_time();
    DensePattern pattern(n, n);
    PetscMatrix B_petsc(pattern);
    inv_matrix_petsc(A_petsc, B_petsc);
    B_petsc.final_assembly();
    double twall_petsc_1 = get_wall_time();
    double tcpu_petsc_1  = get_cpu_time();
    total_time_petsc += twall_petsc_1 - twall_petsc_0;

    // inversion with LAPACK (matrix is in row major order)
    double twall_row_lapack_0 = get_wall_time();
    double tcpu_row_lapack_0  = get_cpu_time();
    DenseMatrix B_row_lapack = inverse(A_row_lapack);
    double twall_row_lapack_1 = get_wall_time();
    double tcpu_row_lapack_1  = get_cpu_time();
    total_time_row_lapack += twall_row_lapack_1 - twall_row_lapack_0;

    // inversion with LAPACK (matrix is in column major order)
    double twall_col_lapack_0 = get_wall_time();
    double tcpu_col_lapack_0  = get_cpu_time();
    DenseMatrix B_col_lapack = inverse(A_col_lapack);
    double twall_col_lapack_1 = get_wall_time();
    double tcpu_col_lapack_1  = get_cpu_time();
    total_time_col_lapack += twall_col_lapack_1 - twall_col_lapack_0;

    // check that the inverse matrices are the same
    double L2_0 = 0, L2_diff_0 = 0, L2_1 = 0, L2_diff_1 = 0, L2_2 = 0, L2_diff_2 = 0;
    for (int i = 0; i < n; ++i)
    {
      for (int j = 0; j < n; ++j)
      {
        L2_0 += B_petsc(i, j) * B_petsc(i, j);
        L2_1 += B_row_lapack(i, j) * B_row_lapack(i, j);
        L2_2 += B_col_lapack(i, j) * B_col_lapack(i, j);

        L2_diff_0 += (B_petsc(i, j) - B_row_lapack(i, j)) * (B_petsc(i, j) - B_row_lapack(i, j));
        L2_diff_1 += (B_petsc(i, j) - B_col_lapack(i, j)) * (B_petsc(i, j) - B_col_lapack(i, j));
        L2_diff_2 += (B_col_lapack(i, j) - B_row_lapack(i, j)) * (B_col_lapack(i, j) - B_row_lapack(i, j));
      }
    }

    L2_diff_0 = sqrt(L2_diff_0 / L2_0);
    L2_diff_1 = sqrt(L2_diff_1 / L2_1);
    L2_diff_2 = sqrt(L2_diff_2 / L2_2);

    // output
    const bool output = false;
    if (output)
    {
      std::cout << "size: " << n << "\n";
      std::cout << "L2_diff_0 = " << L2_diff_0 << "\n";
      std::cout << "L2_diff_1 = " << L2_diff_1 << "\n";
      std::cout << "L2_diff_2 = " << L2_diff_2 << "\n";
      std::cout << "PETSc time: wall = " << twall_petsc_1 - twall_petsc_0
                << " cpu = " << tcpu_petsc_1 - tcpu_petsc_0 << "\n";
      std::cout << "LAPACK (row) time: wall = " << twall_row_lapack_1 - twall_row_lapack_0
                << " cpu = " << tcpu_row_lapack_1 - tcpu_row_lapack_0 << "\n";
      std::cout << "LAPACK (col) time: wall = " << twall_col_lapack_1 - twall_col_lapack_0
                << " cpu = " << tcpu_col_lapack_1 - tcpu_col_lapack_0 << "\n";
    }

    // accuracy comparison
    const double tol = 1e-12;
    EXPECT_TRUE(L2_diff_0 < tol);
    EXPECT_TRUE(L2_diff_1 < tol);
    EXPECT_TRUE(L2_diff_2 < tol);
  }

  // time comparison
  EXPECT_TRUE(total_time_petsc > total_time_row_lapack);
  EXPECT_TRUE(total_time_row_lapack > total_time_col_lapack);

  const bool output = true;
  if (output)
  {
    std::cout << "total time PETSc:        " << d2s<double>(total_time_petsc, 1, 6) << "\n";
    std::cout << "total time LAPACK (row): " << d2s<double>(total_time_row_lapack, 1, 6) << "\n";
    std::cout << "total time LAPACK (col): " << d2s<double>(total_time_col_lapack, 1, 6) << "\n";
  }
}

#endif // FEMPLUS_TEST_MATRIX_HPP
