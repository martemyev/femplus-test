#ifndef FEMPLUS_TEST_VECTOR_HPP
#define FEMPLUS_TEST_VECTOR_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/vector.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/petsc_vector.hpp"

#include <fstream>

using namespace femplus;

// =============================================================================
TEST(Vector, default_ctor)
{
  // default ctor does nothing
  EXPECT_NO_THROW(Vector v0);
  std::vector<Vector> v1;
  EXPECT_NO_THROW(v1.resize(100));
  EXPECT_NO_THROW(v1.clear()); // check destructor for empty vectors
  Vector *v2;
  EXPECT_NO_THROW(v2 = new Vector[100]);
  EXPECT_NO_THROW(delete[] v2); // check destructor for empty vectors
}

// =============================================================================
TEST(Vector, dtor)
{
  Vector *v0;
  EXPECT_NO_THROW(v0 = new Vector);
  EXPECT_NO_THROW(delete v0);
}

// =============================================================================
TEST(Vector, ctor1)
{
  const int n = 100;
  Vector v0(n);
  EXPECT_EQ(v0.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 0); // check the default values, and the operator ()

  Vector v1(n, 42.);
  EXPECT_EQ(v1.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v1(i), 42.);
}

// =============================================================================
TEST(Vector, copy_ctor)
{
  const int n = 100;
  Vector v0(n, 42.);
  Vector v1(v0);
  Vector v2 = v1;
  EXPECT_EQ(v0.size(), n);
  EXPECT_EQ(v1.size(), n);
  EXPECT_EQ(v2.size(), n);
  for (int i = 0; i < n; ++i)
  {
    EXPECT_DOUBLE_EQ(v0(i), 42.);
    EXPECT_DOUBLE_EQ(v1(i), 42.);
    EXPECT_DOUBLE_EQ(v2(i), 42.);
  }
}

// =============================================================================
TEST(Vector, empty)
{
  Vector v0;
  EXPECT_TRUE(v0.empty());

  // copying from an empty vector is prohibited
  EXPECT_ANY_THROW(Vector v1(v0));

  // copying from an empty vector is prohibited
  EXPECT_ANY_THROW(Vector v2 = v0);

  // copying from an empty vector is prohibited
  Vector v3;
  EXPECT_ANY_THROW(v3 = v0);

  Vector v4(1);
  EXPECT_FALSE(v4.empty());

  Vector v5(v4);
  EXPECT_FALSE(v5.empty());

  Vector v6 = v5;
  EXPECT_FALSE(v6.empty());
}

// =============================================================================
TEST(Vector, ctor_petsc)
{
  const int n = 100;
  Vec petsc_vec;
  VecCreateSeq(PETSC_COMM_SELF, n, &petsc_vec);
  for (int i = 0; i < n; ++i)
    VecSetValue(petsc_vec, i, 1.*i, INSERT_VALUES);

  Vector v0(petsc_vec);
  Vector v1 = petsc_vec;
  EXPECT_FALSE(v0.empty());
  EXPECT_FALSE(v1.empty());
  EXPECT_EQ(v0.size(), n);
  EXPECT_EQ(v1.size(), n);
  for (int i = 0; i < n; ++i)
  {
    EXPECT_DOUBLE_EQ(v0(i), 1.*i);
    EXPECT_DOUBLE_EQ(v1(i), 1.*i);
  }

  VecDestroy(&petsc_vec);
}

// =============================================================================
TEST(Vector, ctor_stdvector)
{
  const int n = 100;
  std::vector<double> std_vec(n);
  for (int i = 0; i < n; ++i)
    std_vec[i] = 1.*i;

  Vector v0(std_vec);
  Vector v1 = std_vec;
  EXPECT_FALSE(v0.empty());
  EXPECT_FALSE(v1.empty());
  EXPECT_EQ(v0.size(), n);
  EXPECT_EQ(v1.size(), n);
  for (int i = 0; i < n; ++i)
  {
    EXPECT_DOUBLE_EQ(v0(i), 1.*i);
    EXPECT_DOUBLE_EQ(v1(i), 1.*i);
  }

  std::vector<double> v10; // empty vector
  // initialization from an empty vector is prohibited
  EXPECT_ANY_THROW(Vector v11(v10));
}

// =============================================================================
TEST(Vector, clear)
{
  const int n = 100;
  Vector v0(n);
  Vector v1(n, 42.);
  EXPECT_FALSE(v0.empty());
  EXPECT_FALSE(v1.empty());
  v0.clear();
  v1.clear();
  EXPECT_TRUE(v0.empty());
  EXPECT_TRUE(v1.empty());
}

// =============================================================================
TEST(Vector, resize)
{
  const int n = 100;
  const int m = 200;

  Vector v0(n);
  EXPECT_EQ(v0.size(), n);
  v0.resize(m);
  EXPECT_EQ(v0.size(), m);

  Vector v1;
  v1.resize(n, 42.);
  EXPECT_EQ(v1.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v1(i), 42.);
}

// =============================================================================
TEST(Vector, braces)
{
  const int n = 100;
  Vector v0(n);
  for (int i = 0; i < n; ++i)
    v0(i) = 1.*i;
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 1.*i);
}

// =============================================================================
TEST(Vector, petsc_vector_0)
{
  const int n = 100;
  Vector v0(n);
  for (int i = 0; i < n; ++i)
    v0(i) = 1. * (i+1);

  std::unique_ptr<PetscVector> petsc_vec = v0.petsc_vector();
  int size;
  VecGetSize(petsc_vec->vec(), &size);
  EXPECT_EQ(size, n);
  for (int i = 0; i < n; ++i)
  {
    double value;
    VecGetValues(petsc_vec->vec(), 1, &i, &value);
    EXPECT_DOUBLE_EQ(value, 1. * (i+1));
  }
}

// =============================================================================
TEST(Vector, petsc_vector_1)
{
  const int n = 100;
  Vector v0(n);
  for (int i = 0; i < n; ++i)
    v0(i) = 1. * (i+1);

  // this is the difference between this test and the previous one
  std::unique_ptr<PetscVector> petscvec = v0.petsc_vector();
  const Vec &petsc_vec = petscvec->vec();

  int size;
  VecGetSize(petsc_vec, &size);
  EXPECT_EQ(size, n);
  for (int i = 0; i < n; ++i)
  {
    double value;
    VecGetValues(petsc_vec, 1, &i, &value);
    EXPECT_DOUBLE_EQ(value, 1. * (i+1));
  }
}

// =============================================================================
TEST(Vector, petsc_vector_2) // check memory leaks for this conversion
{
  const int n = 100000;
  Vector v0(n);
  for (int i = 0; i < n; ++i)
    v0(i) = 2. * (i+1);

  // this is the difference between this test and the previous one
  std::unique_ptr<PetscVector> petscvec = v0.petsc_vector();
  const Vec &petsc_vec = petscvec->vec();

  int size;
  VecGetSize(petsc_vec, &size);
  EXPECT_EQ(size, n);
  for (int i = 0; i < n; ++i)
  {
    double value;
    VecGetValues(petsc_vec, 1, &i, &value);
    EXPECT_DOUBLE_EQ(value, 2. * (i+1));
  }
}

//// =============================================================================
// THERE IS NO SUCH A FUNCTION ANYMORE
//
//TEST(Vector, to_petsc)
//{
//  const int n = 100;
//  Vector v0(n);
//  for (int i = 0; i < n; ++i)
//    v0(i) = 1. * (i+1);

//  std::unique_ptr<Vec> petsc_vec = v0.to_petsc();
//  int size;
//  VecGetSize(*petsc_vec.get(), &size);
//  EXPECT_EQ(size, n);
//  for (int i = 0; i < n; ++i)
//  {
//    double value;
//    VecGetValues(*petsc_vec.get(), 1, &i, &value);
//    EXPECT_DOUBLE_EQ(value, 1. * (i+1));
//  }

//  VecDestroy(petsc_vec.get());
//}

// =============================================================================
TEST(Vector, std_vector)
{
  const int n = 100;
  Vector v0(n);
  for (int i = 0; i < n; ++i)
    v0(i) = 1. * (i+1);

  std::vector<double> std_vec = v0.std_vector();
  EXPECT_EQ(std_vec.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(std_vec[i], 1. * (i+1));
}

// =============================================================================
TEST(Vector, set)
{
  const int n = 100;

  Vector v0(n, 10.);
  EXPECT_EQ(v0.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 10.);

  v0.set(42.);
  EXPECT_EQ(v0.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 42.);
}

// =============================================================================
TEST(Vector, init_petsc)
{
  const int n = 100;
  Vec petsc_vec;
  VecCreateSeq(PETSC_COMM_SELF, n, &petsc_vec);
  for (int i = 0; i < n; ++i)
    VecSetValue(petsc_vec, i, 1.*i, INSERT_VALUES);

  const int m = 200;
  Vector v0(m, 42.);
  EXPECT_FALSE(v0.empty());
  EXPECT_EQ(v0.size(), m);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 42.);

  v0.init(petsc_vec);
  EXPECT_FALSE(v0.empty());
  EXPECT_EQ(v0.size(), n); // new size
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 1.*i); // new values

  Vector v1;
  EXPECT_TRUE(v1.empty());

  v1.init(petsc_vec);
  EXPECT_FALSE(v1.empty()); // not empty
  EXPECT_EQ(v1.size(), n); // new size
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v1(i), 1.*i);

  VecDestroy(&petsc_vec);
}

// =============================================================================
TEST(Vector, init_stdvec)
{
  const int n = 100;
  std::vector<double> std_vec(n);
  for (int i = 0; i < n; ++i)
    std_vec[i] =  1.*i;

  const int m = 200;
  Vector v0(m, 42.);
  EXPECT_FALSE(v0.empty());
  EXPECT_EQ(v0.size(), m);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 42.);

  v0.init(std_vec);
  EXPECT_FALSE(v0.empty());
  EXPECT_EQ(v0.size(), n); // new size
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 1.*i); // new values

  Vector v1;
  EXPECT_TRUE(v1.empty());

  v1.init(std_vec);
  EXPECT_FALSE(v1.empty()); // not empty
  EXPECT_EQ(v1.size(), n); // new size
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v1(i), 1.*i);
}

// =============================================================================
TEST(Vector, operator_shift)
{
  const int n = 1;
  Vector v0(n, 42.);

  // standard output
  std::cout << "test output\n";
  std::cout << v0 << std::endl;

  // file output
  std::string outfile = TESTOUT_DIR + "vec_output_test.txt";
  std::ofstream out(outfile.c_str());
  require(out, "File " + outfile + " can't be opened");
  out << v0 << "\n";
  out.close();

  // check the file output
  std::ifstream in(outfile.c_str());
  require(in, "File " + outfile + " can't be opened");
  std::string tmp;
  int m;
  double value;
  in >> tmp; EXPECT_EQ(tmp, "vector:");
  in >> tmp; EXPECT_EQ(tmp, "size");
  in >> tmp; EXPECT_EQ(tmp, "=");
  in >> m; EXPECT_EQ(n, m);
  for (int i = 0; i < m; ++i) {
    in >> value;
    EXPECT_DOUBLE_EQ(value, v0(i));
  }
}

// =============================================================================
TEST(Vector, operator_plus)
{
  const int n = 100;
  Vector v0(n), v1(n), v2(n);
  for (int i = 0; i < n; ++i)
  {
    v0(i) = 1.*i;
    v1(i) = 1.*i + 1;
    v2(i) = 1.*i - 2;
  }

  Vector v3 = v0 + v1 + v2;
  EXPECT_FALSE(v3.empty());
  EXPECT_EQ(v3.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v3(i), 3.*i - 1);
}

// =============================================================================
TEST(Vector, operator_minus)
{
  const int n = 100;
  Vector v0(n), v1(n), v2(n);
  for (int i = 0; i < n; ++i)
  {
    v0(i) = 1.*i;
    v1(i) = 1.*i + 1;
    v2(i) = 1.*i - 2;
  }

  Vector v3 = v0 - v1 - v2;
  EXPECT_FALSE(v3.empty());
  EXPECT_EQ(v3.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v3(i), -1.*i + 1);
}

// =============================================================================
TEST(Vector, operator_number_times_vector)
{
  const int n = 100;
  Vector v0(n);
  for (int i = 0; i < n; ++i)
    v0(i) = 1.*i;

  Vector v1 = 2.5 * v0;
  Vector v2 = 1./10. * v0;
  EXPECT_FALSE(v1.empty());
  EXPECT_FALSE(v2.empty());
  EXPECT_EQ(v1.size(), n);
  EXPECT_EQ(v2.size(), n);
  for (int i = 0; i < n; ++i)
  {
    EXPECT_DOUBLE_EQ(v1(i), 2.5*i);
    EXPECT_DOUBLE_EQ(v2(i), 1.*i/10.);
  }
}

// =============================================================================
TEST(Vector, operator_plus_equal)
{
  const int n = 100;
  Vector v0(n), v1(n), v2(n);
  for (int i = 0; i < n; ++i)
  {
    v0(i) = 1.*i;
    v1(i) = 1.*i + 1;
    v2(i) = 1.*i - 2;
  }

  v0 += v1 + v2;
  EXPECT_FALSE(v0.empty());
  EXPECT_EQ(v0.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 3.*i - 1);

  v0 += v0;
  EXPECT_FALSE(v0.empty());
  EXPECT_EQ(v0.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 6.*i - 2);
}

// =============================================================================
TEST(Vector, operator_minus_equal)
{
  const int n = 100;
  Vector v0(n), v1(n), v2(n);
  for (int i = 0; i < n; ++i)
  {
    v0(i) = 1.*i;
    v1(i) = 1.*i + 1;
    v2(i) = 1.*i - 2;
  }

  v0 -= v1 + v2;
  EXPECT_FALSE(v0.empty());
  EXPECT_EQ(v0.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), -1.*i + 1);

  v0 -= v0;
  EXPECT_FALSE(v0.empty());
  EXPECT_EQ(v0.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v0(i), 0.);
}

// =============================================================================
TEST(Vector, operator_mult_div_equal)
{
  const int n = 100;
  Vector v0(n);
  for (int i = 0; i < n; ++i)
    v0(i) = 1.*i;

  Vector v1(v0);

  v0 *= 2.5;
  v1 /= 10.;
  EXPECT_FALSE(v0.empty());
  EXPECT_FALSE(v1.empty());
  EXPECT_EQ(v0.size(), n);
  EXPECT_EQ(v1.size(), n);
  for (int i = 0; i < n; ++i)
  {
    EXPECT_DOUBLE_EQ(v0(i), 2.5*i);
    EXPECT_DOUBLE_EQ(v1(i), 1.*i/10.);
  }
}

// =============================================================================
TEST(Vector, get_subvector)
{
  const int n = 14;
  const double array[] = { 1, 4, 6, 8, 2, 4, 6, 7, 4, 3, 5, 6, 7, 1 };
  const std::vector<double> vec0(array, array + n);
  Vector v0(vec0);

  const int shift = 4; // starting from the 4-th element
  const int nsub  = 8; // get 8 elements
  const double subarray[] = { 2, 4, 6, 7, 4, 3, 5, 6 };

  Vector v1 = v0.get_subvector(shift, nsub);

  EXPECT_EQ(v1.size(), nsub);
  for (int i = 0; i < nsub; ++i)
    EXPECT_DOUBLE_EQ(v1(i), subarray[i]);
}

// =============================================================================
TEST(Vector, set_subvector)
{
  const int shift = 4;
  const int nsub = 8;
  const double subarray[] = { 2, 4, 6, 7, 4, 3, 5, 6 };
  const std::vector<double> subvec(subarray, subarray + nsub);
  Vector subv0(subvec);

  const int n = 14;
  const double array[] = { 0, 0, 0, 0, 2, 4, 6, 7, 4, 3, 5, 6, 0, 0 };

  Vector v1(n);
  v1.set_subvector(shift, subv0);

  EXPECT_EQ(v1.size(), n);
  for (int i = 0; i < n; ++i)
    EXPECT_DOUBLE_EQ(v1(i), array[i]);
}

#endif // FEMPLUS_TEST_VECTOR_HPP
