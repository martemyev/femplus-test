#ifndef FEMPLUS_TEST_JADAMILU_HPP
#define FEMPLUS_TEST_JADAMILU_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/dense_pattern.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/eigensolver.hpp"

// if we want to check visually the solutions obtained with JADAMILU against
// Matlab
//#define PRINT_EIGS_

using namespace femplus;

extern "C" void dpjd_(const int &N,
                      double *A,
                      int *JA,
                      int *IA,
                      double *EIGS,
                      double *RES,
                      double *X,
                      const int &LX,
                      int &NEIG,
                      const double &SIGMA,
                      const int &ISEARCH,
                      const int &NINIT,
                      const int &MADSPACE,
                      int &ITER,
                      const double &TOL,
                      double &SHIFT,
                      double &DROPTOL,
                      const double &MEM,
                      const int *ICNTL,
                      const int &IPRINT,
                      int &INFO,
                      double &GAP);

// =============================================================================
TEST(Jadamilu, standard)
{
  const int N = 5;
  int IA[] = { 1, 3, 5, 7, 9, 10 };
  int JA[] = { 1, 2, 2, 3, 3, 4, 4, 5, 5 };
  double A[] = { 0., 5., 1., 5., 2., 5., 3., 5., 4. };

  const int MAXEIG = 5;
  const int MAXSP = 20;
  double EIGS[MAXEIG];
  double RES[MAXEIG];
  const int LX = N * (3*MAXSP + MAXEIG + 1) + 4*MAXSP*MAXSP;
  double X[LX];
  int NEIG = MAXEIG;
  const double SIGMA = 0;
  const int ISEARCH = 0; // 0-smallest, 2-target
  const int NINIT = 0;
  const int MADSPACE = MAXSP;
  int ITER = 1000;
  const double TOL = 1e-10;
  double SHIFT = 0;
  double DROPTOL = 1e-6;
  const double MEM = 20.;
  const int ICNTL[] = { 0, 0, 0, 0, 0 };
  const int IPRINT = 0; // stdout
  int INFO;
  double GAP;
  dpjd_(N, A, JA, IA,
        EIGS, RES, X,
        LX, NEIG, SIGMA, ISEARCH, NINIT, MADSPACE,
        ITER, TOL, SHIFT, DROPTOL, MEM, ICNTL, IPRINT,
        INFO, GAP);

  std::vector<double> eigenvalues;
  std::vector<Vector> eigenvectors;
  eigensolver_JADAMILU_HEP(IA, JA, A, N, N, eigenvalues, eigenvectors);

  EXPECT_EQ((int)eigenvalues.size(), N);
  for (int i = 0; i < N; ++i)
    EXPECT_DOUBLE_EQ(eigenvalues[i], EIGS[i]);

  const double eigenval[] = { -6.9654,-2.9619,2.0000,6.9619,10.9654 };
  const double eigenvec[][N] = { { -0.4391, 0.6116, -0.5353, 0.3483, -0.1588 },
                                 { 0.5322, -0.3153, -0.2824, 0.5955, -0.4277 },
                                 { 0.5620, 0.2248, -0.5170, -0.2248, 0.5620 },
                                 { -0.4277, -0.5955, -0.2824, 0.3153, 0.5322 },
                                 { 0.1588, 0.3483, 0.5353, 0.6116, 0.4391 } };

  double tol = 5e-5; // tolerance shouldn't be too small
  for (int i = 0; i < N; ++i)
    EXPECT_NEAR(eigenvalues[i], eigenval[i], tol); // comparison with Matlab

  tol = 5e-5; // for eigenvectors
  for (int i = 0; i < N; ++i)
  {
    // the eigenvectors for the same eigen value may have opposite direction, so
    // we need to find out if this difference exists
    double sign = (eigenvectors[i](0) / eigenvec[i][0] > 0 ? 1. : -1.);
    for (int j = 0; j < N; ++j)
      EXPECT_NEAR(eigenvectors[i](j), sign*eigenvec[i][j], tol);
  }

#if defined(PRINT_EIGS_)
  printf("Eigenvalues\n%12s  %12s\n", "JADAMILU", "Matlab");
  for (int i = 0; i < N; ++i)
    printf("%2.6e  %2.6e\n", EIGS[i], eigenval[i]);
  printf("\nEigenvectors\n");

  for (int i = 0; i < N; ++i)
  {
    for (int j = 0; j < N; ++j)
      printf("%2.6e  ", eigenvectors[i](j));
    printf("\n");
    for (int j = 0; j < N; ++j)
      printf("%2.6e  ", eigenvec[i][j]);
    printf("\n---\n");
  }
#endif // PRINT_EIGS_
}

// =============================================================================
TEST(Jadamilu, petsc_matrix_0)
{
  const int N = 5;
  int IA[] = { 1, 3, 5, 7, 9, 10 };
  int JA[] = { 1, 2, 2, 3, 3, 4, 4, 5, 5 };
  double A[] = { 0., 5., 1., 5., 2., 5., 3., 5., 4. };

  const double mat[][N] = { { 0, 5, 0, 0, 0 },
                            { 5, 1, 5, 0, 0 },
                            { 0, 5, 2, 5, 0 },
                            { 0, 0, 5, 3, 5 },
                            { 0, 0, 0, 5, 4 } };
  DensePattern pattern(N, N);
  PetscMatrix M(pattern);
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      M.add_value(i, j, mat[i][j]);
  M.final_assembly();
  std::vector<int> mIA, mJA;
  std::vector<double> mVA;
  M.sym_csr_arrays(pattern, mIA, mJA, mVA, 1);

  std::vector<double> eigs_0, eigs_1;
  std::vector<Vector> eigenvectors_0, eigenvectors_1;

  eigensolver_JADAMILU_HEP(IA, JA, A, N, N, eigs_0, eigenvectors_0);
  eigensolver_JADAMILU_HEP(&mIA[0], &mJA[0], &mVA[0],
                           N, N, eigs_1, eigenvectors_1);

  EXPECT_EQ((int)eigs_0.size(), N);
  EXPECT_EQ((int)eigs_1.size(), N);

  for (int i = 0; i < N; ++i)
    EXPECT_DOUBLE_EQ(eigs_0[i], eigs_1[i]);
}


#endif // FEMPLUS_TEST_JADAMILU_HPP
